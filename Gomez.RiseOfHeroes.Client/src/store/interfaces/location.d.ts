import { IPaged } from "./paged";

export interface ILocation {
    id:string;
    name?: any;
    coordinate: ICoordinate;
    cost: number;
    distance: number;
    hasBackgrounds:number;
}

export interface ILocationResults extends IPaged  {
    results: Array<ILocation>
}

export interface ILocationIndex {
    locations: ILocationResults
}

export interface ILocationGet {
    location: ILocation
}

export interface ICoordinate{
    x: number;
    y: number;
}