export interface IAppLayout {
    displayNavBar: boolean;
    displaySideBar: boolean;
}

export interface ILoading{
    key:string
}