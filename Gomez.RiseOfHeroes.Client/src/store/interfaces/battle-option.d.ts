export interface IBattleOption {
    retreatAtHealthPercent: number;
    target: number;
    tactic: number;
}