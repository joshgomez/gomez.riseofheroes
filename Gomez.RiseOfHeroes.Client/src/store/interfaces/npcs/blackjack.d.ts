
export interface IBlackJackResult {
    dealersCards: string[];
    playersCards: string[];
    state: string;
    stake: number;
    profit: number;

    dealerScore: number;
    playerScore: number;
    canStay:boolean;
}