export interface ISlot {
    number: number;
    color: string;
}

export interface IRouletteResult {
    stake: number;
    profit: number;
    number: number;
}