
export interface IReelData{
    name:string;
    value:number;
    image:string;
  }

export interface ISlotMachineResult {
    slots: string[];
    credits: number;
    profit: number;
}