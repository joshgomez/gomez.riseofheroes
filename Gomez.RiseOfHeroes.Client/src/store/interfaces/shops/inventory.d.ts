import { IPaged } from "../paged";
import { IValidation } from "../validation";
import { IInventoryItemBase } from "../item";

export interface IInventoryItem extends IInventoryItemBase {
    quantity: number;
    cost: number;
    actualCost: number;
    name: string;
    hasImage:boolean;
}

export interface IInventoryResults extends IPaged  {
    results: Array<IInventoryItem>
}

export interface IInventoryIndex {
    items: IInventoryResults
}

export interface IInventoryBuy extends IValidation<IInventoryBuyState> {
    
}

export interface IInventoryBuyState {
    buyersId:number;
    buyersMoney: number;
    //itemName
    //cost
    //quantity
    //sellersId
    sellersMoney: number;
    sellersQuantity: number;
}

export interface IInventoryBuyPostData{
    qty:number;
}

export interface IInventorySellPostData{
    qty:number;
    buyerId:number;
}