import { IPaged } from "./paged";
import { ICoordinate } from "./location";
import { IEquipmentItem } from "./item";

export interface ICharacter {
    id:number;
    name?: any;
    health: number;
    energy: number;
    maxHealth: number;
    maxEnergy: number;
    maxDamage: number;
    minDamage: number;
    strength: number;
    constitution: number;
    dexterity: number;
    intelligence: number;
    weight: number;
    coordinate: ICoordinate;
    money:number;
    nextLevel:number;
    nextLevelFactor: number;
    level:number;
    experience:number;
    attributes:number
}

export interface ICustomCharacter extends ICharacter {
    hasBlackJack:boolean;
    hasSlotMachine:boolean;
    hasRoulette:boolean;
    workTypes:Record<string,number>;
}

export interface ICharacterResults extends IPaged  {
    results: Array<ICharacter>
}

export interface ICharacterIndex {
    characters: ICharacterResults
}

export interface ICharacterInLocation{
    id:number;
    coordinate: ICoordinate;
}

export interface IEquipment{
    head:IEquipmentItem     | null;
    body:IEquipmentItem     | null;
    weapon:IEquipmentItem   | null;
}