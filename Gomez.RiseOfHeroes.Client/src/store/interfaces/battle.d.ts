import { IBattleOption } from "./battle-option";

export interface IBattleRound{
    round:number;
    messages:Array<string>;
}

export interface IBattle{
    rounds:Record<string,IBattleRound>;
}

export interface IBattlePostData{
    id: number;
    option: IBattleOption;
}