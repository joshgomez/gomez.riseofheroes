import { IPaged } from "../paged";
import { IInventoryItemBase } from "../item";

export interface IInventoryItem extends IInventoryItemBase {
    quantity: number;
    weight: number;
    totalWeight:number;
}

export interface IInventoryResults extends IPaged  {
    results: Array<IInventoryItem>
}

export interface IInventoryIndex {
    items: IInventoryResults
}