export interface IReplenishedEnergy{
    nextUpdateAtMs:number;
    energy:number | null;
}