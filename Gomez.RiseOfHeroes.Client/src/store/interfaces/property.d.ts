

export interface IProperty {
    key: string;
    typeNum: number;
    value: any;
}

export interface IPropertyCollection {
    properties: Array<IProperty>
}