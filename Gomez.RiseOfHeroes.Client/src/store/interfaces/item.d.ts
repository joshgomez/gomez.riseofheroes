import { IPropertyCollection } from "./property";

export interface IItemResponse {
    id:string;
    detail:IItemDetail;
    dynamicProperties:IItemDynamic;
    propertyCollection: IPropertyCollection;
}

export interface IItemDetail {
    id:string;
    name?: any;
    type: number;
}

export interface IItem {
    id:number;
    quantity: number;
    state: number;
}

//Not a typo
export interface IItemDynamic {
    ItemCalculation:IItemCalculation;
}

export interface IItemCalculation {
    healingEffect:number;
    totalWeight:number;
}

export interface IItemInformation {
    id:string;
    name: string;
    type: number;
    healingEffect?:number;
    quantity?:number;
    state?:number;
    weight:number;
    damage:number;
    armor:number;
    hasImage:boolean;
}

export interface IItemGet {
    detail: IItemDetail;
    item: IItem;
    dynamicProperties: IItemDynamic;
    propertyCollection: IPropertyCollection;
    state:number;
}

export interface IEquipmentItem {
    id:number;
    detailId: string;
    name: string;
    type: number;
}


export interface IInventoryItemBase {
    id:number;
    name?: string;
    hasImage:boolean;
    detailId:string;
}