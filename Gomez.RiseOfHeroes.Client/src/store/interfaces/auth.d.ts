export interface IJwtPayload {
    sub: string;
    email: string;
    jti: string;
    exp: number;
    iss: string;
    aud: string;
}

export interface IAuthData {
    token: string;
    expireTime: number;
    refreshToken:IRefreshToken;
}

export interface IRefreshToken {
    token: string;
    expires:string | null;
}

export interface ILoginPostData {
    email: string;
    password: string;
}

export interface IRegisterPostData {
    email: string;
    password: string;
    confirmPassword: string;
}