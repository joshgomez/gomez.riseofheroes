import axios, { AxiosRequestConfig, AxiosInstance } from "axios";

export default class ApiService {

    static HEADER_AUTH_KEY:string = "Authorization";
    instance!: AxiosInstance;
    interceptor?: (value: AxiosRequestConfig) => Promise<AxiosRequestConfig>;
    getRequestConfig(accessToken: string): AxiosRequestConfig {

        var config: AxiosRequestConfig = {
            baseURL: "https://localhost:44311/api",
            headers: {
                "Content-Type": "application/json"
            }
        };

        if (accessToken && accessToken.length > 0) {
            config.headers[ApiService.HEADER_AUTH_KEY] = "bearer " + accessToken;
        }

        return config;
    }

    constructor(token: string, interceptor?: (value: AxiosRequestConfig) => Promise<AxiosRequestConfig>) {
        this.interceptor = interceptor;
        this.setToken(token);
    }

    setToken(token: string):void {
        this.instance = axios.create(this.getRequestConfig(token));
        if(this.interceptor != undefined)
            this.instance.interceptors.request.use(this.interceptor);
    }

    clearToken():void {
        this.instance = axios.create(this.getRequestConfig(""));
    }
}