import jwtDecode from "jwt-decode";
import { IJwtPayload } from '../interfaces/auth';

export default class JwtService {
    static decodeToken(value:string): IJwtPayload {
        return jwtDecode<IJwtPayload>(value);
    }
}