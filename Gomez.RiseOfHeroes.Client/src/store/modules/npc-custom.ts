import {
    getModule,
    Module,
    VuexModule,
} from "vuex-module-decorators";
import store from "@/store";
import { ICoordinate } from '../interfaces/location';
import auth from "@/store/modules/auth";
import { ICharacterInLocation } from '../interfaces/character';


@Module({
    name: "NpcCustomState",
    namespaced: true,
    stateFactory: true,
    store,
    dynamic:true
})

export class NpcCustomModule extends VuexModule {
    public static async InLocationAsync(id:number,coord:ICoordinate): Promise<boolean> {
      const postData: ICharacterInLocation = { id:id,coordinate:coord };
      const response = await auth.api.instance.post<boolean>(`/game/npccustoms/${id}/InLocation`, postData);
      return response.data;
    }
}

export default getModule(NpcCustomModule);