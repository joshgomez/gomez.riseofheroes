import {
    getModule,
    Module,
    VuexModule,
    Mutation,
    Action,
} from "vuex-module-decorators";
import store from "@/store";
import { ILocationIndex, ILocationGet, ILocation, ICoordinate } from '../interfaces/location';
import auth from "@/store/modules/auth";


@Module({
    name: "LocationState",
    namespaced: true,
    stateFactory: true,
    store,
    dynamic:true
})

export class LocationModule extends VuexModule {
    private _index: ILocationIndex | null = null;
    private _current: ILocation | null = null;

    get index(): ILocationIndex | null {
        return this._index;
    }

    get current(): ILocation | null {
        return this._current;
    }

    @Mutation
     setLocations(value:ILocationIndex) : void {
        this._index = value;
    }

    @Mutation
    setLocation(value:ILocation) : void {
       this._current = value;
   }

    @Action({ commit: "setLocations"})
    async loadIndexAsync(coord:ICoordinate) {
      const response = await auth.api.instance.get<ILocationIndex>(`/game/locations?currentPosition.x=${coord.x}&currentPosition.y=${coord.y}`)
      return response.data;
    }

    @Action({ commit: "setLocation"})
    async loadByIdAsync(id:string) {
      const response = await auth.api.instance.get<ILocationGet>(`/game/locations/${id}`);
      return response.data.location;
    }

    @Action({ commit: "setLocation"})
    async loadByCoordAsync(coord:ICoordinate) {
      const response = await auth.api.instance.get<ILocationGet>(`/game/locations/bycoord?x=${coord.x}&y=${coord.y}`);
      return response.data.location;
    }
}

export default getModule(LocationModule);