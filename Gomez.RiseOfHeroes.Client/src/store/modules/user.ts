import {
    getModule,
    Module,
    VuexModule,
    Mutation,
    Action,
} from "vuex-module-decorators";
import store from "@/store";
import auth from "@/store/modules/auth";
import JwtService from "../services/jwtservice";
import { IUser } from '../interfaces/user';
import { IJwtPayload } from '../interfaces/auth';

@Module({
    name: "UserState",
    namespaced: true,
    stateFactory: true,
    store,
    dynamic:true
})

export class UserModule extends VuexModule {
    user: IUser = { id: "", username: "", email : "" };
    get username(): string {
        return (this.user && this.user.username) ?  this.user.username : "";
    }

    get name(): string{
        return this.username.substring(0, this.username.lastIndexOf("@"));
    }

    @Mutation
    setUser(value: IUser): void {
        this.user = value;
    }

    @Action({ commit: "setUser" })
    getUserByToken(): IUser | null {
        if(!auth.data.token && auth.data.token.length === 0) {
            return null;
        }

        var decodedToken:IJwtPayload = JwtService.decodeToken(auth.data.token);
        return { 
            username: decodedToken.email, 
            email: decodedToken.email,
            id: decodedToken.sub 
        };
    }
}

export default getModule(UserModule);