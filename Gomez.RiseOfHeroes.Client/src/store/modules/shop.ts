import {
    getModule,
    Module,
    VuexModule,
    Action,
} from "vuex-module-decorators";
import store from "@/store";
import { ICoordinate } from '../interfaces/location';
import auth from "@/store/modules/auth";
import { ICharacterInLocation } from '../interfaces/character';


@Module({
    name: "ShopState",
    namespaced: true,
    stateFactory: true,
    store,
    dynamic:true
})

export class ShopModule extends VuexModule {
    public static async InLocationAsync(id:number,coord:ICoordinate): Promise<boolean> {
      const postData: ICharacterInLocation = { id:id,coordinate:coord };
      const response = await auth.api.instance.post<boolean>(`/game/npcshops/${id}/InLocation`, postData);
      return response.data;
    }
}

export default getModule(ShopModule);