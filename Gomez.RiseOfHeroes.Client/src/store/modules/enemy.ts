import {
    getModule,
    Module,
    VuexModule,
    Action,
} from "vuex-module-decorators";
import store from "@/store";
import { ICoordinate } from '../interfaces/location';
import auth from "@/store/modules/auth";
import { ICharacterInLocation } from '../interfaces/character';


@Module({
    name: "EnemyState",
    namespaced: true,
    stateFactory: true,
    store,
    dynamic:true
})

export class EnemyModule extends VuexModule {
    public static async InLocationAsync(id:number,coord:ICoordinate): Promise<boolean> {
      const postData: ICharacterInLocation = { id:id,coordinate:coord };
      console.log("InLocationAsync",postData);
      const response = await auth.api.instance.post<boolean>(`/game/npcenemies/${id}/InLocation`, postData);
      return response.data;
    }
}

export default getModule(EnemyModule);