import {
    getModule,
    Module,
    VuexModule,
    Mutation,
    MutationAction,
    Action,
} from "vuex-module-decorators";
import store from "@/store";
import { ICharacter } from '../interfaces/character';
import auth from "@/store/modules/auth";


@Module({
    name: "CharacterState",
    namespaced: true,
    stateFactory: true,
    store,
    dynamic:true
})

export class CharacterModule extends VuexModule {
    private _player: ICharacter | null = null;

    get playerinfo(): ICharacter | null {
        return this._player;
    }

    @Mutation
     setPlayer(value:ICharacter) : void {
        this._player = value;
    }

    @Mutation
     setName(value:string) : void {
         if(this._player != null && value){
             this._player.name = value;
         }
    }

    @Mutation
    setMoney(value:number) : void {
        if(this._player != null && value){
            this._player.money = value;
        }
    }

    @Mutation
    setEnergy(value:number) : void {
        if(this._player != null && value){
            this._player.energy = value;
        }
    }

    @Action({ commit: "setPlayer"})
    async loadAsync() {
      const response = await auth.api.instance.get<ICharacter>("/game/players/basic")
      return response.data;
    }

    @Action({ commit: "setPlayer"})
    async newAsync() {
      let response = await auth.api.instance.post("/game/players")
      if(response.status == 200){
          response = await auth.api.instance.get<ICharacter>("/game/players/basic")
          return response;
      }

      return null;
    }

    @Action({ commit: "setName"})
    async setNameAsync(value:string) : Promise<string> {
        try{
            const response = await auth.api.instance.patch<string>("/game/players/basic", { name : value })
            return response.data;
        } catch{
            return "";
        }
    }

    @Action({ commit: "setEnergy"})
    async setEnergyAsync(value:number) : Promise<number> {
        return value;
    }

    @Action({ commit: "setMoney"})
    async setMoneyAsync(value:number) : Promise<number> {
        return value;
    }
}

export default getModule(CharacterModule);