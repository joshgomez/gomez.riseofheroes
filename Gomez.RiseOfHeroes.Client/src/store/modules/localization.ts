import {
    getModule,
    Module,
    VuexModule,
    Mutation,
    Action,
} from "vuex-module-decorators";
import store from "@/store";
import auth from "@/store/modules/auth";

export enum TextTransformType {
    Normal = 0,
    UpperCaseFirst,
}

@Module({
    name: "LocalizationState",
    namespaced: true,
    stateFactory: true,
    store,
    dynamic:true
})
export class LocalizationModule extends VuexModule {
    private _data : { [s: string]: { [s: string]: Array<string>; }; } = {}

    get data() { return this._data; }

    @Mutation
     setData(value:{ [s: string]: { [s: string]: Array<string>; }; } | undefined) : void {
        if(value != undefined){
            this._data = Object.assign(this._data,value);
        }
    }

    @Action({ commit: "setData"})
    async loadAsync(collection:string) {
        if(this._data.hasOwnProperty(collection)){
            return undefined;
        }

        const response = await auth.api.instance.get<{ [s: string]: Array<string>; }>("/localization?id=" + collection);
        
        let value: { [s: string]: { [s: string]: Array<string>; }; } = {};
        value[collection] = response.data;
        return value;
    }


}

const localization = getModule(LocalizationModule);
export class LocalizationHelper{
    private _module: LocalizationModule;
    collection :string = "";
    constructor(lModule?: LocalizationModule){
        this._module = lModule || localization;
    }

    isLoaded(collection:string): boolean{
        if(this._module.data.hasOwnProperty(collection)) {
            return true;
        }

        return false;
    }

    async loadAync(name:string):Promise<void>{
        await this._module.loadAsync(name);
    }

    translate(key:string, collection:string = "", 
        transform: TextTransformType = TextTransformType.Normal,
        index: number = 0): string {

        collection = collection == "" ? this.collection : collection; 
        if(!this._module.data.hasOwnProperty(collection) || !this._module.data[collection].hasOwnProperty(key)) {
            return key;
        }

        var textCounts = this._module.data[collection][key].length;
        if(textCounts == 0){
            return key;
        }

        if(index > textCounts - 1){
            index = textCounts - 1;
        }

        let retContent: string = this._module.data[collection][key][index];
        switch(transform) {
            case TextTransformType.UpperCaseFirst:
                return this.ucFirst(retContent);
            default:
                return retContent;
        }
    }

    lbl(key:string, collection:string = "", 
    transform: TextTransformType = TextTransformType.Normal,
    index: number = 0) : string {
        key = key + ".Label";
        return this.translate(key,collection,transform,index);
    }

    ttl(key:string, collection:string = "", 
    transform: TextTransformType = TextTransformType.Normal,
    index: number = 0) : string {
        key = key + ".Title";
        return this.translate(key,collection,transform,index);
    }

    txt(key:string, collection:string = "", 
    transform: TextTransformType = TextTransformType.Normal,
    index: number = 0) : string {
        key = key + ".Text";
        return this.translate(key,collection,transform,index);
    }

    act(key:string, collection:string = "", 
    transform: TextTransformType = TextTransformType.Normal,
    index: number = 0) : string {
        key = key + ".Action";
        return this.translate(key,collection,transform,index);
    }

    private ucFirst(content:string): string {
        return content.charAt(0).toUpperCase() + content.slice(1);
    }
}

export default localization