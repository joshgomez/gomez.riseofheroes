import { Action, Module, Mutation, VuexModule, MutationAction, getModule } from "vuex-module-decorators";
import ApiService from "../services/apiservice";
import { AxiosResponse, AxiosRequestConfig } from "axios";
import store from "@/store";
import JwtService from "../services/jwtservice";
import { IAuthData, IJwtPayload, ILoginPostData, IRegisterPostData } from '../interfaces/auth';
import SecureLS from 'secure-ls';

@Module({
    name: "AuthState",
    namespaced: true,
    stateFactory: true,
    store,
    dynamic:true
})
export class AuthModule extends VuexModule {
    private _secureStorage = new SecureLS();
    error: string = "";
    data: IAuthData = { 
        token: "", 
        expireTime: 0,
        refreshToken: { 
            token: "",
            expires: null
        } 
    };

    timerId:number = -1;

    api: ApiService = new ApiService("");
    static accessInvalidationOffset:number = 10; //seconds before invalid access token.

    get isAccessValid(): boolean {
        return (this.data.token.length > 0 && this.remainingSecondsToInvalidation > 0) || false;
    }

    get remainingSecondsToInvalidation(): number {
        const sec:number = new Date().getTime() / 1000;
        return this.data.expireTime - (sec + AuthModule.accessInvalidationOffset);
    }

    
    @Action
    private async _remainingSecondsToInvalidation(data:IAuthData): Promise<number> {
        const sec:number = new Date().getTime() / 1000;
        const decodedToken:IJwtPayload = JwtService.decodeToken(data.token);
        return decodedToken.exp - (sec + AuthModule.accessInvalidationOffset);
    }


    @Action
    private async _setTimerRefresh(data:IAuthData): Promise<boolean> {
        if(!AuthModule._isAuthenticated(data)){
            return false;
        }
        
        const remaining:number = await this._remainingSecondsToInvalidation(data);
        if(remaining < 0){
            console.log("Authenticated");
            await this.context.dispatch("refreshAsync",data);
            return true;
        }

        console.log("Authorizing in:",remaining);
        var self = this;
        const timerId:number = window.setTimeout(() => {
            if(self.isAuthenticated){
                console.log("Authenticated");
                self.context.dispatch("refreshAsync",data);
            }
        },remaining * 1000);

        await this.context.commit("setTimerId",timerId);
        return false;
    }

    get isAuthenticated(): boolean {
        return AuthModule._isAuthenticated(this.data);
    }

    private static _isAuthenticated(data:IAuthData): boolean {
        const milliseconds:number = new Date().getTime();
        if(data.refreshToken.expires != null){
            const expires : Date = new Date(data.refreshToken.expires);
            return expires.getTime() > milliseconds;
        }

        return false;
    }

    @Mutation
    setToken(value: IAuthData | null | undefined): void {
        if(value == undefined){
            return;
        }
        
        if (value !== null) {
            const decodedToken:IJwtPayload = JwtService.decodeToken(value.token);
            this.data.token = value.token;
            this.data.expireTime = decodedToken.exp;
            this.data.refreshToken = { 
                token: value.refreshToken.token, 
                expires: value.refreshToken.expires
            };

            this._secureStorage.set("z", value);
            this.api = new ApiService(value.token);
            return;
        }

        this.data.expireTime = 0;
        this.api = new ApiService("");
    }

    @Mutation
    setTimerId(value:number): void{
        this.timerId = value;
    }

    @Action({ commit: "setToken" })
    async loginAsync(postData: ILoginPostData): Promise<IAuthData | null | undefined> {
        try {
            const response:AxiosResponse<IAuthData> = await this.api.instance.post<IAuthData>("/token", postData);
            if (response.status !== 200) {
                throw "User not found";
            }
        
            if(await this._setTimerRefresh(response.data)){
                return undefined;
            }

            return response.data;
        } catch (err) {
            console.log("err", err);
            return null;
        }
    }

    @Action({ commit: "setToken" })
    async registerAsync(postData: IRegisterPostData): Promise<IAuthData | null | undefined> {
        try {
            const response:AxiosResponse<IAuthData> = await this.api
                .instance.post<IAuthData>("/account", postData);
            if (response.status !== 200) {
                throw "User not found";
            }
        
            if(await this._setTimerRefresh(response.data)){
                return undefined;
            }

            return response.data;
        } catch (err) {
            console.log("err", err);
            return null;
        }
    }

    @Action({ commit: "setToken" })
    async refreshAsync(data?: IAuthData): Promise<IAuthData | null | undefined> {
        try {
            data = data || this.data;
            const postData:any = { token: data.token, refreshToken: data.refreshToken.token };
            const response:AxiosResponse<IAuthData> = await this.api.instance.post<IAuthData>("/token/refresh", postData);
            if (response.status !== 200) {
                throw "Login expired";
            }
            
            if(await this._setTimerRefresh(response.data)){
                return undefined;
            }

            return response.data;
        } catch (err) {
            console.log("err", err);
            return null;
        }
    }

    @Action({ commit: "setToken" })
    async logoutAsync(): Promise<null> {
        this._secureStorage.remove("z");
        return null;
    }

    @Action({ commit: "setToken" })
    async rememberMeAsync(): Promise<IAuthData | null | undefined> {
        const z:IAuthData | undefined = this._secureStorage.get("z");
        if(z != undefined){
            const data: IAuthData | null = z as IAuthData || null;
            if(data != null) {
                if(await this._setTimerRefresh(data)) {
                    return undefined;
                }
                
                return data;
            }
        }

        return null;
    }
}

export default getModule(AuthModule);