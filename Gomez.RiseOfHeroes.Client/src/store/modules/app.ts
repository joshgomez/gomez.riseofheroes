import {
    getModule,
    Module,
    VuexModule,
    Mutation,
    Action,
    MutationAction,
} from "vuex-module-decorators";
import store from "@/store";
import { IAppLayout, ILoading } from '../interfaces/app';


@Module({
    name: "AppState",
    namespaced: true,
    stateFactory: true,
    store,
    dynamic:true
})

export class AppModule extends VuexModule {
    private _layout: IAppLayout = { 
        displayNavBar : true,
        displaySideBar: true
    }
    private _isLoading: Array<string> = [];

    get isLoading(): boolean {
        return this._isLoading.length > 0;
    }

    get layout(): IAppLayout {
        return this._layout;
    }

    get publicPath() : string{
        return process?.env.BASE_URL ?? "";
      }

    @Mutation
    private setLayout(value:IAppLayout) : void {
        this._layout = Object.assign({},this._layout, value);
    }

    @Mutation
    private addLoading(value:string) : void {
       this._isLoading.push(value);
    }

    @Mutation
    private removeLoading(value:string) : void {
        const i:number = this._isLoading.indexOf(value);
        this._isLoading.splice(i,1);
    }

    @Action({ commit: 'setLayout' })
    setNavBar(value:boolean) {
        return {displayNavBar:value};
    }

    @Action({ commit: 'addLoading' })
    setLoading(key:string): string {
        return key;
    }

    @Action({ commit: 'removeLoading' })
    deleteLoading(key:string): string {
        return key;
    }
}

export default getModule(AppModule);