import character from './modules/character';
import { LocalizationHelper } from './modules/localization';
import app from './modules/app';
import auth from './modules/auth';
import location from './modules/location';
import { ICharacter } from './interfaces/character';
import router from '@/router'

export default class InitHelper {
    _l:LocalizationHelper;
    constructor(l:LocalizationHelper){
        this._l = l || new LocalizationHelper();
    }

    async loadGameResourceAsync(): Promise<void>{
        if(!this._l.isLoaded("GameResource"))
        {
            try{
                await this._l.loadAync("GameResource");
            }catch(err){
                console.log("runAsync",err);
            }
        }
    }

    async runAsync(): Promise<void> {
        if(!auth.isAuthenticated){
            router.push("/login");
        }

        app.setLoading("character.playerinfo");
        app.setLoading("GameResource");
        app.setLoading("location.current");
        
        await this.loadGameResourceAsync();
        this._l.collection = "GameResource";
        app.deleteLoading("GameResource");
        
        if(character.playerinfo == null)
        {
            try{
                await character.loadAsync();
                if(character.playerinfo != null) {
                    const playerInfo: ICharacter = character.playerinfo;
                    await location.loadByCoordAsync(playerInfo.coordinate);
                }
            } catch(err) {
                console.log("No character could be found.");
            }
        }

        app.deleteLoading("location.current");
        app.deleteLoading("character.playerinfo");
    }

    async runAnonymousAsync(): Promise<void> {
        app.setLoading("GameResource");
        await this.loadGameResourceAsync();
        this._l.collection = "GameResource";
        app.deleteLoading("GameResource");
    }
}