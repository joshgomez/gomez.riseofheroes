import { IItemResponse, IItemInformation } from '../interfaces/item';
import { PropertyUtility } from '../utilities/property-utility';

export class ItemModel{
    constructor(data:IItemResponse | null){
        this.data = data;
    }

    data:IItemResponse | null = null;
    get info(): IItemInformation | null {
        if(this.data != null){

            const dmgProp = PropertyUtility.FindFromCollection(this.data.propertyCollection,"Stats.Damage");
            let damage:number = dmgProp ? dmgProp.value : 0;

            const armorProp = PropertyUtility.FindFromCollection(this.data.propertyCollection,"Stats.Armor");
            let armor:number = armorProp ? armorProp.value : 0;

            const hasImageProp = PropertyUtility.FindFromCollection(this.data.propertyCollection,"Has.Image");
            let hasImage:boolean = hasImageProp ? hasImageProp.value : false;

            this.data.propertyCollection
            return { 
                id: this.data.detail.id,
                name: this.data.detail.name,
                type: this.data.detail.type,
                healingEffect: this.data.dynamicProperties.ItemCalculation.healingEffect * 100,
                weight: this.data.dynamicProperties.ItemCalculation.totalWeight,
                damage: damage,
                armor: armor,
                hasImage:hasImage
             };
        }

        return null;
    }
}