﻿import { IProperty, IPropertyCollection } from '../interfaces/property';

export class PropertyUtility
{
    properties: Array<IProperty>;

    constructor(properties: Array<IProperty>) {
        this.properties = properties;
    }

    public Find(key: string): IProperty | undefined {
        return PropertyUtility.Find(this.properties, key);
    }

    public static Find(properties: Array<IProperty>, key: string): IProperty | undefined {
        return properties.find(function (element) {
            return element.key == key;
        });
    }

    public static FindFromCollection(collection: IPropertyCollection, key: string): IProperty | undefined {
        return collection.properties.find(function (element) {
            return element.key == key;
        });
    }
}