export function clone<T>(source: T) {
    // using native JSON functions removes reactivity
    // so we can clone an object without mutating the original source
    return JSON.parse(JSON.stringify(source)) as T;
}

export function randomInteger(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function randomNumber(min: number, max: number) {
    return Math.random() * (max - min) + min;
}