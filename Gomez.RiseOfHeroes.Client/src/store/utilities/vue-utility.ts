import { Route } from 'vue-router/types/router';

export class VueUtility {
    public static GetPageNum(route: Route) : number{
        let page: number = 1;
        if(route.query["page"] != null){
          page = parseInt(route.query["page"].toString(),10);
        }

        return page;
    }
}

export default VueUtility;