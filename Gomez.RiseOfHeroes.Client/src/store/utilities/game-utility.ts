import { ILocation } from '../interfaces/location';

export class GameUtility
{
    static SetBackgroundByLocation(location:ILocation): void {
        if(location.hasBackgrounds > 0){
            console.log("GameUtility","SetBackgroundByLocation");
            window.document.body.style.backgroundImage = `url('/img/locations/${location.id}-1.jpeg')`;
            return;
        }

        window.document.body.style.backgroundImage = "";
    }
}