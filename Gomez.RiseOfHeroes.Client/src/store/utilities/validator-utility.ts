﻿export class ValidatorUtility
{
    private static _password: RegExp =  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
    private static _email:RegExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+$/;
    
    static isPasswordValid(input:string): boolean {
        if(!input || input.length < 8) {
            return false;
        }
        
        if(!input.match(ValidatorUtility._password)) 
        { 
            return false;
        }

        return true;
    }

    static isEmailValid(input:string): boolean {
        if(!input || input.length < 3) {
            return false;
        }
        
        if(!input.match(ValidatorUtility._email)) 
        { 
            return false;
        }

        return true;
    }
}