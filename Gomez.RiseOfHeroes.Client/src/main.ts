import Vue from 'vue'

import 'bootstrap';
import BootstrapVue from 'bootstrap-vue';
import PortalVue from 'portal-vue'

import App from './App.vue'
import router from './router'
import store from './store'
import auth from './store/modules/auth';

Vue.config.productionTip = true;
Vue.use(BootstrapVue);
Vue.use(PortalVue)

auth.rememberMeAsync().then(() => {
    new Vue({
        router,
        store,
        render: h => h(App)
    }).$mount('#app');
});
