import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "@/views/Home.vue";
import auth from "@/store/modules/auth";
import Locations from "@/views/game/Locations.vue";
import Location from "@/views/game/Location.vue";
import Login from "@/views/Login.vue";

import NewPlayer from "@/views/game/players/NewPlayer.vue";
import Inventory from "@/views/game/players/Inventory.vue";

import Npc from "@/views/game/npcs/Npc.vue";
import Shop from "@/views/game/shops/Shop.vue";
import Enemy from "@/views/game/enemies/Enemy.vue";
import Item from "@/views/game/Item.vue";

import NotFound from "@/views/NotFound.vue";
import Register from "@/views/Register.vue";


Vue.use(VueRouter);
const routes:RouteConfig[] = [
    {
        path: "/",
        name: "home",
        component: Home,
    },
    {
        path: "/login",
        name: "login",
        component: Login,
    },
	{
        path: "/about",
        name: "about",
        component: Home,
    },
	{
        path: "/players/new",
        name: "player-new",
        component: NewPlayer,
    },
	{
        path: "/players/inventory",
        name: "player-inventory",
        component: Inventory,
    },
	{
        path: "/locations/:id",
        name: "location",
        component: Location,
    },
	{
        path: "/locations",
        name: "locations",
        component: Locations,
    },
	{
        path: "/enemies/:id",
        name: "enemy",
        component: Enemy,
    },
	{
        path: "/shops/:id",
        name: "shop",
        component: Shop,
    },
	{
        path: "/npc/:id",
        name: "npc",
        component: Npc,
    },
	{
        path: "/items/:id",
        name: "item",
        component: Item,
    },
	{
        path: "*",
        name: "not-found",
        component: NotFound,
        meta:{
            isAnonymous:true
        }
    },
	{
        path: "/register",
        name: "register",
        component: Register,
        meta:{
            isAnonymous:true
        }
    }
];

const router: VueRouter = new VueRouter({
    routes, 
    mode:"history",
    linkActiveClass: "active"
});

router.beforeEach((to, from, next) => {
    if (!auth.isAuthenticated && to.path != "/login" && !to.meta.isAnonymous) {
        next("/login");
        console.log("Route","Not authenticated.");
        return;
    }

    next();
});

export default router;
