﻿using Gomez.Core.Web.Helpers;
using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.Socket.Helpers;
using Gomez.RiseOfHeroes.Socket.Hubs.Shared;
using Gomez.RiseOfHeroes.Socket.Services;
using Gomez.RiseOfHeroes.ViewModels.Hubs.Chat;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.Socket.Hubs
{
    [Authorize]
    public class ChatHub : MainHub<ChatHub>
    {
        private readonly IStringLocalizer<ChatHub> _localizer;
        private readonly static IConnectionMapping<Guid,UserInformationViewModel> _store =
            new ConnectionMapping<Guid, UserInformationViewModel>();
        public ChatHub(IServiceScopeFactory factory, IStringLocalizer<ChatHub> localizer) : 
            base(factory, _store)
        {
            _localizer = localizer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="caller">Refresh only for the caller</param>
        /// <returns></returns>
        private async Task RefreshUserListAsync(bool caller)
        {
            var (user, userId) = GetUser();
            if (user == null)
            {
                return;
            }

            var users = GetUserInSameLocation((user, userId))
                .Select(x => new { 
                    x.user.Name, 
                    x.user.CharacterId, 
                    UserId = x.userId,
                    IsSelected = false
                }).ToArray();

            if (caller)
            {
                await Clients.Caller
                    .SendAsync("userListRefreshed", users);
                return;
            }

            await Clients.Group(user.Location.ToString())
                .SendAsync("userListRefreshed", users);
        }

        public async Task HasJoined()
        {
            if (GetConnections().Count() == 1)
            {
                await NewMessage(_localizer["Has joined."]);
                await RefreshUserListAsync(false);
                return;
            }

            await RefreshUserListAsync(true);
        }

        public override async Task BeforeDisconnectAsync(Guid userId)
        {
            var user = _store.GetData(userId);
            await Clients.Group(user.Location.ToString())
                .SendAsync("messageReceived",
                user.CharacterId,
                user.Name,
                _sanitizer.ToSafeHtml(_localizer["Has left."]));
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            if (GetConnections().Count() == 1)
            {
                await NewMessage(_localizer["Has left."]);
                await RefreshUserListAsync(false);
            }

            await base.OnDisconnectedAsync(exception);
        }

        public async Task NewMessage(string message)
        {
            var (user, userId) = GetUser();
            if (user == null)
            {
                return;
            }

            await Clients.Group(user.Location.ToString())
                .SendAsync("messageReceived", 
                user.CharacterId,
                user.Name, 
                _sanitizer.ToSafeHtml(message),
                userId, false);
        }

        public async Task NewPrivateMessage(string[] userIds,string message)
        {
            var (user, userId) = GetUser();
            if (user == null)
            {
                return;
            }

            var connections = GetConnections(userIds)
                .Append(this.Context.ConnectionId).ToArray();

            await Clients.Clients(connections)
                .SendAsync("messageReceived",
                user.CharacterId,
                user.Name,
                _sanitizer.ToSafeHtml(message),
                userId, true);
        }
    }
}
