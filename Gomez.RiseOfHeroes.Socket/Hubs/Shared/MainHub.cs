﻿using Gomez.Core.Web.Helpers;
using Gomez.RiseOfHeroes.Socket.Helpers;
using Gomez.RiseOfHeroes.Socket.Services;
using Gomez.RiseOfHeroes.ViewModels.Hubs.Chat;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Gomez.RiseOfHeroes.Socket.Hubs.Shared
{
    public abstract class MainHub<T> : Hub where T : Hub
    {
        private readonly IConnectionMapping<Guid,UserInformationViewModel> _store;
        protected readonly SanitizeHelper _sanitizer = new SanitizeHelper();
        protected readonly ServiceHelper<T> _helper;

        protected MainHub(IServiceScopeFactory factory, IConnectionMapping<Guid, UserInformationViewModel> store)
        {
            _store = store;
            _sanitizer.MaxLength = 255;
            _sanitizer.StripLength = true;
            _helper = new ServiceHelper<T>(this, factory);
        }

        public async Task Ping()
        {
            Guid userId = new Guid(this.Context.UserIdentifier);
            var date = _store.GetExpireDate(userId);
            await this.Clients.Caller.SendAsync("pinged", date);
            var userIds = _store.GetExpireUsers().ToArray();
            foreach (var expireUserId in userIds)
            {
                await BeforeDisconnectAsync(expireUserId);
                _store.Remove(expireUserId);
            }
        }

        public virtual Task BeforeDisconnectAsync(Guid userId)
        {
            return Task.CompletedTask;
        }

        protected (GameService<T> game, IServiceScope scope) CreateGameService()
        {
            var (db, logger, localizerFactory, scope) = _helper.CreateService();
            return (new GameService<T>(db, localizerFactory, logger), scope);
        }

        protected (UserInformationViewModel user, Guid userId) GetUser()
        {
            Guid userId = new Guid(this.Context.UserIdentifier);
            var model = _store.GetData(userId);
            if (model == null)
            {
                this.Context.Abort();
                return default;
            }

            return (model,userId);
        }

        protected IEnumerable<string> GetConnections()
        {
            Guid userId = new Guid(this.Context.UserIdentifier);
            return _store.GetConnections(userId);
        }

        protected IEnumerable<(UserInformationViewModel user, Guid userId)> 
            GetUserInSameLocation((UserInformationViewModel user, Guid userId) currentUser)
        {
            var (user,userId) = currentUser;
            if(user == null)
            {
                yield break;
            }

            var collection = _store.Query
                .OrderBy(x => x.Data.Location)
                .Where(x => x.Data.Location == user.Location)
                .Select(x => new { User = x.Data, x.UserId })
                .OrderBy(x => x.User.Name);

            foreach (var item in collection)
            {
                yield return (item.User, item.UserId);
            }
        }

        protected IEnumerable<string> GetConnections(IEnumerable<string> userIds)
        {
            foreach (var strUserId in userIds)
            {
                if (Guid.TryParse(strUserId, out Guid targetUserId))
                {
                    var connections = _store.GetConnections(targetUserId);
                    foreach (var connection in connections)
                    {
                        yield return connection;
                    }
                }
            }
        }

        public override async Task OnConnectedAsync()
        {
            var (game, scope) = CreateGameService();
            using (scope)
            {
                Guid userId = new Guid(this.Context.UserIdentifier);
                var model = await game.GameHandler
                    .PlayerHandler.GetMinimumChatInfoAsync(userId);

                if(model == null)
                {
                    this.Context.Abort();
                    return;
                }

                _store.Add(userId, this.Context.ConnectionId, model);
                await _helper.AddToGroupAsync(model.Location.ToString());
            }

            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            Guid userId = new Guid(this.Context.UserIdentifier);
            var model = _store.GetData(userId);
            if(model != null)
            {
                await _helper.RemoveFromGroupAsync(model.Location.ToString());
            }

            _store.Remove(userId, this.Context.ConnectionId);
            await base.OnDisconnectedAsync(exception);
        }
    }
}
