﻿using Gomez.RiseOfHeroes.Socket.Helpers;
using Gomez.RiseOfHeroes.Socket.Hubs.Shared;
using Gomez.RiseOfHeroes.ViewModels.Hubs.Chat;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.Socket.Hubs
{
    [Authorize]
    public class GameHub : MainHub<GameHub>
    {
        private readonly static IConnectionMapping<Guid,UserInformationViewModel> _store =
            new ConnectionMapping<Guid, UserInformationViewModel>();

        public GameHub(IServiceScopeFactory factory) : base(factory, _store)
        {

        }

        public async Task ReplenishEnergy()
        {
            var (user, userId) = GetUser();
            if (user == null)
            {
                return;
            }

            var (game, scope) = CreateGameService();
            using (scope)
            {
                var vm = await game.GameHandler.PlayerHandler.ReplenishEnergyAsync(userId);
                if (vm != null)
                {
                    await Clients.Group(user.Location.ToString())
                        .SendAsync("replenishedEnergy",
                        user.CharacterId, userId, vm);
                }
            }
        }
    }
}
