﻿using Gomez.RiseOfHeroes.BusinessLogic.Areas.Game;
using Gomez.RiseOfHeroes.Models.Contexts;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace Gomez.RiseOfHeroes.Socket.Services
{
    public class GameService<THub> where THub : Hub
    {
        protected readonly GameHandler _gameHandler;
        public GameHandler GameHandler => _gameHandler;

        public GameService(ApplicationDbContext db,
            IStringLocalizerFactory localizerFactory,
            ILogger<THub> logger)
        {
            _gameHandler = new GameHandler(db, localizerFactory);
        }

    }
}
