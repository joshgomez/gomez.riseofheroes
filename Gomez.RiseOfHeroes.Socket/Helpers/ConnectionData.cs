﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.Socket.Helpers
{
    public class ConnectionData<T,TModel> : IConnectionData<T,TModel>
    {
        public ConnectionData()
        {
            Connections = new HashSet<string>();
            ExpireAt = DateTime.UtcNow.AddMinutes(5);
        }

        public T UserId { get; set; }
        public TModel Data { get; set; }
        public HashSet<string> Connections { get; set; }
        public DateTime ExpireAt { get; set; }
    }
}
