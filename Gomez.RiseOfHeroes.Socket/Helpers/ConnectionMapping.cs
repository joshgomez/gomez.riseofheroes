﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Gomez.RiseOfHeroes.Socket.Helpers
{
    public class ConnectionMapping<T, TModel> : IConnectionMapping<T, TModel>
    {
        private readonly Dictionary<T, ConnectionData<T,TModel>> _store;

        public ConnectionMapping()
        {
            _store = new Dictionary<T, ConnectionData<T,TModel>>();
        }

        public int Count
        {
            get
            {
                return _store.Count;
            }
        }

        public IQueryable<ConnectionData<T,TModel>> Query => _store.Select(x => x.Value).AsQueryable();
 

        public void Add(T userId, string connectionId, TModel data)
        {
            lock (_store)
            {
                if (!_store.TryGetValue(userId, out ConnectionData<T,TModel> store))
                {
                    store = new ConnectionData<T,TModel>
                    {
                        UserId = userId,
                        Data = data
                    };

                    _store.Add(userId, store);
                }
                else
                {
                    ExtendExpireDate(ref store);
                }

                lock (store)
                {
                    store.Connections.Add(connectionId);
                }
            }
        }

        private void ExtendExpireDate(ref ConnectionData<T,TModel> store)
        {
            lock (store)
            {
                var currentTime = DateTime.UtcNow;
                store.ExpireAt = currentTime.AddMinutes(5);
            }
        }

        public IEnumerable<string> GetConnections(T userId)
        {
            if (_store.TryGetValue(userId, out ConnectionData<T,TModel> store))
            {
                ExtendExpireDate(ref store);
                return store.Connections;
            }

            return Enumerable.Empty<string>();
        }

        public TModel GetData(T userId)
        {
            if (_store.TryGetValue(userId, out ConnectionData<T,TModel> store))
            {
                ExtendExpireDate(ref store);
                return store.Data;
            }

            return default;
        }

        public DateTime? GetExpireDate(T userId)
        {
            if (_store.TryGetValue(userId, out ConnectionData<T, TModel> store))
            {
                ExtendExpireDate(ref store);
                return store.ExpireAt;
            }

            return null;
        }

        public IEnumerable<T> GetExpireUsers()
        {
            var currentTime = DateTime.UtcNow;
            return _store.Values.Where(x => currentTime > x.ExpireAt)
                .Select(x => x.UserId);
        }

        public void Remove(T userId)
        {
            Remove(userId, null);
        }

        public void Remove(T userId, string connectionId)
        {
            lock (_store)
            {
                if (!_store.TryGetValue(userId, out ConnectionData<T,TModel> store))
                {
                    return;
                }

                lock (store)
                {
                    if (String.IsNullOrEmpty(connectionId))
                    {
                        _store.Remove(userId);
                        return;
                    }

                    store.Connections.Remove(connectionId);
                    if (store.Connections.Count == 0)
                    {
                        _store.Remove(userId);
                    }
                }
            }
        }
    }
}
