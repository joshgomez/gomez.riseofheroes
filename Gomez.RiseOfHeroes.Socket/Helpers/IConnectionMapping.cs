﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Gomez.RiseOfHeroes.Socket.Helpers
{
    public interface IConnectionMapping<T, TModel>
    {
        int Count { get; }
        IQueryable<ConnectionData<T, TModel>> Query { get; }

        void Add(T userId, string connectionId, TModel data);
        IEnumerable<string> GetConnections(T userId);
        TModel GetData(T userId);
        DateTime? GetExpireDate(T userId);
        IEnumerable<T> GetExpireUsers();

        void Remove(T userId);
        void Remove(T userId, string connectionId);
    }
}