﻿using Gomez.RiseOfHeroes.Models.Contexts;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.Socket.Helpers
{
    public class ServiceHelper<T> where T : Hub
    {
        private readonly Hub _hub;
        private readonly IServiceScopeFactory _factory;
        public ServiceHelper(Hub hub, IServiceScopeFactory factory)
        {
            _factory = factory;
            _hub = hub;
        }

        public (ApplicationDbContext db, ILogger<T> logger, IStringLocalizerFactory localizerFactory, IServiceScope scope) 
            CreateService()
        {
            var serviceScope = _factory.CreateScope();
            var services = serviceScope.ServiceProvider;
            var db = services.GetRequiredService<ApplicationDbContext>();
            var localizerFactory = services.GetRequiredService<IStringLocalizerFactory>();
            var logger = services.GetRequiredService<ILogger<T>>();
            return (db, logger, localizerFactory, serviceScope);
        }

        public async Task AddToGroupAsync(string groupName)
        {
            await _hub.Groups.AddToGroupAsync(_hub.Context.ConnectionId, groupName);
        }

        public async Task RemoveFromGroupAsync(string groupName)
        {
            await _hub.Groups.RemoveFromGroupAsync(_hub.Context.ConnectionId, groupName);
        }
    }
}
