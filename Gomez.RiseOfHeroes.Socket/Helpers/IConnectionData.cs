﻿using System;
using System.Collections.Generic;

namespace Gomez.RiseOfHeroes.Socket.Helpers
{
    public interface IConnectionData<T,TModel>
    {
        T UserId { get; set; }
        HashSet<string> Connections { get; set; }
        TModel Data { get; set; }
        DateTime ExpireAt { get; set; }
    }
}