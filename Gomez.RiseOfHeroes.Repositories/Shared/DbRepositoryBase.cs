﻿using Gomez.RiseOfHeroes.Models.Contexts;
using Microsoft.Extensions.Localization;

namespace Gomez.RiseOfHeroes.Repositories.Shared
{
    public abstract class DbRepositoryBase<T> : Gomez.Core.Ef.Repositories.DbRepositoryBase<ApplicationDbContext, T> where T : class
    {
        protected DbRepositoryBase(ApplicationDbContext db, IStringLocalizerFactory localizerFactory) : base(db, localizerFactory)
        {
        }
    }

    public abstract class DbRepositoryBase : Gomez.Core.Ef.Repositories.DbRepositoryBase<ApplicationDbContext>
    {
        protected DbRepositoryBase(ApplicationDbContext db, IStringLocalizerFactory localizerFactory) : base(db, localizerFactory)
        {
        }
    }
}
