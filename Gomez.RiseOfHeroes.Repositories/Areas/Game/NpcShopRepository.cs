﻿using Gomez.Core.Ef.Database;
using Gomez.Core.Ef.Models.Entity;
using Gomez.Core.ViewModels.Shared;
using Gomez.RiseOfHeroes.Calculations;
using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using Gomez.RiseOfHeroes.Models.Areas.Game;
using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.Repositories.Areas.Game.Shared;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcShops;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Gomez.RiseOfHeroes.Repositories.Areas.Game
{
    public class NpcShopRepository : CharacterRepository
    {
        public NpcShopRepository(ApplicationDbContext db, IStringLocalizerFactory localizerFactory) : base(db, localizerFactory)
        {
            Query = Query.Where(x => x.Type == CharacterConstant.TYPE_NPC_SHOP);
        }

        public override async Task<GetViewModel> GetWithDetailsAsync(GetViewModel vm)
        {
            vm = await base.GetWithDetailsAsync(vm);
            if (vm.PropertyCollection != null)
            {
                var calculation = new NpcShopCalculation();
                calculation.Init(vm.PropertyCollection);
                vm.DynamicProperties.Add(nameof(CharacterCalculation), calculation);
            }

            return vm;
        }

        public async Task<InventoryViewModel> GetInventoryAsync(InventoryViewModel vm)
        {
            vm.Items = await _db.Items.Where(x => x.CharacterId == vm.Id)
                .Select(x => new ViewModels.Areas.Game.NpcShops.ItemIndexViewModel { 
                    Id = x.Id,
                    Name = x.Detail.Name,
                    Quantity = x.Quantity,
                    State = x.State,
                    Cost = x.Detail.Properties
                        .Where(z => z.Key == ItemConstant.KEY_STATS_COST)
                        .Select(z => z.ValueDecimal).FirstOrDefault() ?? 0m,
                    HasImage = x.Detail.Properties
                        .Where(z => z.Key == ItemConstant.KEY_HAS_IMAGE)
                        .Select(z => z.ValueBool).FirstOrDefault() ?? false,
                    DetailId = x.DetailId
                })
                .OrderBy(x => x.Name).GetPagedAsync(vm.Page, vm.Rows);

            return vm;
        }

        public async Task SellAsync(ValidationViewModel<SellInternalViewModel> vm, bool saveChanges = true)
        {
            if (vm.IsValid)
            {
                await SetAsync(vm.Model.SellersId, saveChanges, (CharacterConstant.KEY_MONEY, 
                    vm.Model.SellersMoney + vm.Model.Cost, vm.Model.SellersMoneyVersion));
            }
        }

        public async Task BuyAsync(ValidationViewModel<SellInternalViewModel> vm, bool saveChanges = true)
        {
            if (vm.IsValid)
            {
                await SetAsync(vm.Model.BuyersId, saveChanges, (CharacterConstant.KEY_MONEY,
                    vm.Model.BuyersMoney - vm.Model.Cost, vm.Model.BuyersMoneyVersion));
            }
        }

        private class ShopData
        {
            public ShopData(decimal money, byte[] version)
            {
                CurrentMoney = money;
                Version = version;
            }

            public decimal CurrentMoney { get; set; }
            public byte[] Version { get; set; }
            public decimal Cost { get; set; }
            public decimal NewMoney => CurrentMoney - Cost;
        }

        private class ItemData
        {
            public ItemData(int currentQty, int qty, byte[] version)
            {
                CurrentQty = currentQty;
                Qty = qty;
                Version = version;
            }

            public int CurrentQty { get; set; }
            public int Qty { get; private set; }
            public byte[] Version { get; set; }

            public int NewQty => CurrentQty + Qty;
        }

        /// <summary>
        /// Reflect all users logged in on current day, 
        /// the shop will only refill/resell to match the demand of active users.
        /// </summary>
        /// <returns></returns>
        public async Task<int> GetRequiredQtyForActiveUsersAsync()
        {
            var DbF = EF.Functions;
            var currentDay = (DateTime?)DateTime.UtcNow.Date;
            return await _db.Users.Where(x => DbF.DateDiffDay(x.LastLoginAt, currentDay) == 0).CountAsync() * NpcShopConstant.DEMAND_MULTIPLIER;
        }

        private async Task SetShopDataAsync(int currentShopId, ItemStockViewModel item, Dictionary<int, ShopData> shopData)
        {
            if (currentShopId != item.CharacterId)
            {
                currentShopId = item.CharacterId;
                var info = await _db.CharacterProperties.Where(x => x.CharacterId == currentShopId && x.Key == CharacterConstant.KEY_MONEY)
                    .Select(x => new { ShopMoney = x.ValueDecimal, x.RowVersion })
                    .FirstOrDefaultAsync();

                if (info != null && !shopData.ContainsKey(currentShopId))
                {
                    shopData[currentShopId] = new ShopData(info.ShopMoney ?? 0, info.RowVersion);
                }
            }
        }

        /// <summary>
        /// Get the required data for refilling/reselling
        /// </summary>
        /// <param name="itemState"></param>
        /// <returns></returns>
        public IQueryable<ItemStockViewModel> GetQueryForItemStock(Models.Areas.Game.Enums.ItemState itemState)
        {
            var charIdsQuery = Query.Select(x => x.Id);
            return _db.Items.
                Where(x => charIdsQuery.Contains(x.CharacterId) && x.State == itemState)
                .Select(x => new ItemStockViewModel
                {
                    Id = x.Id,
                    Quantity = x.Quantity,
                    CharacterId = x.CharacterId,
                    Cost = x.Detail.Properties.Where(z => z.Key == ItemConstant.KEY_STATS_COST).Select(z => z.ValueDecimal).FirstOrDefault() ?? 0,
                    RowVersion = x.RowVersion
                }).OrderBy(x => x.CharacterId).ThenBy(x => x.Quantity);
        }

        /// <summary>
        /// Update npc money for refilling or reselling stock.
        /// </summary>
        /// <param name="currentShopId"></param>
        /// <param name="shopData"></param>
        /// <returns></returns>
        private async Task<int> UpdateNpcMoneyForStockAsync(int currentShopId, Dictionary<int, ShopData> shopData)
        {
            var updated = 0;
            foreach (var sItem in shopData)
            {
                updated = await _db.CharacterProperties.Where(x => x.CharacterId == sItem.Key &&
                x.Key == CharacterConstant.KEY_MONEY && x.RowVersion == sItem.Value.Version)
                    .UpdateAsync(x => new CharacterProperty() { ValueDecimal = sItem.Value.NewMoney });
                if (updated == 0)
                {
                    //concurrency check
                    var info = await _db.CharacterProperties.Where(x => x.CharacterId == currentShopId && x.Key == CharacterConstant.KEY_MONEY)
                        .Select(x => new { ShopMoney = x.ValueDecimal, x.RowVersion })
                        .FirstOrDefaultAsync();

                    if (info != null)
                    {
                        sItem.Value.CurrentMoney = info.ShopMoney ?? 0; //Refreshing money from db.
                        updated = await _db.CharacterProperties.Where(x => x.CharacterId == sItem.Key &&
                        x.Key == CharacterConstant.KEY_MONEY && x.RowVersion == info.RowVersion)
                            .UpdateAsync(x => new CharacterProperty() { ValueDecimal = sItem.Value.NewMoney });
                    }

                    if (updated == 0)
                    {
                        //Could not update, concurrency error after another try. Will try again on next cronjob.
                        break;
                    }
                }
            }

            return updated;
        }

        /// <summary>
        /// Update qty for selling/refilling the stock
        /// </summary>
        /// <param name="updated"></param>
        /// <param name="itemData"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        private async Task<int> UpdateNpcStockItemForStockAsync(int updated, Dictionary<long, ItemData> itemData,
            Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction transaction)
        {
            if (updated != 0)
            {
                foreach (var iItem in itemData)
                {
                    updated = await _db.Items
                        .Where(x => x.Id == iItem.Key && x.RowVersion == iItem.Value.Version)
                        .UpdateAsync(x => new Item { Quantity = iItem.Value.NewQty });

                    if (updated == 0)
                    {
                        //concurrency check
                        var info = await _db.Items.Where(x => x.Id == iItem.Key)
                            .Select(x => new { x.Quantity, x.RowVersion })
                            .FirstOrDefaultAsync();
                        if (info != null)
                        {
                            iItem.Value.CurrentQty = info.Quantity; //refreshing qty from database
                            updated = await _db.Items
                                .Where(x => x.Id == iItem.Key && x.RowVersion == info.RowVersion)
                                .UpdateAsync(x => new Item { Quantity = iItem.Value.NewQty });
                        }

                        if (updated == 0)
                        {
                            //Could not update, concurrency error after second try. Will try again on next cronjob.
                            await transaction.RollbackAsync();
                            break;
                        }
                    }
                }
            }

            return updated;
        }

        /// <summary>
        /// Refilling items to the shop if qty is too low, concurrency safe
        /// </summary>
        /// <returns></returns>
        public async Task<int> RefillStockForAllAsync()
        {

            var qty = await GetRequiredQtyForActiveUsersAsync();
            var query = GetQueryForItemStock(Models.Areas.Game.Enums.ItemState.ShopSale);

            int currentShopId = -1;
            int updates = 0;
            var helper = await BatchHelper<ItemStockViewModel>.CreateAsync(query, 1, 200);

            var shopData = new Dictionary<int, ShopData>();
            var itemData = new Dictionary<long, ItemData>();
            for (int i = 1; i <= helper.TotalPages; i++)
            {
                var items = await helper.GetBatch(i).ToArrayAsync();
                shopData.Clear();
                itemData.Clear();

                foreach (var item in items)
                {
                    int qtyToBuy = qty - item.Quantity;
                    if (qtyToBuy < 1) continue; //has enough qty
                    decimal cost = qtyToBuy * item.Cost * 0.70m;

                    await SetShopDataAsync(currentShopId, item, shopData);
                    if (cost > shopData[currentShopId].CurrentMoney) continue; // cannot buy batch

                    shopData[currentShopId].Cost += cost;
                    itemData[item.Id] = new ItemData(item.Quantity, qtyToBuy, item.RowVersion);
                }

                //Begin saving the batch
                using var transaction = await _db.Database.BeginTransactionAsync();
                try
                {
                    var updated = await UpdateNpcMoneyForStockAsync(currentShopId, shopData);
                    updated = await UpdateNpcStockItemForStockAsync(updated, itemData, transaction);

                    if (updated != 0)
                    {
                        await transaction.CommitAsync();
                        updates++;
                    }
                }
                catch
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }

            return updates;
        }

        /// <summary>
        /// Sell stock if qty of the items are to high
        /// </summary>
        /// <returns></returns>
        public async Task<int> ResellStockForAllAsync()
        {
            var qty = await GetRequiredQtyForActiveUsersAsync();
            var query = GetQueryForItemStock(Models.Areas.Game.Enums.ItemState.ShopSecondHand);

            int currentShopId = -1;
            int updates = 0;
            var helper = await BatchHelper<ItemStockViewModel>.CreateAsync(query, 1, 200);

            var shopData = new Dictionary<int, ShopData>();
            var itemData = new Dictionary<long, ItemData>();
            for (int i = 1; i <= helper.TotalPages; i++)
            {
                var items = await helper.GetBatch(i).ToArrayAsync();
                shopData.Clear();
                itemData.Clear();

                foreach (var item in items)
                {
                    int qtyToSell = 0;
                    if(item.Quantity > qty)
                    {
                        qtyToSell = item.Quantity - qty;
                    }

                    if (qtyToSell < 1) continue; //has enough qty
                    decimal sellPrice = qtyToSell * item.Cost;

                    await SetShopDataAsync(currentShopId, item, shopData);

                    shopData[currentShopId].Cost -= sellPrice;
                    itemData[item.Id] = new ItemData(item.Quantity, -qtyToSell, item.RowVersion);
                }

                //Begin saving the batch
                using var transaction = await _db.Database.BeginTransactionAsync();
                try
                {
                    var updated = await UpdateNpcMoneyForStockAsync(currentShopId, shopData);
                    updated = await UpdateNpcStockItemForStockAsync(updated, itemData, transaction);
                    if (updated != 0)
                    {
                        await transaction.CommitAsync();
                        updates++;
                    }
                }
                catch
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }

            return updates;
        }
    }
}
