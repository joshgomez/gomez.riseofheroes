﻿using AutoMapper;
using Gomez.Core.Ef.Models.Entity;
using Gomez.Core.Models.Exceptions;
using Gomez.Core.Models.Shared;
using Gomez.Core.Utilities;
using Gomez.Core.ViewModels.Shared;
using Gomez.RiseOfHeroes.Calculations;
using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using Gomez.RiseOfHeroes.Models.Areas.Game;
using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.Repositories.Areas.Game.Shared;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Players;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using NetTopologySuite.Geometries;
using System;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;
using CoreConstants = Gomez.Core.Configurations.Constants;
using NpcShopsVm = Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcShops;

namespace Gomez.RiseOfHeroes.Repositories.Areas.Game
{
    public class PlayerRepository : CharacterRepository
    {
        public PlayerRepository(ApplicationDbContext db, IStringLocalizerFactory localizerFactory) : base(db, localizerFactory)
        {
            SetMapper(nameof(PutViewModel), new MapperConfiguration(cfg => cfg.CreateMap<Character, PutViewModel>()));
            Query = Query.Where(x => x.Type == CharacterConstant.TYPE_PLAYER);
        }

        private async Task InitDefaultPropertiesAsync(Character model)
        {
            await _db.Entry(model)
                .Collection(b => b.Properties)
                .LoadAsync();

            model.SetProperty(PlayerConstant.KEY_STATS_EXPERIENCE, 0);
            model.SetProperty(CharacterConstant.KEY_STATS_LEVEL, 1);
            model.SetProperty(CharacterConstant.KEY_MONEY, 1000m);


            model.SetProperty(CharacterConstant.KEY_ATTR_STRENGTH, 5);
            model.SetProperty(CharacterConstant.KEY_ATTR_CONSTITUTION, 5);
            model.SetProperty(CharacterConstant.KEY_ATTR_DEXTERITY, 5);
            model.SetProperty(CharacterConstant.KEY_ATTR_INTELLIGENCE, 5);

            model.SetProperty(PlayerConstant.KEY_STATS_ATTRIBUTEPOINTS, 5);

            var calculation = new PlayerCalculation();
            calculation.Init(PropsToSimpleCollection(model.Properties));

            model.SetProperty(CharacterConstant.KEY_STATS_HEALTH, calculation.MaxHealth);
            model.SetProperty(CharacterConstant.KEY_STATS_ENERGY, calculation.MaxEnergy);
        }

        public virtual async Task<bool> SaveFromViewModelAsync(CharacterViewModel vm)
        {
            var playCalc = vm.Calculation as PlayerCalculation;
            await SetAsync(vm.Id,
                    (CharacterConstant.KEY_STATS_ENERGY, playCalc.Energy, playCalc.RowVersions[CharacterConstant.KEY_STATS_ENERGY]),
                    (CharacterConstant.KEY_STATS_HEALTH, playCalc.Health, playCalc.RowVersions[CharacterConstant.KEY_STATS_HEALTH]),
                    (CharacterConstant.KEY_MONEY, playCalc.Money, playCalc.RowVersions[CharacterConstant.KEY_MONEY]),
                    (PlayerConstant.KEY_STATS_EXPERIENCE, playCalc.Experience, playCalc.RowVersions[PlayerConstant.KEY_STATS_EXPERIENCE]),
                    (CharacterConstant.KEY_STATS_LEVEL, playCalc.Level, playCalc.RowVersions[CharacterConstant.KEY_STATS_LEVEL]),
                    (PlayerConstant.KEY_STATS_ATTRIBUTEPOINTS, playCalc.Attributes, playCalc.RowVersions[PlayerConstant.KEY_STATS_ATTRIBUTEPOINTS])
                );

            return false;
        }

        private async Task CreateAsync(Guid userId)
        {
            var model = new Character
            {
                UserId = userId,
                Type = CharacterConstant.TYPE_PLAYER,
                Location = new Point(0,0)
            };

            await _db.Characters.AddAsync(model);
            await InitDefaultPropertiesAsync(model);

            await _db.SaveChangesAsync();
        }

        private async Task<bool> AnyByUserIdAsyc(Guid userId)
        {
            return await Query.AnyAsync(x => x.UserId == userId);
        }

        public async Task<int> GetIdByUserIdAsyc(Guid userId)
        {
            return await Query.Where(x => x.UserId == userId).Select(x => x.Id).FirstOrDefaultAsync();
        }

        public async Task CreateCharacterIfNullAsync(Guid userId)
        {
            if (!await this.AnyByUserIdAsyc(userId))
            {
                await this.CreateAsync(userId);
            }
        }

        public async Task<bool> SetBasicAsync(Guid userId, QuerySetBasicViewModel vm)
        {
            if (String.IsNullOrEmpty(vm.Name))
            {
                throw new BadRequestException(nameof(vm.Name));
            }

            if (vm.Name.Length <= 1 || vm.Name.Length > 32)
            {
                throw new OutOfBoundException($"Min: 2, Max:32",nameof(vm.Name));
            }

            int id = await this.GetIdByUserIdAsyc(userId);
            if (id != 0)
            {
                return (await Query.Where(x => x.Id == id)
                    .UpdateAsync(x => new Character() { Name = vm.Name })) > 0;
            }

            return false;
        }

        public async Task<ReplenishEnergyViewModel> ReplenishEnergyAsync(Guid userId)
        {
            return await ReplishEnergyAsync(await GetIdByUserIdAsyc(userId));
        }

        public async Task<ReplenishEnergyViewModel> ReplishEnergyAsync(int id)
        {
            if (id == 0)
            {
                throw new NotFoundException(nameof(id));
            }

            var currentTime = DateTime.UtcNow;
            var vm = new ReplenishEnergyViewModel(currentTime);

            var propertyKeys = new string[] {
                PlayerConstant.KEY_TIMESTAMP_ENERGY,
                CharacterConstant.KEY_STATS_ENERGY
            };

            if(!await Query.AnyAsync(x => x.Id == id))
            {
                throw new NotFoundException(nameof(id));
            }

            var currentProperties = await _db.CharacterProperties
                .Where(x => x.CharacterId == id
                    && propertyKeys.Contains(x.Key))
                .Select(x => new { x.Key, x.ValueDouble, x.ValueDateTime, x.RowVersion })
                .ToDictionaryAsync(x => x.Key, x => new { x.ValueDouble, x.ValueDateTime, x.RowVersion });
            var energyProp = currentProperties[CharacterConstant.KEY_STATS_ENERGY];
            if (energyProp.ValueDouble >= CharacterCalculation.BASE_ENERGY)
            {
                vm.NextUpdateAt = currentTime.AddMinutes(1);
                return vm;
            }

            var lastTime = currentProperties
                .ContainsKey(PlayerConstant.KEY_TIMESTAMP_ENERGY) ?
                    currentProperties[PlayerConstant.KEY_TIMESTAMP_ENERGY].ValueDateTime : null;

            using var transaction = await _db.Database.BeginTransactionAsync();
            try
            {
                if (lastTime == null)
                {
                    _db.CharacterProperties.Add(new CharacterProperty(PlayerConstant.KEY_TIMESTAMP_ENERGY, currentTime)
                    { 
                     CharacterId = id
                    });

                    await _db.SaveChangesAsync();
                    await transaction.CommitAsync();
                    vm.NextUpdateAt = currentTime.AddMinutes(1);
                    return vm;
                }

                var energy = (currentTime - lastTime.Value).TotalMinutes / 100d;
                if(energy < 0.01)
                {
                    vm.NextUpdateAt = lastTime.Value.AddMinutes(1);
                    return vm;
                }

                if (await _db.CharacterProperties
                    .Where(x => x.CharacterId == id
                    && x.Key == PlayerConstant.KEY_TIMESTAMP_ENERGY
                    && x.RowVersion == currentProperties[PlayerConstant.KEY_TIMESTAMP_ENERGY].RowVersion)
                    .UpdateAsync(x => new CharacterProperty { ValueDateTime = currentTime }) == 0)
                {
                    throw new ConcurrencyException(nameof(PlayerConstant.KEY_TIMESTAMP_ENERGY));
                }

                
                var calc = new PlayerCalculation();
                calc.InitEnergy(energyProp.ValueDouble ?? 0, energyProp.RowVersion);
                calc.AddEnergy(energy);
                if (await _db.CharacterProperties
                    .Where(x => x.CharacterId == id
                    && x.Key == CharacterConstant.KEY_STATS_ENERGY
                    && x.RowVersion == currentProperties[CharacterConstant.KEY_STATS_ENERGY].RowVersion)
                    .UpdateAsync(x => new CharacterProperty { ValueDouble = calc.Energy }) == 0)
                {
                    throw new ConcurrencyException(nameof(CharacterConstant.KEY_STATS_ENERGY));
                }

                vm.NextUpdateAt = currentTime.AddMinutes(1);
                vm.Energy = calc.Energy;
                await _db.SaveChangesAsync();
                await transaction.CommitAsync();
                return vm;
            }
            catch
            {
                await transaction.RollbackAsync();
                throw;
            }
        }

        public async Task<bool> SetAttributesAsync(Guid userId, QuerySetAttributeViewModel vm)
        {
            int id = await this.GetIdByUserIdAsyc(userId);
            if (id != 0)
            {
                return await SetAttributesAsync(id,vm);
            }

            return false;
        }

        /// <summary>
        /// concurrency safe
        /// </summary>
        /// <param name="id"></param>
        /// <param name="vm"></param>
        /// <returns></returns>
        public async Task<bool> SetAttributesAsync(int id, QuerySetAttributeViewModel vm)
        {
            var propertyKeys = new string[] {
                CharacterConstant.KEY_ATTR_STRENGTH,
                CharacterConstant.KEY_ATTR_CONSTITUTION,
                CharacterConstant.KEY_ATTR_DEXTERITY,
                CharacterConstant.KEY_ATTR_INTELLIGENCE,
                PlayerConstant.KEY_STATS_ATTRIBUTEPOINTS
            };

            var currentProperties = await _db.CharacterProperties
                .Where(x => x.CharacterId == id
                    && propertyKeys.Contains(x.Key))
                .Select(x => new { x.Key, x.ValueInt32, x.RowVersion })
                .ToDictionaryAsync(x => x.Key,x => new { x.ValueInt32, x.RowVersion });

            if(currentProperties.ContainsKey(PlayerConstant.KEY_STATS_ATTRIBUTEPOINTS) && currentProperties[PlayerConstant.KEY_STATS_ATTRIBUTEPOINTS].ValueInt32 >= vm.Total)
            {
                string attributePrefix = "Attr.";
                using var transaction = await _db.Database.BeginTransactionAsync();
                try
                {
                    foreach (var propertyKey in propertyKeys.Where(x => x.StartsWith(attributePrefix)))
                    {
                        string propertyName = propertyKey.Substring(attributePrefix.Length);
                        int valueToAdd = ClassUtility.GetPropertyValue<int>(vm, propertyName);
                        if(valueToAdd < 0)
                        {
                            valueToAdd = 0;
                        }

                        if(valueToAdd == 0)
                        {
                            continue;
                        }

                        var prop = currentProperties[propertyKey];
                        int newValue = (prop.ValueInt32 + valueToAdd) ?? 0;
                        int rows = await _db.CharacterProperties
                            .Where(x => x.CharacterId == id && x.Key == propertyKey && x.RowVersion == prop.RowVersion)
                            .UpdateAsync(x => new CharacterProperty() { ValueInt32 = newValue });

                        if(rows == 0)
                        {
                            throw new UnavailableException();
                        }
                    }

                    var apProp = currentProperties[PlayerConstant.KEY_STATS_ATTRIBUTEPOINTS];
                    int attributePoints = (apProp.ValueInt32 - vm.Total) ?? 0;
                    var updated = await _db.CharacterProperties.Where(x => x.CharacterId == id 
                        && x.Key == PlayerConstant.KEY_STATS_ATTRIBUTEPOINTS
                        && x.RowVersion == apProp.RowVersion)
                        .UpdateAsync(x => new CharacterProperty() { ValueInt32 = attributePoints });
                    if (updated > 0)
                    {
                        await transaction.CommitAsync();
                        return true;
                    }

                    await transaction.RollbackAsync();
                }
                catch
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }

            return false;
        }

        public async Task<bool> DeleteAsync(Guid userId)
        {
            int id = await this.GetIdByUserIdAsyc(userId);
            if (id != 0)
            {
                return await base.DeleteAsync(id);
            }

            return false;
        }

        public async Task<MinimumInformationViewModel> GetMinimumInfoAsync(Guid userId)
        {
            int id = await GetIdByUserIdAsyc(userId);
            return await GetMinimumInfoAsync(id);
        }

        public async Task<GetViewModel> GetWithDetailsAsync(Guid userId, GetViewModel vm)
        {
            vm.Id = await GetIdByUserIdAsyc(userId);
            return await GetWithDetailsAsync(vm);
        }

        public virtual async Task PutAsync(Guid userId, PutViewModel vm)
        {
            vm.Id = vm.Id > 0 ? vm.Id : await GetIdByUserIdAsyc(userId);
            Character model = await Query.Where(x => x.UserId == userId && x.Id == vm.Id)
                .FirstOrDefaultAsync();
            if (model == null)
            {
                return;
            }

            ValidateViewModel(vm);

            model.Name = vm.Name;
            await _db.SaveChangesAsync();
        }

        /// <summary>
        /// Throw exception if view model has invalid requests.
        /// </summary>
        /// <param name="vm"></param>
        private void ValidateViewModel(PutViewModel vm)
        {
            if (String.IsNullOrEmpty(vm.Name) || vm.Name.Length <= CoreConstants.Validation.NAME_MIN_LENGHT
                || vm.Name.Length > CoreConstants.Validation.NAME_MAX_LENGHT)
            {
                throw new BadRequestException(nameof(vm.Name));
            }
        }

        public override async Task<GetViewModel> GetWithDetailsAsync(GetViewModel vm)
        {
            vm = await base.GetWithDetailsAsync(vm);
            if (vm.PropertyCollection != null)
            {
                var calculation = new PlayerCalculation();
                calculation.Init(vm.PropertyCollection);
                vm.DynamicProperties.Add(nameof(CharacterCalculation), calculation);
            }

            return vm;
        }

        public async Task BuyAsync(ValidationViewModel<NpcShopsVm.SellInternalViewModel> vm, bool saveChanges = true)
        {
            if (vm.IsValid)
            {
                await SetAsync(vm.Model.BuyersId, saveChanges, (CharacterConstant.KEY_MONEY, 
                    vm.Model.BuyersMoney - vm.Model.Cost, vm.Model.BuyersMoneyVersion));
            }
        }

        public async Task SellAsync(ValidationViewModel<NpcShopsVm.SellInternalViewModel> vm, bool saveChanges = true)
        {
            if (vm.IsValid)
            {
                await SetAsync(vm.Model.SellersId, saveChanges, (CharacterConstant.KEY_MONEY,
                    vm.Model.SellersMoney + vm.Model.Cost, vm.Model.SellersMoneyVersion));
            }
        }

        public async Task<InventoryViewModel> GetInventoryAsync(Guid userId,InventoryViewModel vm)
        {
            vm.Id = await GetIdByUserIdAsyc(userId);
            return await GetInventoryAsync(vm);
        }

        public async Task<InventoryViewModel> GetInventoryAsync(InventoryViewModel vm)
        {
            vm.Items = await _db.Items.Where(x => x.CharacterId == vm.Id)
                .Select(x => new ViewModels.Areas.Game.Players.ItemIndexViewModel
                {
                    Id = x.Id,
                    Name = x.Detail.Name,
                    Quantity = x.Quantity,
                    State = x.State,
                    Weight = x.Detail.Properties
                        .Where(z => z.Key == ItemConstant.KEY_STATS_WEIGHT)
                        .Select(z => z.ValueDouble).FirstOrDefault() ?? 0d,
                    HasImage = x.Detail.Properties
                        .Where(z => z.Key == ItemConstant.KEY_HAS_IMAGE)
                        .Select(z => z.ValueBool).FirstOrDefault() ?? false,
                    DetailId = x.DetailId
                })
                .OrderBy(x => x.Name).GetPagedAsync(vm.Page, vm.Rows);

            return vm;
        }

        public async Task<NpcShopsVm.PlayerInventoryViewModel> GetSaleInventoryAsync(Guid userId, NpcShopsVm.PlayerInventoryViewModel vm)
        {
            vm.PlayerId = await GetIdByUserIdAsyc(userId);
            return await GetSaleInventoryAsync(vm);
        }

        public async Task<NpcShopsVm.PlayerInventoryViewModel> GetSaleInventoryAsync(NpcShopsVm.PlayerInventoryViewModel vm)
        {
            var strCanBuyTypes = await _db.CharacterProperties
                .Where(x => x.Key == NpcShopConstant.KEY_CAN_BUY_TYPES && x.CharacterId == vm.Id)
                .Select(x => x.ValueString).FirstOrDefaultAsync();
            if (string.IsNullOrEmpty(strCanBuyTypes))
            {
                return vm;
            }

            int[] itemTypes = strCanBuyTypes.Split(',').Select(x => int.Parse(x)).ToArray();

            vm.Items = await _db.Items.Where(x => x.CharacterId == vm.PlayerId 
                && itemTypes.Contains(x.Detail.Type))
                .Select(x => new NpcShopsVm.ItemIndexViewModel
                {
                    Id = x.Id,
                    Name = x.Detail.Name,
                    Quantity = x.Quantity,
                    State = x.State,
                    Cost = x.Detail.Properties
                        .Where(z => z.Key == ItemConstant.KEY_STATS_COST)
                        .Select(z => z.ValueDecimal).FirstOrDefault() ?? 0m,
                    HasImage = x.Detail.Properties
                        .Where(z => z.Key == ItemConstant.KEY_HAS_IMAGE)
                        .Select(z => z.ValueBool).FirstOrDefault() ?? false,
                    DetailId = x.DetailId
                })
                .OrderBy(x => x.Name).GetPagedAsync(vm.Page, vm.Rows);

            return vm;
        }

        /// <summary>
        /// Travel by money, fail on concurency conflict.
        /// </summary>
        /// <param name="characterVm"></param>
        /// <param name="cost"></param>
        /// <param name="coord"></param>
        /// <returns></returns>
        public async Task<bool> TravelAsync(GetViewModel characterVm, decimal cost, ICoordinate coord)
        {
            using var transaction = await _db.Database.BeginTransactionAsync();
            var calc = characterVm.GetPropertyByType<CharacterCalculation>() as PlayerCalculation;
            if (cost > calc.Money)
            {
                throw new NotEnoughException();
            }

            try
            {
                var point = new Point(coord.X, coord.Y);
                await _db.Characters.Where(x => x.Id == characterVm.Character.Id).UpdateAsync(x => new Character { Location = point });
                await this.SetSinglePropertyAsync(characterVm.Character.Id, CharacterConstant.KEY_MONEY, calc.Money - cost, 
                    rowVersion: calc.RowVersions[CharacterConstant.KEY_MONEY]);
                await transaction.CommitAsync();
                return true;
            }
            catch
            {
                await transaction.RollbackAsync();
                throw;
            }
        }
    }
}
