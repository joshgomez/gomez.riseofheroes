﻿using AutoMapper;
using Gomez.Core.Models.Exceptions;
using Gomez.Core.ViewModels.Shared;
using Gomez.RiseOfHeroes.Models.Areas.Game;
using Gomez.RiseOfHeroes.Models.Areas.Game.Enums;
using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.Repositories.Shared;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Items;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Gomez.RiseOfHeroes.Repositories.Areas.Game
{
    public class ItemRepository : DbRepositoryBase<Item>
    {
        public ItemDetailRepository DetailRepo { get; private set; }

        private void SetMappers()
        {
            SetMapper(nameof(ItemBaseViewModel), new MapperConfiguration(
                cfg =>
                {
                    cfg.CreateMap<Item, ItemBaseViewModel>()
                    .ConstructUsing(x => new ItemBaseViewModel());
                }));
        }

        public ItemRepository(ApplicationDbContext db, IStringLocalizerFactory localizerFactory) : base(db, localizerFactory)
        {
            DetailRepo = new ItemDetailRepository(db, localizerFactory);
            SetMappers();
            Init();
        }

        public ItemRepository(ApplicationDbContext db, IStringLocalizerFactory localizerFactory, ItemDetailRepository detailRepo) : base(db, localizerFactory)
        {
            DetailRepo = detailRepo;
            SetMappers();
            Init();
        }

        public ItemBaseViewModel MapItemToViewModel(Item item)
        {
            var cfg = GetMapperCfg(nameof(ItemBaseViewModel));
            var mapper = new Mapper(cfg);
            return mapper.Map<ItemBaseViewModel>(item);
        }

        public async Task<bool> AnyAsyc(long id)
        {
            return await Query.AnyAsync(x => x.Id == id);
        }

        protected override IQueryable<Item> SearchQuery(IQueryable<Item> query, string term)
        {
            return query.Where(x => x.Detail.Name.Contains(term));
        }

        public async Task<Guid> GetDetailIdByIdAsync(long id)
        {
            return await Query.Where(x => x.Id == id).Select(x => x.DetailId).FirstOrDefaultAsync();
        }

        public async Task<Item> GetItemByDetailIdAsync(int characterId, Guid detailId, ItemState? state = null)
        {
            var query = Query.Where(x => x.CharacterId == characterId && x.DetailId == detailId);
            if(state != null)
            {
                query = query.Where(x => x.State == state);
            }


            return await query.FirstOrDefaultAsync();
        }

        public async Task<Item> AddItemByDetailAsync(Guid detailId, int qty, int characterId, Item to = null, bool saveChanges = true)
        {
            if (to != null && to.State == ItemState.Unequiped)
            {
                to.Quantity += qty;
            }
            else
            {
                to = new Item()
                {
                    CharacterId = characterId,
                    DetailId = detailId,
                    State = ItemState.Unequiped,
                    Quantity = qty
                };

                _db.Add(to);
            }

            await this.SaveChangesAsync(saveChanges);
            return to;
        }

        private void TransferItemInternalException(int qty, Item from)
        {
            if (qty <= 0)
            {
                throw new ArgumentException("Must be greater than zero.", nameof(qty));
            }

            if (qty > from.Quantity)
            {
                throw new NotEnoughException("Selected qty is to big.");
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="qty">Must be greater than zero, can throw NotEnoughException, ArgumentException</param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="deleteIfZeroQty"></param>
        /// <returns></returns>
        public async Task<Item> TransferItemAsync(int qty, int characterId, Item from, Item to = null, 
            bool deleteIfZero = false, ItemState state = ItemState.Unequiped, bool saveChanges = true)
        {
            TransferItemInternalException(qty, from);

            from.Quantity -= qty;
            if(to != null && from.DetailId == to.DetailId && to.State == state)
            {
                to.Quantity += qty;
            }
            else
            {
                to = new Item()
                {
                    CharacterId = characterId,
                    DetailId = from.DetailId,
                    State = state,
                    Quantity = qty
                };

                _db.Add(to);
            }

            if (deleteIfZero && from.Quantity == 0)
            {
                _db.Items.Remove(from);
            }

            await this.SaveChangesAsync(saveChanges);
            return to;
        }

        private async Task MergeEquippedItemAsync(Item equipedItem)
        {
            var inventoryItem = await _db.Items.FirstOrDefaultAsync(x => x.CharacterId == equipedItem.CharacterId
                && x.DetailId == equipedItem.DetailId && x.State != ItemState.Equiped);

            equipedItem.State = ItemState.Unequiped;
            if (inventoryItem != null)
            {
                inventoryItem.Quantity++;
                _db.Items.Remove(equipedItem);
            }
        }

        public async Task UnequipItemAsync(int characterId, long id, bool saveChanges = true)
        {
            var equipedItem = await _db.Items.FirstOrDefaultAsync(x =>
                x.CharacterId == characterId
                && x.Id == id
                && x.State == ItemState.Equiped);

            if(equipedItem == null)
            {
                throw new NotFoundException(nameof(equipedItem));
            }

            await MergeEquippedItemAsync(equipedItem);
            await SaveChangesAsync(saveChanges);
        }

        /// <summary>
        /// Unequip the equipped item.
        /// </summary>
        /// <param name="characterId"></param>
        /// <param name="itemType"></param>
        /// <param name="saveChanges"></param>
        /// <returns></returns>
        private async Task<bool> UnequipItemByTypeAsync(int characterId, int itemType, bool saveChanges = false)
        {
            var existingItem = await _db.Items.FirstOrDefaultAsync(x =>
                x.CharacterId == characterId
                && x.Detail.Type == itemType
                && x.State == ItemState.Equiped);

            if (existingItem != null)
            {
                await MergeEquippedItemAsync(existingItem);
                await SaveChangesAsync(saveChanges);
                return true;
            }

            return false;
        }

        public async Task<Item> AddEquippedItemAsync(Item item, ViewModels.Areas.Game.ItemDetails.GetViewModel detailVm, bool saveChanges = true)
        {
            if(item.Quantity <= 0)
            {
                return null;
            }

            //Check if a item already is equipped and unequip it
            await UnequipItemByTypeAsync(item.CharacterId, detailVm.Detail.Type); //Will be saved later.

            if (item.Quantity > 1)
            {
                //Remove 1 item from the stack and equip it as a new item.
                item.Quantity--;
                var equipedItem = new Item()
                {
                    CharacterId = item.CharacterId,
                    DetailId = item.DetailId,
                    Quantity = 1,
                    State = ItemState.Equiped
                };

                _db.Items.Add(equipedItem);
                await SaveChangesAsync(saveChanges);

                return equipedItem;
            }

            //Only 1 item
            item.State = ItemState.Equiped;
            await SaveChangesAsync(saveChanges);

            return item;
        }

        public async Task<Guid[]> GetAllLootsAsync(int[] enemyIds)
        {
            return await _db.Items.Where(x => enemyIds.Contains(x.CharacterId) 
                && x.State == ItemState.Loot).Select(x => x.DetailId).ToArrayAsync();
        }

        public async Task<Dictionary<Guid,string>> GetNamesOfDropsAsync(Guid[] detailIds)
        {
            return await _db.ItemDetails.Where(x => detailIds.Contains(x.Id))
                .ToDictionaryAsync(x => x.Id, x => x.Name);
        }
    }
}
