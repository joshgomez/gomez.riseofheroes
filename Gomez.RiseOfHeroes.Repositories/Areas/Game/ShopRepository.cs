﻿using Gomez.Core.ViewModels.Shared;
using Gomez.RiseOfHeroes.Models.Areas.Game;
using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.Repositories.Shared;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcShops;
using Microsoft.Extensions.Localization;
using System;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.Repositories.Areas.Game
{
    public class ShopRepository : DbRepositoryBase
    {
        private readonly ItemRepository _itemRepository;
        private readonly PlayerRepository _playerRepository;
        private readonly NpcShopRepository _npcRepository;

        public ShopRepository(ApplicationDbContext db, IStringLocalizerFactory localizerFactory,
            ItemRepository itemRepository, PlayerRepository playerRepository, NpcShopRepository npcRepository) : base(db, localizerFactory)
        {
            _itemRepository = itemRepository;
            _playerRepository = playerRepository;
            _npcRepository = npcRepository;
        }

        /// <summary>
        /// A player is buying something from a npc.
        /// </summary>
        /// <param name="vm"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<ValidationViewModel<SellInternalViewModel>> SellAsync(ValidationViewModel<SellInternalViewModel> vm, Item item)
        {
            if (vm.IsValid)
            {
                using var transaction = await _db.Database.BeginTransactionAsync();

                try
                {
                    bool delete = false;
                    if(item.State == Models.Areas.Game.Enums.ItemState.ShopSecondHand)
                    {
                        delete = true;
                    }

                    await _npcRepository.SellAsync(vm, false);
                    await _playerRepository.BuyAsync(vm, false);
                    var toItem = await _itemRepository.GetItemByDetailIdAsync(vm.Model.BuyersId, item.DetailId, 
                        Models.Areas.Game.Enums.ItemState.Unequiped);
                    await _itemRepository.TransferItemAsync(vm.Model.Quantity, vm.Model.BuyersId, item, toItem, delete, saveChanges: false);
                    await _db.SaveChangesAsync();
                    await transaction.CommitAsync();
                    vm.Model.BuyersMoney -= vm.Model.Cost;
                    vm.Model.SellersMoney += vm.Model.Cost;
                }
                catch(Exception ex)
                {
                    await transaction.RollbackAsync();
                    if(_db.IsConcurrencyException(ex))
                    {
                        vm.ConcurrencyConflict = true;
                        vm.AddError(nameof(SellAsync));
                        return vm;
                    }

                    throw;
                }
            }


            return vm;
        }

        /// <summary>
        /// A player is selling something to a npc.
        /// </summary>
        /// <param name="vm"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task<ValidationViewModel<SellInternalViewModel>> BuyAsync(ValidationViewModel<SellInternalViewModel> vm, Item item)
        {
            if (vm.IsValid)
            {
                using var transaction = await _db.Database.BeginTransactionAsync();

                try
                {
                    await _npcRepository.BuyAsync(vm, false);
                    await _playerRepository.SellAsync(vm, false);
                    var toItem = await _itemRepository.GetItemByDetailIdAsync(vm.Model.BuyersId, item.DetailId,
                        Models.Areas.Game.Enums.ItemState.ShopSecondHand);
                    await _itemRepository.TransferItemAsync(vm.Model.Quantity, vm.Model.BuyersId, item, toItem
                        ,true,Models.Areas.Game.Enums.ItemState.ShopSecondHand, saveChanges: false);
                    await _db.SaveChangesAsync();
                    await transaction.CommitAsync();
                    vm.Model.BuyersMoney -= vm.Model.Cost;
                    vm.Model.SellersMoney += vm.Model.Cost;
                }
                catch (Exception ex)
                {
                    await transaction.RollbackAsync();
                    if (_db.IsConcurrencyException(ex))
                    {
                        vm.ConcurrencyConflict = true;
                        vm.AddError(nameof(BuyAsync));
                        return vm;
                    }

                    throw;
                }
            }


            return vm;
        }
    }
}
