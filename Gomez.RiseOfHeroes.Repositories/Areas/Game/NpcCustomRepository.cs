﻿using Gomez.Core.Models.Exceptions;
using Gomez.RiseOfHeroes.Calculations;
using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.Repositories.Areas.Game.Shared;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcCustoms;
using Microsoft.Extensions.Localization;
using System;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;
using Gomez.RiseOfHeroes.Models.Areas.Game;
using Microsoft.EntityFrameworkCore;
using Gomez.Core.Utilities;

namespace Gomez.RiseOfHeroes.Repositories.Areas.Game
{
    public class NpcCustomRepository : CharacterRepository
    {
        public NpcCustomRepository(ApplicationDbContext db, IStringLocalizerFactory localizerFactory) : base(db, localizerFactory)
        {
            Query = Query.Where(x => x.Type == CharacterConstant.TYPE_NPC_CUSTOM);
        }

        public override async Task<GetViewModel> GetWithDetailsAsync(GetViewModel vm)
        {
            vm = await base.GetWithDetailsAsync(vm);
            if (vm.PropertyCollection != null)
            {
                var calculation = new NpcCustomCalculation();
                calculation.Init(vm.PropertyCollection);
                vm.DynamicProperties.Add(nameof(CharacterCalculation), calculation);
            }

            return vm;
        }

        public async Task<WorkDetailsViewModel> GetWorkDetailsAsync(int id, string type)
        {
            ValidateWorkType(type);

            var propKeys = new string[2];
            propKeys[0] = CharacterConstant.KEY_STATS_LEVEL;
            propKeys[1] = type;

            var properties = await _db.CharacterProperties
                    .Where(x => x.CharacterId == id
                        && x.Character.Type == CharacterConstant.TYPE_NPC_CUSTOM
                        && propKeys.Contains(x.Key))
                    .Select(x => new { Value = x.ValueInt32,x.Key })
                    .ToDictionaryAsync(x => x.Key,x => x.Value);
            if(!properties.ContainsKey(CharacterConstant.KEY_STATS_LEVEL))
            {
                throw new NotFoundException(nameof(id));
            }

            if (!properties.ContainsKey(type))
            {
                throw new NotFoundException(nameof(type));
            }

            return new WorkDetailsViewModel(DateTime.UtcNow,
                properties[CharacterConstant.KEY_STATS_LEVEL] ?? 1, properties[type] ?? 0)
            {
                Type = type
            };
        }

        private void ValidateWorkType(string type)
        {
            var validWork = ClassUtility.GetConstants<string>(typeof(NpcCustomConstant),
                x => ((string)x.GetRawConstantValue()).StartsWith("Work."));
            if (!validWork.ContainsValue(type))
            {
                throw new NotFoundException(nameof(type));
            }
        }

        private async Task ValidationLocationAsync(GetViewModel npc, GetViewModel player)
        {
            if (!await this.IsInSamePositionAsync(npc.Id, player.Character.Coordinate))
            {
                throw new BadRequestException(nameof(player.Character.Coordinate));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="energy">amount of energy to use when working.</param>
        /// <param name="playerVm"></param>
        /// <returns></returns>
        public async Task<WorkViewModel> WorkAsync(WorkViewModel vm, GetViewModel playerVm)
        {
            if(vm.Energy < 0.1)
            {
                throw new NotEnoughException(nameof(vm.Energy));
            }

            ValidateWorkType(vm.Type);

            var npcVm = new GetViewModel() { Id = vm.Id, Property = "*" };
            npcVm = await this.GetWithDetailsAsync(npcVm);
            await ValidationLocationAsync(npcVm, playerVm);

            if (!npcVm.PropertyCollection.PropertyExists(vm.Type))
            {
                throw new NotFoundException(nameof(vm.Type));
            }

            var workAttribute = npcVm.PropertyCollection.GetProperty<int>(vm.Type);
            var npcCalc = npcVm.GetPropertyByType<CharacterCalculation>() as NpcCustomCalculation;
            var playerCalc = playerVm.GetPropertyByType<CharacterCalculation>() as PlayerCalculation;
            var workDetail = new WorkDetailsViewModel(DateTime.UtcNow, npcCalc.Level, workAttribute) { 
                Type = vm.Type
            };
            vm.Details = workDetail;

            if (playerCalc.Energy < vm.Energy)
            {
                throw new NotEnoughException(nameof(vm.Energy));
            }

            if (
                playerCalc.Strength < workDetail.Strength ||
                playerCalc.Constitution < workDetail.Constitution ||
                playerCalc.Dexterity < workDetail.Dexterity ||
                playerCalc.Intelligence < workDetail.Intelligence
            )
            {
                //Some has taken the job.
                throw new UnavailableException(nameof(playerCalc.Attributes));
            }

            vm.Wage = playerCalc.WorkWage(npcCalc.Level, workDetail.Intelligence, vm.Energy);
            using var transaction = await BeginTransactionAsync();
            try
            {
                playerCalc.AddMoney(vm.Wage);
                playerCalc.AddEnergy(-vm.Energy);
                vm.EnergyLeft = playerCalc.Energy;
                vm.TotalMoney = playerCalc.Money;

                var rowUpdate = await _db.CharacterProperties.Where(x =>
                    x.CharacterId == playerVm.Id
                    && x.Key == CharacterConstant.KEY_MONEY
                    && x.RowVersion == playerCalc.RowVersions[CharacterConstant.KEY_MONEY])
                    .UpdateAsync(x => new CharacterProperty { ValueDecimal = playerCalc.Money });
                if(rowUpdate == 0)
                {
                    throw new ConcurrencyException(nameof(playerCalc.Money));
                }

                rowUpdate = await _db.CharacterProperties.Where(x =>
                    x.CharacterId == playerVm.Id
                    && x.Key == CharacterConstant.KEY_STATS_ENERGY
                    && x.RowVersion == playerCalc.RowVersions[CharacterConstant.KEY_STATS_ENERGY])
                    .UpdateAsync(x => new CharacterProperty { ValueDouble = playerCalc.Energy });
                if (rowUpdate == 0)
                {
                    throw new ConcurrencyException(nameof(playerCalc.Energy));
                }

                await transaction.CommitAsync();
            }
            catch
            {
                await transaction.RollbackAsync();
                throw;
            }

            return vm;
        }
    }
}
