﻿using Gomez.Core.Models.Exceptions;
using Gomez.RiseOfHeroes.Models.Areas.Game;
using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.Repositories.Shared;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Locations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Nts = NetTopologySuite.Geometries;
using System;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;
using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using Gomez.Core.Ef.Models.Entity;

namespace Gomez.RiseOfHeroes.Repositories.Areas.Game
{
    public class LocationRepository : DbRepositoryBase<Location>
    {
        public LocationRepository(ApplicationDbContext db, IStringLocalizerFactory localizerFactory) : base(db, localizerFactory)
        {
            Init();
        }

        public async Task<bool> AnyAsyc(Guid id)
        {
            return await Query.AnyAsync(x => x.Id == id);
        }

        public async Task<Guid> GetIdByCoordAsyc(Nts.Point point)
        {
            return await Query.Where(x => x.Coordinate == point).Select(x => x.Id).FirstOrDefaultAsync();
        }

        protected override IQueryable<Location> SearchQuery(IQueryable<Location> query, string term)
        {
            return query.Where(x => x.Name.Contains(term));
        }

        public virtual async Task<IndexViewModel> IndexAsync(IndexViewModel vm)
        {
            var query = MultiSearch(vm.Search, Query);
            if (vm.CharacterId != null)
            {
                query = query.Where(x => x.CharacterSubscriptions.Any(z => z.CharacterId == vm.CharacterId));
            }

            vm.Locations = await ToIndexViewModel(query)
                .OrderBy(x => x.Name)
                .GetPagedAsync(vm.Page, vm.Rows);

            return vm;
        }

        protected IQueryable<LocationIndexViewModel> ToIndexViewModel(IQueryable<Location> query = null)
        {
            query ??= Query;
            return query.Select(x => new LocationIndexViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                Coordinate = new LocationCoordinate() { X = x.Coordinate.X, Y = x.Coordinate.Y }
            });
        }

        protected IQueryable<LocationBaseViewModel> ToGetViewModel(IQueryable<Location> query = null)
        {
            query ??= Query;
            return query.Select(x => new LocationBaseViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                HasBackgrounds = x.HasBackgrounds,
                Coordinate = new LocationCoordinate() { X = x.Coordinate.X, Y = x.Coordinate.Y }
            });
        }

        protected virtual async Task<GetViewModel> GetAsync(GetViewModel vm)
        {
            var query = Query.Where(x => x.Id == vm.Id);
            vm.Location = await ToGetViewModel(query).FirstOrDefaultAsync();
            if (vm.Location == null)
            {
                throw new NotFoundException(nameof(vm.Location));
            }
            return vm;
        }

        public virtual async Task<GetViewModel> GetWithDetailsAsync(Guid id, QueryGetViewModel query = null)
        {
            var baseVm = new GetViewModel() { Id = id };
            if(query != null)
            {
                baseVm = new GetViewModel(query)
                {
                    Id = id
                };
            }

            return await GetWithDetailsAsync(baseVm);
        }
        public virtual async Task<GetViewModel> GetWithDetailsAsync(Nts.Point point, QueryGetViewModel query = null)
        {
            var id = await GetIdByCoordAsyc(point);
            return await GetWithDetailsAsync(id, query);
        }


        public async Task<GetViewModel> GetWithDetailsAsync(GetViewModel vm)
        {
            vm = await GetAsync(vm);
            return vm;
        }

        public async Task<string> GetDescriptionAsync(Guid id)
        {
            return await _db.Locations.Where(x => x.Id == id)
                .Select(x => x.Description)
                .DeferredFirstOrDefault().FromCacheAsync(string.Format(LocationConstant.CACHE_KEY_DESC, id));
        }
    }
}
