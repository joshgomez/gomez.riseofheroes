﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Gomez.Core.Ef.Models.Entity;
using Gomez.Core.LibraryExtensions.AutoMapper;
using Gomez.Core.Models.Exceptions;
using Gomez.Core.Models.Helpers;
using Gomez.Core.Models.Shared;
using Gomez.Core.ViewModels.Shared;
using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using Gomez.RiseOfHeroes.Models.Areas.Game;
using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.Repositories.Shared;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Gomez.RiseOfHeroes.Repositories.Areas.Game.Shared
{
    public abstract class CharacterRepository : DbRepositoryBase<Character>
    {
        protected CharacterRepository(ApplicationDbContext db, IStringLocalizerFactory localizerFactory) : base(db, localizerFactory)
        {
            SetMapper(nameof(PropertyDto<int>), new MapperConfiguration(
                cfg =>
                {
                    cfg.CreateMap<CharacterProperty, PropertyDto<int>>()
                    .ConstructUsing(x => new PropertyDto<int>())
                    .ForMember(x => x.RelationId, x => x.MapFrom(z => z.CharacterId))
                    .Ignore(x => x.Value);
                }));

            Init();
        }

        public async Task SetSinglePropertyAsync<T>(int id, string key, T value, bool saveChanges = true, byte[] rowVersion = null)
        {
            var property = await _db.CharacterProperties
                .Where(x => x.CharacterId == id).FirstOrDefaultAsync(x => x.Key == key);
            if (property == null)
            {
                property = new CharacterProperty(typeof(T), key, value)
                {
                    CharacterId = id
                };

                _db.CharacterProperties.Add(property);
            }

            await base.SetSinglePropertyAsync(property, value, saveChanges, rowVersion);
        }

        public virtual async Task<bool> AnyAsyc(int id)
        {
            return await Query.AnyAsync(x => x.Id == id);
        }

        protected override IQueryable<Character> SearchQuery(IQueryable<Character> query, string term)
        {
            return query.Where(x => x.Name.Contains(term));
        }

        public virtual async Task<IndexViewModel> IndexAsync(IndexViewModel vm)
        {
            var query = MultiSearch(vm.Search, Query);
            if(vm.LocationId != null)
            {
                query = query.Where(x => x.LocationSubscriptions.Any(z => z.LocationId == vm.LocationId));
            }

            vm.Characters = await ToIndexViewModel(query)
                .OrderBy(x => x.Name)
                .GetPagedAsync(vm.Page, vm.Rows);

            return vm;
        }

        protected IQueryable<CharacterIndexViewModel> ToIndexViewModel(IQueryable<Character> query = null)
        {
            query ??= Query;
            return query.Select(x => new CharacterIndexViewModel()
            {
                Id = x.Id,
                Name = x.Name
            });
        }

        protected IQueryable<CharacterBaseViewModel> ToGetViewModel(IQueryable<Character> query = null)
        {
            query ??= Query;
            return query.Select(x => new CharacterBaseViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                Type = x.Type,
                Coordinate = x.Location != null ? new ViewModels.Areas.Game.Locations.LocationCoordinate() { X = x.Location.X, Y = x.Location.Y } :
                new ViewModels.Areas.Game.Locations.LocationCoordinate()
            });
        }

        protected virtual async Task<GetViewModel> GetAsync(GetViewModel vm)
        {
            var query = Query.Where(x => x.Id == vm.Id);
            vm.Character = await ToGetViewModel(query).FirstOrDefaultAsync();
            if (vm.Character == null)
            {
                throw new NotFoundException(nameof(vm.Character));
            }
            return vm;
        }

        public virtual async Task<GetViewModel> GetWithDetailsAsync(GetViewModel vm)
        {
            vm = await GetAsync(vm);
            if (!String.IsNullOrEmpty(vm.Property))
            {
                vm.Property = vm.Property.StartsWith("*") ? "" : vm.Property;
                var collection = await GetDetailsAsync(vm.Character.Id, false, vm.Property.Split(","));
                var properties = collection.Properties.Select(x => new SimplePropertyDto() 
                { 
                    Key = x.Key, TypeNum = x.TypeNum, Value = x.Value,
                    RowVersion = x.RowVersion
                });
                vm.PropertyCollection = new SimplePropertyDtoCollection(properties);
            }



            return vm;
        }

        public virtual async Task<GetViewModel> GetWithDetailsAsync(int id, string property = "*")
        {
            return await GetWithDetailsAsync(new GetViewModel() { Id = id, Property = property });
        }

        protected IQueryable<CharacterProperty> GetProperties(int id, params string[] keys)
        {
            var query = _db.CharacterProperties.Where(x => x.CharacterId == id && !x.Key.StartsWith("_"));
            if (keys?.Length > 0 && !string.IsNullOrEmpty(keys[0]))
            {
                query = query.Where(x => keys.Contains(x.Key));
            }

            return query;
        }

        protected IQueryable<PropertyDto<int>> GetDtoProperties(int id, params string[] keys)
        {
            var cfg = GetMapperCfg(nameof(PropertyDto<int>));
            var query = GetProperties(id, keys)
                .ProjectTo<PropertyDto<int>>(cfg);
            return query;
        }

        protected async Task<PropertyDtoCollection<int>> GetDetailsAsync(int id, bool @private = false, params string[] keys)
        {
            await Task.Yield();
            var model = new PropertyDtoCollection<int>
            {
                Properties = await GetDtoProperties(id, keys).Where(x => @private || (!@private && !x.Key.StartsWith("_")))
                .ToArrayAsync()
            };

            return model;
        }

        public virtual async Task<bool> DeleteAsync(int id, Func<Task> onTransaction = null)
        {
            using var transaction = await _db.Database.BeginTransactionAsync();
            try
            {
                await _db.CharacterProperties.Where(x => x.CharacterId == id).DeleteAsync();
                await _db.Items.Where(x => x.CharacterId == id).DeleteAsync();
                int rows = await _db.Characters.Where(x => x.Id == id).DeleteAsync();
                if(onTransaction != null)
                {
                    await onTransaction();
                }

                await transaction.CommitAsync();
                return rows > 0;
            }
            catch
            {
                await transaction.RollbackAsync();
                throw;
            }
        }

        public async Task<Dictionary<long, Guid>> GetEquipmentDetailIdsAsync(int id)
        {
            var equipmentTypes = new int[] {
                ItemConstant.TYPE_HEAD_GEAR,
                ItemConstant.TYPE_BODY_GEAR,
                ItemConstant.TYPE_WEAPON
            };

            return await _db.Items.Where(x => x.CharacterId == id && equipmentTypes.Contains(x.Detail.Type) && x.State == Models.Areas.Game.Enums.ItemState.Equiped)
                .ToDictionaryAsync(x => x.Id, x => x.DetailId);
        }

        public async Task<EquipmentItemViewModel[]> GetEquipmentItemsAsync(int id)
        {
            var equipmentTypes = new int[] {
                ItemConstant.TYPE_HEAD_GEAR,
                ItemConstant.TYPE_BODY_GEAR,
                ItemConstant.TYPE_WEAPON
            };

            return await _db.Items.Where(x => x.CharacterId == id 
                && equipmentTypes.Contains(x.Detail.Type) 
                && x.State == Models.Areas.Game.Enums.ItemState.Equiped)
                .Select(x => new EquipmentItemViewModel() 
                { 
                    Id = x.Id, 
                    DetailId = x.DetailId, 
                    Name = x.Detail.Name, 
                    Type = x.Detail.Type
                })
                .ToArrayAsync();
        }

        public Task<Character> SetAsync(int id, bool saveChanges, params (string key, object value, byte[] rowVersion)[] pairs)
        {
            if (id == 0)
            {
                throw new ArgumentNullException(nameof(id));
            }

            if (pairs.Length == 0)
            {
                throw new ArgumentNullException(nameof(pairs));
            }

            return SetInternalAsync(id, saveChanges, pairs);
        }

        public Task<Character> SetAsync(int id, params (string key, object value, byte[] rowVersion)[] pairs)
        {
            return SetAsync(id, true, pairs);
        }

        private async Task<Character> SetInternalAsync(int id, bool saveChanges, params (string key, object value, byte[] rowVersion)[] pairs)
        {
            Character character = await FindAsync(id);
            if (character == null)
            {
                throw new NotFoundException(nameof(character));
            }

            var keys = pairs.Select(x => x.key).ToArray();
            character.Properties = await GetProperties(character.Id, keys).ToListAsync();

            await base.SetInternalAsync(character, saveChanges, pairs);

            return character;
        }

        public async Task<bool> IsInSamePositionAsync(int id, ICoordinate coord)
        {
           var point = NtsHelper.GetPoint(coord);
           return await _db.CharacterLocationSubscriptions.Where(x => x.CharacterId == id && x.Location.Coordinate == point).AnyAsync();
        }

        public async Task<string> GetDescriptionAsync(int id)
        {
            return await _db.Characters.Where(x => x.Id == id)
                .Select(x => x.Description)
                .DeferredFirstOrDefault().FromCacheAsync(string.Format(CharacterConstant.CACHE_KEY_DESC,id));
        }

        public async Task TransferMoneyAsync(int sourceId, int destinationId, decimal value, bool saveChanges = true)
        {
            int[] charIds = new int[2] { sourceId, destinationId };
            var properties = await _db.CharacterProperties
                .Where(x => charIds.Contains(x.CharacterId) && x.Key == CharacterConstant.KEY_MONEY)
                .Select(x => new { x.RowVersion, x.CharacterId, Value = x.ValueDecimal })
                .ToDictionaryAsync(x => x.CharacterId, x => x);

            if (properties[sourceId].Value < value)
            {
                throw new NotEnoughException(nameof(TransferMoneyAsync));
            }

            await this.SetSinglePropertyAsync(sourceId, CharacterConstant.KEY_MONEY,
                 properties[sourceId].Value - value, 
                false, properties[sourceId].RowVersion);
            await this.SetSinglePropertyAsync(destinationId, CharacterConstant.KEY_MONEY,
                properties[destinationId].Value + value,
                saveChanges, properties[destinationId].RowVersion);
        }

        public async Task<MinimumInformationViewModel> GetMinimumInfoAsync(int id)
        {
            return await _db.Characters
                .Where(x => x.Id == id).Select(x => new MinimumInformationViewModel()
                {
                     CharacterId = x.Id, 
                     Name = x.Name,
                     Location = x.Location
                }).FirstOrDefaultAsync();
        }
    }
}
