﻿using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.Repositories.Areas.Game.Shared;
using Microsoft.Extensions.Localization;

namespace Gomez.RiseOfHeroes.Repositories.Areas.Game
{
    public class CharacterSharedRepository : CharacterRepository
    {
        public CharacterSharedRepository(ApplicationDbContext db, IStringLocalizerFactory localizerFactory) : base(db, localizerFactory)
        {

        }
    }
}
