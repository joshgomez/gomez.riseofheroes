﻿using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.Repositories.Areas.Game.MiniGames;
using Gomez.RiseOfHeroes.Repositories.Shared;
using Microsoft.Extensions.Localization;

namespace Gomez.RiseOfHeroes.Repositories.Areas.Game
{
    public class MiniGameRepository : DbRepositoryBase
    {
        protected readonly PlayerRepository _playerRepository;
        protected readonly NpcCustomRepository _npcRepository;

        public MiniGameRepository(ApplicationDbContext db, IStringLocalizerFactory localizerFactory,
            PlayerRepository playerRepository, NpcCustomRepository npcRepository) : base(db, localizerFactory)
        {
            _playerRepository = playerRepository;
            _npcRepository = npcRepository;
        }

        public BlackJackRepository CreateBlackJackRepo()
        {
            return new BlackJackRepository(_db, _localizerFactory, _playerRepository, _npcRepository);
        }

        public SlotsRepository CreateSlotsRepo()
        {
            return new SlotsRepository(_db, _localizerFactory, _playerRepository, _npcRepository);
        }

        public RouletteRepository CreateRouletteRepo()
        {
            return new RouletteRepository(_db, _localizerFactory, _playerRepository, _npcRepository);
        }
    }
}
