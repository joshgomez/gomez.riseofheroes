﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Gomez.Core.LibraryExtensions.AutoMapper;
using Gomez.Core.Models.Exceptions;
using Gomez.Core.ViewModels.Shared;
using Gomez.RiseOfHeroes.Calculations;
using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using Gomez.RiseOfHeroes.Models.Areas.Game;
using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.Repositories.Shared;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.ItemDetails;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Gomez.RiseOfHeroes.Repositories.Areas.Game
{
    public class ItemDetailRepository : DbRepositoryBase<ItemDetail>
    {
        public ItemDetailRepository(ApplicationDbContext db, IStringLocalizerFactory localizerFactory) : base(db, localizerFactory)
        {
            SetMapper(nameof(PropertyDto<Guid>), new MapperConfiguration(
                cfg =>
                {
                    cfg.CreateMap<ItemDetailProperty, PropertyDto<Guid>>()
                    .ConstructUsing(x => new PropertyDto<Guid>())
                    .ForMember(x => x.RelationId, x => x.MapFrom(z => z.DetailId))
                    .Ignore(x => x.Value);
                }));

            Init();
        }

        public async Task<bool> AnyAsyc(Guid id)
        {
            return await Query.AnyAsync(x => x.Id == id);
        }

        protected override IQueryable<ItemDetail> SearchQuery(IQueryable<ItemDetail> query, string term)
        {
            return query.Where(x => x.Name.Contains(term));
        }

        protected IQueryable<DetailBaseViewModel> ToGetViewModel(IQueryable<ItemDetail> query = null)
        {
            query ??= Query;
            return query.Select(x => new DetailBaseViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                Type = x.Type
            });
        }

        protected virtual async Task<GetViewModel> GetAsync(GetViewModel vm)
        {
            var query = Query.Where(x => x.Id == vm.Id);
            vm.Detail = await ToGetViewModel(query).FirstOrDefaultAsync();
            if (vm.Detail == null)
            {
                throw new NotFoundException(nameof(vm.Detail));
            }
            return vm;
        }

        public virtual async Task<GetViewModel> GetWithDetailsAsync(Guid id, string property = "*")
        {
            var baseVm = new GetViewModel() { Id = id, Property = property };
            return await GetWithDetailsAsync(baseVm);
        }

        public async IAsyncEnumerable<(Guid itemId, double score, int level)> GetAllRarenessAsync(Guid[] itemIds)
        {
            var propKeys = new string[2] { ItemConstant.KEY_STATS_COST, ItemConstant.KEY_STATS_LEVEL };
            var allProperties = await _db.ItemDetailProperties
                .Where(x => itemIds.Contains(x.DetailId)
                    && propKeys.Contains(x.Key)).ToArrayAsync();

            var properties = allProperties
                .OrderBy(x => x.Key == ItemConstant.KEY_STATS_LEVEL ? x.ValueInt32 : x.ValueDecimal)
                .GroupBy(x => x.DetailId)
                .ToDictionary(x => x.Key, x => x.ToDictionary(x => x.Key, x => x.Value));

            foreach (var itemId in itemIds)
            {
                decimal cost = (decimal?)properties[itemId][ItemConstant.KEY_STATS_COST] ?? 1m;
                int lvl = (int?)properties[itemId][ItemConstant.KEY_STATS_LEVEL] ?? 1;
                yield return (itemId, lvl + Math.Log10(Convert.ToDouble(cost)), lvl);
            }
        }

        public async Task<GetViewModel> GetWithDetailsAsync(GetViewModel vm)
        {
            vm = await GetAsync(vm);
            if (!String.IsNullOrEmpty(vm.Property))
            {
                vm.Property = vm.Property.StartsWith("*") ? "" : vm.Property;
                var collection = await GetDetailsAsync(vm.Id, false, vm.Property.Split(","));
                var properties = collection.Properties.Select(x => new SimplePropertyDto() { Key = x.Key, TypeNum = x.TypeNum, Value = x.Value });

                if(vm.Item != null)
                {
                    var itemProperties = new SimplePropertyDto[] {
                    new SimplePropertyDto() { Key = ItemConstant.KEY_STATS_QUANTITY, TypeNum = Core.Models.Entity.PropertyBaseModel.GetPropertyTypeNum(typeof(int)), Value = vm.Item.Quantity } };
                    properties = properties.Union(itemProperties);
                }

                vm.PropertyCollection = new SimplePropertyDtoCollection(properties);
                var calculation = new ItemCalculation();
                calculation.Init(vm.PropertyCollection);
                vm.DynamicProperties.Add(nameof(ItemCalculation), calculation);
            }

            return vm;
        }

        protected IQueryable<ItemDetailProperty> GetProperties(Guid id, params string[] keys)
        {
            var query = _db.ItemDetailProperties.Where(x => x.DetailId == id);
            if (keys?.Length > 0 && !string.IsNullOrEmpty(keys[0]))
            {
                query = query.Where(x => keys.Contains(x.Key));
            }

            return query;
        }

        protected IQueryable<PropertyDto<Guid>> GetDtoProperties(Guid id, params string[] keys)
        {
            var cfg = GetMapperCfg(nameof(PropertyDto<Guid>));
            var query = GetProperties(id, keys)
                .ProjectTo<PropertyDto<Guid>>(cfg);
            return query;
        }

        protected async Task<PropertyDtoCollection<Guid>> GetDetailsAsync(Guid id, bool @private = false, params string[] keys)
        {
            await Task.Yield();
            var model = new PropertyDtoCollection<Guid>
            {
                Properties = await GetDtoProperties(id, keys).Where(x => @private || (!@private && !x.Key.StartsWith("_")))
                .ToArrayAsync()
            };

            return model;
        }

        public async Task<Dictionary<Guid, SimplePropertyDto[]>> GetPropertiesAsync(Guid[] ids, params string[] keys)
        {
            var query = _db.ItemDetailProperties.Where(x => ids.Contains(x.DetailId));
            if (keys?.Length > 0 && !string.IsNullOrEmpty(keys[0]))
            {
                query = query.Where(x => keys.Any(z => x.Key.StartsWith(z)));
            }

            var cfg = GetMapperCfg(nameof(PropertyDto<Guid>));
            var query2 = query.ProjectTo<PropertyDto<Guid>>(cfg);

            var result = (await query2.ToArrayAsync())
                .GroupBy(x => x.RelationId)
                .ToDictionary(x => x.Key, 
                    x => x.Select(z => new SimplePropertyDto() { Key = z.Key, TypeNum = z.TypeNum, Value = z.Value }).ToArray());
            
            return result;
        }

        public async Task<string> GetDescriptionAsync(Guid id)
        {
            return await _db.ItemDetails.Where(x => x.Id == id)
                .Select(x => x.Description)
                .DeferredFirstOrDefault().FromCacheAsync(string.Format(ItemConstant.CACHE_KEY_DESC, id));
        }
    }
}
