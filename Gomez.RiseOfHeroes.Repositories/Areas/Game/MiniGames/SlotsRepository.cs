﻿using Gomez.Core.Utilities.Game.Models.Slots;
using Gomez.RiseOfHeroes.Models.Contexts;
using Microsoft.Extensions.Localization;
using System.Threading.Tasks;
using NpcVm = Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcCustoms;
using playerVm = Gomez.RiseOfHeroes.ViewModels.Areas.Game.Players;

namespace Gomez.RiseOfHeroes.Repositories.Areas.Game.MiniGames
{
    public class SlotsRepository : MiniGameRepository
    {
        public SlotsRepository(ApplicationDbContext db, IStringLocalizerFactory localizerFactory,
            PlayerRepository playerRepository, NpcCustomRepository npcRepository) : base(db, localizerFactory,playerRepository,npcRepository)
        {

        }

        public async Task NewGameAsync(NpcVm.IBasicInformationViewModel npc, playerVm.BasicInformationViewModel player, ISlotMachine game)
        {
            decimal profit = game.Profit - game.Credits;
            if (profit > 0)
            {
                await _npcRepository.TransferMoneyAsync(npc.Id, player.Id, profit);
                return;
            }

            await _npcRepository.TransferMoneyAsync(player.Id, npc.Id, game.Credits);
        }
    }
}
