﻿using Gomez.Core.Utilities.Game.Models.Cards;
using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.MiniGames;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;
using NpcVm = Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcCustoms;
using playerVm = Gomez.RiseOfHeroes.ViewModels.Areas.Game.Players;

namespace Gomez.RiseOfHeroes.Repositories.Areas.Game.MiniGames
{
    public class BlackJackRepository : MiniGameRepository
    {
        public BlackJackRepository(ApplicationDbContext db, IStringLocalizerFactory localizerFactory,
            PlayerRepository playerRepository, NpcCustomRepository npcRepository) : base(db, localizerFactory,playerRepository,npcRepository)
        {

        }

        public async Task SetBlackJackStateAsync(int characterId, string base64Data)
        {
            await _playerRepository.SetSinglePropertyAsync(characterId, PlayerConstant.KEY_P_GAME_BLACKJACK_STATE, base64Data);
        }

        public async Task<CardSaveData> InitBlackJackStateAsync(NpcVm.IBasicInformationViewModel npc, playerVm.BasicInformationViewModel player, IBlackJack game)
        {
            //Before starting the game the player will transfer the stake to the dealer.
            await _npcRepository.TransferMoneyAsync(player.Id, npc.Id, game.Stake, false);
            var data = await SaveAsync(player.Id, game);
            return new CardSaveData(data)
            {
                PlayerId = player.Id,
                NpcId = npc.Id
            };
        }

        public async Task<SaveData> SaveAsync(int playerId, IBlackJack game)
        {
            var saveData = game.Save();
            var ms = new MemoryStream();
            using (var writer = new BsonDataWriter(ms))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(writer, saveData);
            }

            string base64Data = Convert.ToBase64String(ms.ToArray());
            await _playerRepository.SetSinglePropertyAsync(playerId, PlayerConstant.KEY_P_GAME_BLACKJACK_STATE, base64Data);
            return saveData;
        }

        public async Task<string> GetBlackJackStateAsync(int characterId)
        {
            return await _db.CharacterProperties.Where(x =>
            x.CharacterId == characterId
            && x.Key == PlayerConstant.KEY_P_GAME_BLACKJACK_STATE)
                .Select(x => x.ValueString).FirstOrDefaultAsync();
        }

        public async Task<bool> AnyBlackJackStateAsync(int characterId)
        {
            return await _db.CharacterProperties.Where(x =>
            x.CharacterId == characterId
            && x.Key == PlayerConstant.KEY_P_GAME_BLACKJACK_STATE).AnyAsync();
        }

        public async Task EndBlackJackGameAsync(int npcId, int playerId, decimal profit)
        {
            using var transaction = await _db.Database.BeginTransactionAsync();
            try
            {
                await _db.CharacterProperties.Where(x =>
                            x.CharacterId == playerId
                            && x.Key == PlayerConstant.KEY_P_GAME_BLACKJACK_STATE)
                                .DeleteAsync();
                await _playerRepository.TransferMoneyAsync(npcId, playerId, profit);
                await transaction.CommitAsync();
            }
            catch
            {
                await transaction.RollbackAsync();
                throw;
            }

        }
    }
}
