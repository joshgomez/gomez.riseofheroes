﻿using Gomez.Core.Utilities.Game.Models.Roulette;
using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.MiniGames;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;
using NpcVm = Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcCustoms;
using playerVm = Gomez.RiseOfHeroes.ViewModels.Areas.Game.Players;

namespace Gomez.RiseOfHeroes.Repositories.Areas.Game.MiniGames
{
    public class RouletteRepository : MiniGameRepository
    {
        public RouletteRepository(ApplicationDbContext db, IStringLocalizerFactory localizerFactory,
            PlayerRepository playerRepository, NpcCustomRepository npcRepository) : base(db, localizerFactory,playerRepository,npcRepository)
        {

        }

        public async Task NewGameAsync(NpcVm.IBasicInformationViewModel npc, playerVm.BasicInformationViewModel player, IRouletteTable game)
        {
            decimal profit = game.Profit - game.Stake;
            if (profit > 0)
            {
                await _npcRepository.TransferMoneyAsync(npc.Id, player.Id, profit);
                return;
            }

            await _npcRepository.TransferMoneyAsync(player.Id, npc.Id, game.Stake);
        }
    }
}
