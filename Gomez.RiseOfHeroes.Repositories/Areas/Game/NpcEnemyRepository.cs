﻿using Gomez.RiseOfHeroes.Calculations;
using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.Repositories.Areas.Game.Shared;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Gomez.RiseOfHeroes.Repositories.Areas.Game
{
    public class NpcEnemyRepository : CharacterRepository
    {
        public NpcEnemyRepository(ApplicationDbContext db, IStringLocalizerFactory localizerFactory) : base(db, localizerFactory)
        {
            Query = Query.Where(x => x.Type == CharacterConstant.TYPE_NPC_ENEMY);
        }

        public override async Task<GetViewModel> GetWithDetailsAsync(GetViewModel vm)
        {
            vm = await base.GetWithDetailsAsync(vm);
            if (vm.PropertyCollection != null)
            {
                var calculation = new NpcEnemyCalculation();
                calculation.Init(vm.PropertyCollection);
                vm.DynamicProperties.Add(nameof(CharacterCalculation), calculation);
            }

            return vm;
        }
    }
}
