using Gomez.Core.Ef.Localization.SqlLocalizer;
using Gomez.Core.Ef.Localization.SqlLocalizer.DbStringLocalizer;
using Gomez.Core.Ef.Web.Services;
using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Gomez.RiseOfHeroes.Models.Contexts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using Pomelo.EntityFrameworkCore.MySql.Storage;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes
{
    public class Startup
    {
        private const string DefaultConnection = "DefaultConnection";
        private static string MigrationsAssembly => $"{nameof(Gomez)}.{nameof(Gomez.RiseOfHeroes)}.{nameof(Gomez.RiseOfHeroes.Models)}";
       
        private readonly string _allowSpecificOrigins = "AllowSpecificOrigins";
        private readonly string _allowAllOrigins = "AllowAllOrigins";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                var origins = Configuration.GetValue<string>("AllowedOrigins");
                options.AddPolicy(_allowSpecificOrigins,
                builder => builder.WithOrigins(origins)
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowCredentials());

                options.AddPolicy(_allowAllOrigins,
                    builder => builder.AllowAnyOrigin()
                                .AllowAnyHeader()
                                .AllowAnyMethod());
            });

            Socket.Startup.RegisterService(services);

            services.AddTransient<IJwtTokenService<ApplicationUser>, JwtTokenService<ApplicationUser>>();

            // Add in the DbContext which points to the appsettings connection string
            services.AddDbContextPool<ApplicationDbContext>( // replace "YourDbContext" with the class name of your DbContext
                options => options.UseMySql(Configuration.GetConnectionString(DefaultConnection), // replace with your Connection String
                    mySqlOptions =>
                    {
                        mySqlOptions.ServerVersion(new Version(10, 1, 31), ServerType.MariaDb); // replace with your Server Version and Type
                        mySqlOptions.MigrationsAssembly(MigrationsAssembly);
                        mySqlOptions.CharSet(CharSet.Latin1);
                    }));

            services.AddDbContext<LocalizationModelContext>( // replace "YourDbContext" with the class name of your DbContext
                options => options.UseMySql(Configuration.GetConnectionString(DefaultConnection), // replace with your Connection String
                    mySqlOptions =>
                    {
                        mySqlOptions.ServerVersion(new Version(10, 1, 31), ServerType.MariaDb); // replace with your Server Version and Type
                        mySqlOptions.MigrationsAssembly(MigrationsAssembly);
                        mySqlOptions.CharSet(CharSet.Latin1);
                    }),
                    ServiceLifetime.Singleton,
                    ServiceLifetime.Singleton);

            services.AddSqlLocalization(options => options.UseSettings
            (
              createNewRecordOnDefault: true
            ));


            services.Configure<RequestLocalizationOptions>(
                options =>
                {
                    var supportedCultures = new List<CultureInfo>
                    {
                        new CultureInfo("en-US"),
                        new CultureInfo("sv-SE"),
                    };

                    options.DefaultRequestCulture = new RequestCulture(culture: "en-US", uiCulture: "en-US");
                    options.SupportedCultures = supportedCultures;
                    options.SupportedUICultures = supportedCultures;
                });

            services.AddDefaultIdentity<ApplicationUser>()
                .AddRoles<ApplicationRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>();

            //Setting up Jwt Authentication
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration[JwtTokenService.JWT_ISSUER],
                        ValidAudience = Configuration[JwtTokenService.JWT_AUDIENCE],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration[JwtTokenService.JWT_KEY]))
                    };

#if DEBUG
                    options.TokenValidationParameters.ValidateAudience = false;
#endif

                    options.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            Socket.Startup.RegisterJwtOnMessageReceived(context);
                            return Task.CompletedTask;
                        }
                    };

                }).AddMicrosoftAccount(option =>
                {
                    option.ClientId = Configuration["Authentication:Microsoft:ClientId"];
                    option.ClientSecret = Configuration["Authentication:Microsoft:ClientSecret"];
                });

            services.AddResponseCaching();
            services.AddResponseCompression();

            services.AddControllers()
                .AddHybridModelBinder()
                .AddDataAnnotationsLocalization()
                .AddNewtonsoftJson();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, 
            UserManager<ApplicationUser> userManager, ApplicationDbContext db)
        {
            var dbInitializer = new ApplicationDbInitializer(db, userManager);
            dbInitializer.SeedUsers();
            dbInitializer.AddMissingCharacterProperties();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var locOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(locOptions.Value);

            app.UseCors(_allowSpecificOrigins);

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRequestLocalization();
            app.UseRouting();
   
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseResponseCaching();
            app.UseResponseCompression();
            app.UseEndpoints(endpoints =>
            {
                Socket.Startup.RegisterEndpoints(endpoints);
                endpoints.MapControllers();
            });
        }
    }
}
