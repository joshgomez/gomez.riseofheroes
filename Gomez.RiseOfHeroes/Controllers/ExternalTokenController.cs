﻿using Gomez.Core.Ef.Web.Services;
using Gomez.RiseOfHeroes.Configurations;
using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Gomez.RiseOfHeroes.Models.Contexts;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.Controllers
{
    [Route(RouteContant.API + "[controller]")]
    public class ExternalTokenController : Gomez.Core.Ef.Web.Controllers.ExternalTokenController<ApplicationDbContext,ApplicationUser,ApplicationRole, ExternalTokenController>
    {
        public static ApplicationUser OnCreateUser(Core.Web.Models.ExternalLogin vm)
        {
            string userName = vm.Info.Principal.Identity.Name;
            string email = vm.Info.Principal.FindFirst(ClaimTypes.Email)?.Value;
            var user = new ApplicationUser()
            {
                Id = Guid.NewGuid(),
                UserName = userName,
                NormalizedUserName = userName.ToUpper(),
                Email = email,
                NormalizedEmail = email?.ToUpper(),
                LastLoginAt = DateTime.UtcNow
            };

            return user;
        }

        public ExternalTokenController(ApplicationDbContext db, 
            UserManager<ApplicationUser> um, 
            IStringLocalizerFactory localizerFactory, 
            IJwtTokenService<ApplicationUser> tokenService,
            RoleManager<ApplicationRole> rm,
            SignInManager<ApplicationUser> sim,
            ILogger<ExternalTokenController> logger) : base(db, um, localizerFactory, tokenService, rm, sim, logger)
        {
            this.SetRedirectionWhiteList("http://localhost:8081");

            if(CreateUser == null)
            {
                CreateUser = OnCreateUser;
            }
        }



        [HttpPost]
        public IActionResult OnPost(string provider, string returnUrl = null)
        {
            // Request a redirect to the external login provider.
            string url = Url.Action("", "ExternalToken");
            return  base.OnPost(provider, url, returnUrl);
        }

        [HttpGet]
        public async Task<IActionResult> OnGetCallback(string returnUrl = null, string remoteError = null)
        {
            return await base.OnGetCallbackAsync(true, returnUrl, remoteError);
        }
    }
}
