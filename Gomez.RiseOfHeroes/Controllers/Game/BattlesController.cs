﻿using Gomez.RiseOfHeroes.Configurations;
using Gomez.RiseOfHeroes.Controllers.Game.Shared;
using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Gomez.RiseOfHeroes.Models.Contexts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.Controllers.Game
{
    [Authorize]
    [Route(RouteContant.API_AREA_GAME + "[controller]")]
    public class BattlesController : GameBaseController<BattlesController>
    {
        public BattlesController(ApplicationDbContext db, 
            UserManager<ApplicationUser> um, 
            IStringLocalizerFactory localizerFactory, 
            ILogger<BattlesController> logger) : base(db, um, localizerFactory, logger)
        {
        }


        [HttpGet("TargetTypes")]
        public async Task<IActionResult> GetTargetTypes()
        {
            await Task.Yield();
            return Ok(_gameHandler.BattleHelper.TargetTypes().Select(x => new { Value = x.Key ,Text = x.Value }));
        }

        [HttpGet("TacticTypes")]
        public async Task<IActionResult> GetTacticTypes()
        {
            await Task.Yield();
            return Ok(_gameHandler.BattleHelper.TacticTypes().Select(x => new { Value = x.Key, Text = x.Value }));
        }
    }
}
