﻿using Gomez.RiseOfHeroes.Configurations;
using Gomez.RiseOfHeroes.Controllers.Game.Shared;
using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Locations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using NetTopologySuite.Geometries;
using System;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.Controllers.Game
{
    [Authorize]
    [Route(RouteContant.API_AREA_GAME + "[controller]")]
    public class LocationsController : GameBaseController<LocationsController>
    {
        public LocationsController(ApplicationDbContext db,
            UserManager<ApplicationUser> um,
            IStringLocalizerFactory localizerFactory,
            ILogger<LocationsController> logger) : base(db, um, localizerFactory, logger)
        {
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]QueryIndexViewModel query)
        {
            var vm = await _gameHandler.LocationHandler.GetIndexAsync(query);
            return Ok(vm);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(QueryGetViewModel query)
        {
            var vm = await _gameHandler.LocationHandler.GetAsync(query);
            return Ok(vm);
        }

        [HttpGet("ByCoord")]
        public async Task<IActionResult> GetByCoord([FromQuery]double x, [FromQuery]double y)
        {
            var query = new QueryGetViewModel();
            var vm = await _gameHandler.LocationHandler.GetAsync(new Point(x, y), query);
            return Ok(vm);
        }

        [ResponseCache(Duration = 60*60*24, Location = ResponseCacheLocation.Client, VaryByQueryKeys = new string[] { "id" })]
        [HttpGet("{id}/Description")]
        public async Task<IActionResult> GetDescription([FromRoute]Guid id)
        {
            var vm = await _gameHandler.LocationHandler.GetGescriptionAsync(id);
            return Ok(vm);
        }
    }
}
