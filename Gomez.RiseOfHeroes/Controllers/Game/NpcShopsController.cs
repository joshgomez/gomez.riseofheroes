﻿using Gomez.Core.Web.Extensions;
using Gomez.RiseOfHeroes.Configurations;
using Gomez.RiseOfHeroes.Controllers.Game.Shared;
using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using VmNpcShop = Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcShops;

namespace Gomez.RiseOfHeroes.Controllers.Game
{
    [Authorize]
    [Route(RouteContant.API_AREA_GAME + "[controller]")]
    public class NpcShopsController : GameBaseController<NpcShopsController>
    {
        public NpcShopsController(ApplicationDbContext db, 
            UserManager<ApplicationUser> um, 
            IStringLocalizerFactory localizerFactory, 
            ILogger<NpcShopsController> logger) : base(db, um, localizerFactory, logger)
        {
        }

        [HttpGet]
        public async Task<IActionResult> Get(QueryIndexViewModel query)
        {
            var vm = await _gameHandler.NpcShopHandler.GetIndexAsync(query);
            return Ok(vm);
        }


        [HttpGet("{id}/Basic")]
        public async Task<IActionResult> GetBasicInformation(int id)
        {
            var vm = await _gameHandler.NpcShopHandler.GetBasicInformationAsync(id);
            return Ok(vm);
        }

        [HttpGet("{id}/Inventory")]
        public async Task<IActionResult> Inventory(VmNpcShop.QueryInventoryViewModel query)
        {
            var vm = await _gameHandler.NpcShopHandler.GetInventoryAsync(query);
            return Ok(vm);
        }

        /// <summary>
        /// The shop is found by item id.
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost("Inventory/Buy/{id}")]
        public async Task<IActionResult> BuyItem(VmNpcShop.QueryBuyViewModel query)
        {
            var userId = this.User.GetUserId<Guid>();
            var vm = await _gameHandler.ShopHandler.SellAsync(userId, query.Id, query.Qty);
            return Ok(vm);
        }

        [HttpGet("Cron/Inventory/RefillForAll")]
        public async Task<IActionResult> CronRefillForAll()
        {
            var result = await _gameHandler.NpcShopHandler.RefillStockForAllAsync();
            return Ok(result);
        }

        [HttpPost("{id}/InLocation")]
        public async Task<IActionResult> InLocation(QueryInLocationViewModel query)
        {
            var vm = await _gameHandler.NpcShopHandler.InInSamePositionAsync(query.Id,query.Coordinate);
            return Ok(vm);
        }
    }
}
