﻿using Gomez.RiseOfHeroes.Configurations;
using Gomez.RiseOfHeroes.Controllers.Game.Shared;
using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Gomez.RiseOfHeroes.Models.Contexts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.Controllers.Game
{
    [Authorize]
    [Route(RouteContant.API_AREA_GAME + "[controller]")]
    public class CharactersController : GameBaseController<NpcShopsController>
    {
        public CharactersController(ApplicationDbContext db, 
            UserManager<ApplicationUser> um, 
            IStringLocalizerFactory localizerFactory, 
            ILogger<NpcShopsController> logger) : base(db, um, localizerFactory, logger)
        {
        }

        [ResponseCache(Duration = 60 * 60 * 24, Location = ResponseCacheLocation.Client, VaryByQueryKeys = new string[] { "id" })]
        [HttpGet("{id}/Description")]
        public async Task<IActionResult> GetDescription([FromRoute]int id)
        {
            var vm = await _gameHandler.CharacterHandler.GetGescriptionAsync(id);
            return Ok(vm);
        }
    }
}
