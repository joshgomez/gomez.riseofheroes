﻿using Gomez.Core.Web.Extensions;
using Gomez.RiseOfHeroes.BusinessLogic.Areas.Game.MiniGames;
using Gomez.RiseOfHeroes.Configurations;
using Gomez.RiseOfHeroes.Controllers.Game.Shared;
using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.MiniGames.Slots;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.Controllers.Game
{
    [Authorize]
    [Route(RouteContant.API_AREA_GAME + "[controller]")]
    public class SlotMachineController : GameBaseController<SlotMachineController>
    {
        private readonly SlotsHandler _slots;

        public SlotMachineController(ApplicationDbContext db, 
            UserManager<ApplicationUser> um, 
            IStringLocalizerFactory localizerFactory, 
            ILogger<SlotMachineController> logger) : base(db, um, localizerFactory, logger)
        {
            //loaded from GameBase Controller.
            _slots = _gameHandler.MiniGameHandler.Slots;
        }



  
        [HttpPost("{id}")]
        public async Task<IActionResult> Post(NewQueryViewModel query)
        {
            var userId = this.User.GetUserId<Guid>();

            var playerVm = await _gameHandler.PlayerHandler.GetBasicInformationAsync(userId);
            var npcVm = await _gameHandler.NpcCustomHandler.GetBasicInformationAsync(query.Id);

            await _slots.NewAsync(npcVm,playerVm,query.Credits);
            return Ok(_slots.GetResults());
        }
    }
}
