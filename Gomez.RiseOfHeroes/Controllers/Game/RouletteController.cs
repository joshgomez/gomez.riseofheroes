﻿using Gomez.Core.Utilities.Game.Models.Roulette;
using Gomez.Core.Web.Extensions;
using Gomez.RiseOfHeroes.BusinessLogic.Areas.Game.MiniGames;
using Gomez.RiseOfHeroes.Configurations;
using Gomez.RiseOfHeroes.Controllers.Game.Shared;
using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.MiniGames.Roulette;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.Controllers.Game
{
    [Authorize]
    [Route(RouteContant.API_AREA_GAME + "[controller]")]
    public class RouletteController : GameBaseController<RouletteController>
    {
        private readonly RouletteHandler _roulette;

        public RouletteController(ApplicationDbContext db, 
            UserManager<ApplicationUser> um, 
            IStringLocalizerFactory localizerFactory, 
            ILogger<RouletteController> logger) : base(db, um, localizerFactory, logger)
        {
            //loaded from GameBase Controller.
            _roulette = _gameHandler.MiniGameHandler.Roulette;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            await Task.Yield();
            return Ok(RouletteHandler.GetSlots());
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> Post(NewQueryViewModel query)
        {
            var userId = this.User.GetUserId<Guid>();

            var playerVm = await _gameHandler.PlayerHandler.GetBasicInformationAsync(userId);
            var npcVm = await _gameHandler.NpcCustomHandler.GetBasicInformationAsync(query.Id);

            Bet bet = null;
            if(query.Number > 0)
            {
                bet = Bet.SetNumber(query.Number);
            }
            else
            {
                if (query.OneToEighteen)
                {
                    bet = Bet.SetOneToEighteen();
                }
                else if (query.NineteenToThirtySix)
                {
                    bet = Bet.SetNineteenToThirtySix();
                }

                if(bet != null)
                {
                    bet.Color = query.Color;
                    bet.EvenOrOdd = query.EvenOrOdd;
                }
            }
 

            if(bet == null)
            {
                return NotFound(nameof(bet));
            }

            await  _roulette.NewAsync(npcVm,playerVm,bet,query.Stake);
            return Ok(_roulette.GetResults());
        }
    }
}
