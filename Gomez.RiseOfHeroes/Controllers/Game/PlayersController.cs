﻿using Gomez.Core.Models.Exceptions;
using Gomez.Core.Web.Extensions;
using Gomez.RiseOfHeroes.Configurations;
using Gomez.RiseOfHeroes.Controllers.Game.Shared;
using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Players;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using VmNpcShop = Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcShops;

namespace Gomez.RiseOfHeroes.Controllers.Game
{
    [Authorize]
    [Route(RouteContant.API_AREA_GAME + "[controller]")]
    public class PlayersController : GameBaseController<PlayersController>
    {
        public PlayersController(ApplicationDbContext db, 
            UserManager<ApplicationUser> um, 
            IStringLocalizerFactory localizerFactory, 
            ILogger<PlayersController> logger) : base(db, um, localizerFactory, logger)
        {
        }

        [HttpPost]
        public async Task<IActionResult> Post()
        {
            var userId = this.User.GetUserId<Guid>();
            await _gameHandler.PlayerHandler.NewPlayerAsync(userId);
            return Ok();
        }

        [HttpGet("Basic")]
        public async Task<IActionResult> GetBasicInformation()
        {
            var userId = this.User.GetUserId<Guid>();
            try { 
                var vm = await _gameHandler.PlayerHandler.GetBasicInformationAsync(userId);
                return Ok(vm);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPatch("Basic")]
        public async Task<IActionResult> PatchBasic([FromBody]QuerySetBasicViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var userId = this.User.GetUserId<Guid>();
                await _gameHandler.PlayerHandler.SetBasicAsync(userId,vm);
                return Ok(vm.Name);
            }

            return BadRequest();
        }

        [HttpPatch("Attributes")]
        public async Task<IActionResult> PatchAttributes([FromBody]QuerySetAttributeViewModel vm)
        {
            if (ModelState.IsValid)
            {
                var userId = this.User.GetUserId<Guid>();
                var result = await _gameHandler.PlayerHandler.SetAttributesAsync(userId, vm);
                if (result)
                {
                    return Ok();
                }
            }

            return BadRequest();
        }

        [HttpGet("Inventory")]
        public async Task<IActionResult> Inventory(QueryInventoryViewModel query)
        {
            var userId = this.User.GetUserId<Guid>();
            var vm = await _gameHandler.PlayerHandler.GetInventoryAsync(userId, query);
            return Ok(vm);
        }

        /// <summary>
        /// Id is the npc
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet("Inventory/Sell/{id}")]
        public async Task<IActionResult> InventorySell(VmNpcShop.QueryPlayerInventoryViewModel query)
        {
            var userId = this.User.GetUserId<Guid>();
            var vm = await _gameHandler.PlayerHandler.GetSaleInventoryAsync(userId, query);
            return Ok(vm);
        }

        [HttpGet("Inventory/{id}/Use")]
        public async Task<IActionResult> UseItem(long id)
        {
            var userId = this.User.GetUserId<Guid>();
            var vm = await _gameHandler.PlayerHandler.UseItemAsync(userId, id);
            return Ok(vm);
        }

        /// <summary>
        /// The seller is found by item id.
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost("Inventory/{id}/Sell")]
        public async Task<IActionResult> SellItem(VmNpcShop.QuerySellViewModel query)
        {
            var userId = this.User.GetUserId<Guid>();
            var vm = await _gameHandler.ShopHandler.BuyAsync(userId, query.BuyerId, query.Id, query.Qty);
            return Ok(vm);
        }

        [HttpPatch("Travel/{id}")]
        public async Task<IActionResult> PatchTravel(Guid id)
        {
            if (ModelState.IsValid)
            {
                var userId = this.User.GetUserId<Guid>();
                var result = await _gameHandler.PlayerHandler.TravelAsync(userId, id, _gameHandler.LocationHandler);
                return Ok(result);
            }

            return BadRequest();
        }

        [HttpGet("Equipment")]
        public async Task<IActionResult> GetEquipment()
        {
            var userId = this.User.GetUserId<Guid>();
            var vm = await _gameHandler.PlayerHandler.GetEquipmentAsync(userId);
            return Ok(vm);
        }

        [HttpGet("Equipment/{id}/Unequip")]
        public async Task<IActionResult> Unequip(long id)
        {
            var userId = this.User.GetUserId<Guid>();
            var vm = await _gameHandler.PlayerHandler.UnequipItemAsync(userId, id);
            return Ok(vm);
        }


        [HttpDelete]
        public async Task<IActionResult> Delete()
        {
            var userId = this.User.GetUserId<Guid>();
            if(await _gameHandler.PlayerHandler.DeleteAsync(userId))
            {
                return Ok();
            }

            return BadRequest();
        }
    }
}
