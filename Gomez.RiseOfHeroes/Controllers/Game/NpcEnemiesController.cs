﻿using Gomez.Core.Web.Extensions;
using Gomez.RiseOfHeroes.Configurations;
using Gomez.RiseOfHeroes.Controllers.Game.Shared;
using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using VmNpcEnemy = Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcEnemies;

namespace Gomez.RiseOfHeroes.Controllers.Game
{
    [Authorize]
    [Route(RouteContant.API_AREA_GAME + "[controller]")]
    public class NpcEnemiesController : GameBaseController<NpcEnemiesController>
    {
        public NpcEnemiesController(ApplicationDbContext db, 
            UserManager<ApplicationUser> um, 
            IStringLocalizerFactory localizerFactory, 
            ILogger<NpcEnemiesController> logger) : base(db, um, localizerFactory, logger)
        {
        }

        [HttpGet]
        public async Task<IActionResult> Get(QueryIndexViewModel query)
        {
            var vm = await _gameHandler.NpcEnemyHandler.GetIndexAsync(query);
            return Ok(vm);
        }


        [HttpGet("{id}/Basic")]
        public async Task<IActionResult> GetBasicInformation(int id)
        {
            var vm = await _gameHandler.NpcEnemyHandler.GetBasicInformationAsync(id);
            return Ok(vm);
        }

        [HttpPost("{id}/AttackSingle")]
        public async Task<IActionResult> PostAttackSingle(VmNpcEnemy.QueryAttackSingleViewModel vm)
        {
            var userId = this.User.GetUserId<Guid>();
            var result = await _gameHandler.AttackHandler.AttackSingleNpcAsync(userId, vm.Id, vm.Option);
            return Ok(result);
        }

        [HttpPost("{id}/InLocation")]
        public async Task<IActionResult> InLocation(QueryInLocationViewModel query)
        {
            var vm = await _gameHandler.NpcEnemyHandler.InInSamePositionAsync(query.Id, query.Coordinate);
            return Ok(vm);
        }

        [HttpGet("{id}/Equipment")]
        public async Task<IActionResult> GetEquipment(int id)
        {
            var vm = await _gameHandler.NpcEnemyHandler.GetEquipmentAsync(id);
            return Ok(vm);
        }
    }
}
