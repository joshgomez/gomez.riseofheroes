﻿using Gomez.RiseOfHeroes.BusinessLogic.Areas.Game;
using Gomez.RiseOfHeroes.Controllers.Shared;
using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Gomez.RiseOfHeroes.Models.Contexts;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace Gomez.RiseOfHeroes.Controllers.Game.Shared
{
    public abstract class GameBaseController<TController> : MainApiController<TController> where TController : ControllerBase
    {
        protected readonly GameHandler _gameHandler;
        protected GameBaseController(ApplicationDbContext db, 
            UserManager<ApplicationUser> um, 
            IStringLocalizerFactory localizerFactory, 
            ILogger<TController> logger) : base(db, um, localizerFactory, logger)
        {
            _gameHandler = new GameHandler(db, localizerFactory);
        }
    }
}
