﻿using Gomez.Core.Models.Exceptions;
using Gomez.Core.Web.Extensions;
using Gomez.RiseOfHeroes.Configurations;
using Gomez.RiseOfHeroes.Controllers.Game.Shared;
using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.Resources;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcCustoms;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.Controllers.Game
{
    [Authorize]
    [Route(RouteContant.API_AREA_GAME + "[controller]")]
    public class NpcCustomsController : GameBaseController<NpcCustomsController>
    {
        public NpcCustomsController(ApplicationDbContext db, 
            UserManager<ApplicationUser> um, 
            IStringLocalizerFactory localizerFactory, 
            ILogger<NpcCustomsController> logger) : base(db, um, localizerFactory, logger)
        {
        }

        [HttpGet]
        public async Task<IActionResult> Get(QueryIndexViewModel query)
        {
            var vm = await _gameHandler.NpcCustomHandler.GetIndexAsync(query);
            return Ok(vm);
        }


        [HttpGet("{id}/Basic")]
        public async Task<IActionResult> GetBasicInformation(int id)
        {
            var vm = await _gameHandler.NpcCustomHandler.GetBasicInformationAsync(id);
            return Ok(vm);
        }

        [HttpPost("{id}/InLocation")]
        public async Task<IActionResult> InLocation(QueryInLocationViewModel query)
        {
            var vm = await _gameHandler.NpcCustomHandler.InInSamePositionAsync(query.Id,query.Coordinate);
            return Ok(vm);
        }

        [HttpGet("{id}/WorkDetails")]
        public async Task<IActionResult> WorkDetails(int id, [FromQuery]string type)
        {
            var vm = await _gameHandler.NpcCustomHandler.GetWorkDetailsAsync(id, type);
            return Ok(vm);
        }

        [HttpPost("{id}/Work")]
        public async Task<IActionResult> Work(QueryWorkViewModel query)
        {
            var userId = this.User.GetUserId<Guid>();
            try
            {
                var vm = await _gameHandler.WorkHandler.DoWorkAsync(query, userId);
                return Ok(vm);
            }
            catch(UnavailableException)
            {
                var localizer = _localizerFactory.Create(typeof(GameResource));
                return Conflict(localizer["The work is already taken, please try again."].ToString());
            }
        }
    }
}
