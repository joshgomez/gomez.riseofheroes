﻿using Gomez.RiseOfHeroes.Configurations;
using Gomez.RiseOfHeroes.Controllers.Game.Shared;
using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.ItemDetails;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.Controllers.Game
{
    [Authorize]
    [Route(RouteContant.API_AREA_GAME + "[controller]")]
    public class ItemsController : GameBaseController<ItemsController>
    {
        public ItemsController(ApplicationDbContext db,
            UserManager<ApplicationUser> um,
            IStringLocalizerFactory localizerFactory,
            ILogger<ItemsController> logger) : base(db, um, localizerFactory, logger)
        {
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get([FromRoute]long id)
        {
            var query = new GetViewModel
            {
                Property = "*"
            };

            var vm = await _gameHandler.ItemHandler.GetAsync(id, query);
            return Ok(vm);
        }

        [HttpGet("{id}/Description")]
        public async Task<IActionResult> GetDescription([FromRoute]long id)
        {
            var vm = await _gameHandler.ItemHandler.GetDescriptionAsync(id);
            return Ok(vm);
        }
    }
}
