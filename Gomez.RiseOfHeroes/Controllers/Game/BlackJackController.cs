﻿using Gomez.Core.Web.Extensions;
using Gomez.RiseOfHeroes.BusinessLogic.Areas.Game.MiniGames;
using Gomez.RiseOfHeroes.Configurations;
using Gomez.RiseOfHeroes.Controllers.Game.Shared;
using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.MiniGames.BlackJack;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.Controllers.Game
{
    [Authorize]
    [Route(RouteContant.API_AREA_GAME + "[controller]")]
    public class BlackJackController : GameBaseController<BlackJackController>
    {
        private readonly BlackJackHandler _blackjack;

        public BlackJackController(ApplicationDbContext db, 
            UserManager<ApplicationUser> um, 
            IStringLocalizerFactory localizerFactory, 
            ILogger<BlackJackController> logger) : base(db, um, localizerFactory, logger)
        {
            //loaded from GameBase Controller.
            _blackjack = _gameHandler.MiniGameHandler.BlackJack;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var userId = this.User.GetUserId<Guid>();

            var playerVm = await _gameHandler.PlayerHandler.GetBasicInformationAsync(userId);
            var npcVm = await _gameHandler.NpcCustomHandler.GetBasicInformationAsync(id);

            if(await _blackjack.LoadAsync(npcVm, playerVm))
            {
                return Ok(_blackjack.GetResults());
            }
           
            //No game, request a new game using another call to the api.
            return NoContent();
        }

  
        [HttpPost("{id}")]
        public async Task<IActionResult> Post(QueryNewViewModel query)
        {
            var userId = this.User.GetUserId<Guid>();

            var playerVm = await _gameHandler.PlayerHandler.GetBasicInformationAsync(userId);
            var npcVm = await _gameHandler.NpcCustomHandler.GetBasicInformationAsync(query.Id);

            await _gameHandler.MiniGameHandler.BlackJack.NewAsync(npcVm,playerVm,query.Stake);
            return Ok(_blackjack.GetResults());
        }

        [HttpGet("{id}/Stay")]
        public async Task<IActionResult> GetStay(int id)
        {
            var userId = this.User.GetUserId<Guid>();

            var playerVm = await _gameHandler.PlayerHandler.GetBasicInformationAsync(userId);
            var npcVm = await _gameHandler.NpcCustomHandler.GetBasicInformationAsync(id);

            if (await _blackjack.LoadAsync(npcVm, playerVm))
            {
                await _blackjack.StayAsync();
                return Ok(_blackjack.GetResults());
            }

            //No game, request a new game using another call to the api.
            return NoContent();
        }

        [HttpGet("{id}/Hit")]
        public async Task<IActionResult> GetHit(int id)
        {
            var userId = this.User.GetUserId<Guid>();

            var playerVm = await _gameHandler.PlayerHandler.GetBasicInformationAsync(userId);
            var npcVm = await _gameHandler.NpcCustomHandler.GetBasicInformationAsync(id);

            if (await _blackjack.LoadAsync(npcVm, playerVm))
            {
                await _blackjack.HitAsync();
                return Ok(_blackjack.GetResults());
            }

            //No game, request a new game using another call to the api.
            return NoContent();
        }
    }
}
