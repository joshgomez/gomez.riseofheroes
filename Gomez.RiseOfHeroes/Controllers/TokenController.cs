﻿using Gomez.Core.ViewModels.Areas.Identity;
using Gomez.Core.Ef.Web.Services;
using Gomez.RiseOfHeroes.Configurations;
using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Gomez.RiseOfHeroes.Models.Contexts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.Controllers
{
    [Route(RouteContant.API + "[controller]")]
    public class TokenController : Gomez.Core.Ef.Web.Controllers.TokenApiController<ApplicationDbContext,ApplicationUser,ApplicationRole, TokenController>
    {
        public TokenController(ApplicationDbContext db, 
            UserManager<ApplicationUser> um, 
            IStringLocalizerFactory localizerFactory, 
            IJwtTokenService<ApplicationUser> tokenService, 
            RoleManager<ApplicationRole> rm,
            SignInManager<ApplicationUser> sim,
            ILogger<TokenController> logger) : base(db, um, localizerFactory, tokenService, rm,sim, logger)
        {
        }

        [HttpPost]
        public Task<IActionResult> Post(LoginForm vm)
        {
            return base.Login(vm);
        }

        [HttpPost("Refresh")]
        public Task<IActionResult> PostRefresh(QueryRefreshTokenViewModel query)
        {
            return base.Refresh(query);
        }

        [HttpDelete]
        [Authorize]
        public Task<IActionResult> Delete()
        {
            return base.Revoke();
        }
    }
}
