﻿using Gomez.Core.ViewModels.Areas.Identity;
using Gomez.Core.Ef.Web.Services;
using Gomez.RiseOfHeroes.Configurations;
using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Gomez.RiseOfHeroes.Models.Contexts;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.Controllers
{
    [Route(RouteContant.API + "[controller]")]
    public class AccountController : Gomez.Core.Ef.Web.Controllers.TokenApiController<ApplicationDbContext,ApplicationUser,ApplicationRole, AccountController>
    {
        public AccountController(ApplicationDbContext db, 
            UserManager<ApplicationUser> um, 
            IStringLocalizerFactory localizerFactory, 
            IJwtTokenService<ApplicationUser> tokenService, 
            RoleManager<ApplicationRole> rm,
            SignInManager<ApplicationUser> sim,
            ILogger<AccountController> logger) : base(db, um, localizerFactory, tokenService, rm,sim, logger)
        {
        }

        [HttpPost]
        public async Task<IActionResult> Post(RegisterForm vm)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = vm.Email,
                    Email = vm.Email
                };

                IdentityResult result = await _userManager.CreateAsync(user, vm.Password);
                if (!result.Succeeded)
                {
                    return BadRequest(result.Errors.Select(x => x.Description).FirstOrDefault());
                }

                return await base.Login(new LoginForm()
                {
                    Email = user.Email,
                    Password = vm.Password
                });
            }

            return BadRequest(ModelState.Values.SelectMany(x => x.Errors)
                .Select(x => x.ErrorMessage).FirstOrDefault());
        }
    }
}
