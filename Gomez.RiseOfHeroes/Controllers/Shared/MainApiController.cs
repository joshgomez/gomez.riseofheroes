﻿using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Gomez.RiseOfHeroes.Models.Contexts;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace Gomez.RiseOfHeroes.Controllers.Shared
{
    public class MainApiController<TController> : Gomez.Core.Ef.Web.Controllers.MainApiController<ApplicationDbContext, ApplicationUser, TController> 
        where TController : ControllerBase
    {
        public MainApiController(ApplicationDbContext db, IStringLocalizerFactory localizerFactory) : base(db, localizerFactory)
        {
        }

        public MainApiController(ApplicationDbContext db, UserManager<ApplicationUser> um, IStringLocalizerFactory localizerFactory) : base(db, um, localizerFactory)
        {

        }

        public MainApiController(ApplicationDbContext db, UserManager<ApplicationUser> um, IStringLocalizerFactory localizerFactory, ILogger<TController> logger) : base(db, um, localizerFactory, logger)
        {

        }
    }
}
