﻿using Gomez.Core.Localization;
using Gomez.RiseOfHeroes.Configurations;
using Gomez.RiseOfHeroes.Controllers.Shared;
using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Gomez.RiseOfHeroes.Models.Contexts;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using IO = System.IO;

namespace Gomez.RiseOfHeroes.Controllers
{
    [Route(RouteContant.API + "[controller]")]
    public class LocalizationController : MainApiController<LocalizationController>
    {
        private readonly IWebHostEnvironment _env;

        public LocalizationController(ApplicationDbContext db, 
            UserManager<ApplicationUser> um, 
            IStringLocalizerFactory localizerFactory, 
            ILogger<LocalizationController> logger,
            IWebHostEnvironment env) : base(db, um, localizerFactory, logger)
        {
            _env = env;
        }

        [HttpGet]
        public async Task<IActionResult> Get(string id)
        {
            await Task.Yield();
            var basePath = IO.Path.Combine(_env.ContentRootPath, "App_Data", "Localization");
            var helper = new LocalizationHelper(_localizerFactory, typeof(Gomez.RiseOfHeroes.Resources.SharedResource));
            return Ok(helper.LoadJson(basePath, id));
        }
    }
}
