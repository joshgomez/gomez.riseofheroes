﻿namespace Gomez.RiseOfHeroes.Configurations
{
    public static class RouteContant
    {
        public const string API = "api/";
        public const string API_AREA_REPORTS = API + "reports/";
        public const string API_AREA_ADMINISTRATION = API + "administration/";
        public const string API_AREA_TESTS = API + "tests/";
        public const string API_AREA_GAME = API + "game/";
    }
}
