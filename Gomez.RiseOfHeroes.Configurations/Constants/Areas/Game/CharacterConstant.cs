﻿namespace Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game
{
    public static class CharacterConstant
    {
        public const string CACHE_KEY_DESC = "Char_Desc_{0}";

        public const int TYPE_PLAYER = 0;
        public const int TYPE_NPC_ENEMY = 1;
        public const int TYPE_NPC_SHOP = 2;
        public const int TYPE_NPC_CUSTOM = 3;

        public const string KEY_MONEY       = "Money";
        public const string KEY_HAS_IMAGE   = "Has.Image";
        
        public const string KEY_STATS_LEVEL = "Stats.Level";
        public const string KEY_STATS_ENERGY = "Stats.Energy";
        public const string KEY_STATS_HEALTH = "Stats.Health";
        public const string KEY_STATS_ARMOR = "Stats.Armor";

        /// <summary>
        /// measuring physical power and carrying capacity
        /// </summary>
        public const string KEY_ATTR_STRENGTH        = "Attr.Strength";
        /// <summary>
        /// measuring endurance, stamina and good health
        /// </summary>
        public const string KEY_ATTR_CONSTITUTION    = "Attr.Constitution";
        /// <summary>
        /// measuring agility, balance, coordination and reflexes
        /// </summary>
        public const string KEY_ATTR_DEXTERITY       = "Attr.Dexterity";
        /// <summary>
        /// Intelligence - measuring deductive reasoning, knowledge, memory, logic and rationality
        /// </summary>
        public const string KEY_ATTR_INTELLIGENCE    = "Attr.Intelligence";

        public const string KEY_EQUIPMENT_HEADGEAR_ID       = "Equipment.HeadGear.Id";
        public const string KEY_EQUIPMENT_BODYGEAR_ID       = "Equipment.BodyGear.Id";
        public const string KEY_EQUIPMENT_WEAPON_ID         = "Equipment.Weapon.Id";

    }
}
