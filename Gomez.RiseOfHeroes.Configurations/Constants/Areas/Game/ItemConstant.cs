﻿namespace Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game
{
    public static class ItemConstant
    {
        public const string CACHE_KEY_DESC = "Item_Desc_{0}";

        public const int TYPE_MISC      = 0;
        public const int TYPE_WEAPON    = 1;
        public const int TYPE_HEAD_GEAR = 2;
        public const int TYPE_BODY_GEAR = 3;
        public const int TYPE_POTION    = 4;

        public const string KEY_STATS_QUANTITY      = "Stats.Quantity";
        public const string KEY_STATS_COST          = "Stats.Cost";
        public const string KEY_STATS_LEVEL         = "Stats.Level";
        public const string KEY_STATS_HEALTH        = "Stats.Health";
        public const string KEY_STATS_ARMOR         = "Stats.Armor";
        public const string KEY_STATS_DAMAGE        = "Stats.Damage";
        public const string KEY_STATS_WEIGHT        = "Stats.Weight";

        public const string KEY_HAS_IMAGE           = "Has.Image";

        /// <summary>
        /// measuring physical power and carrying capacity
        /// </summary>
        public const string KEY_ATTR_STRENGTH        = "Attr.Strength";
        /// <summary>
        /// measuring endurance, stamina and good health
        /// </summary>
        public const string KEY_ATTR_CONSTITUTION    = "Attr.Constitution";
        /// <summary>
        /// measuring agility, balance, coordination and reflexes
        /// </summary>
        public const string KEY_ATTR_DEXTERITY       = "Attr.Dexterity";
        /// <summary>
        /// Intelligence - measuring deductive reasoning, knowledge, memory, logic and rationality
        /// </summary>
        public const string KEY_ATTR_INTELLIGENCE    = "Attr.Intelligence";
    }
}
