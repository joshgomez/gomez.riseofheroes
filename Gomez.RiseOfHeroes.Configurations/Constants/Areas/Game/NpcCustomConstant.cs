﻿namespace Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game
{
    public static class NpcCustomConstant
    {
        public const string KEY_WORK_MUDLARK        = "Work.Mudlark";
        public const string KEY_WORK_QUARRYMAN      = "Work.Quarryman";
        public const string KEY_WORK_BUSKER         = "Work.Busker";
        public const string KEY_WORK_LAMPLIGHTER    = "Work.Lamplighter";
        public const string KEY_WORK_FULLER         = "Work.Fuller";
        public const string KEY_WORK_BARTENDER      = "Work.Bartender";
        public const string KEY_WORK_WATCHMAN       = "Work.Watchman";
        public const string KEY_WORK_COURTJESTER    = "Work.CourtJester";

        public const string KEY_GAME_BLACKJACK  = "Game.BlackJack";
        public const string KEY_GAME_ROULETTE   = "Game.Roulette";
        public const string KEY_GAME_SLOTS      = "Game.Slots";

        public const string KEY_CHAT            = "Chat";
    }
}
