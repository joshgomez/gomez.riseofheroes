﻿namespace Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game
{
    public static class NpcShopConstant
    {
        public const string KEY_CAN_BUY_TYPES   = "CanBuyTypes";
        public const int DEMAND_MULTIPLIER      = 10;
    }
}
