﻿namespace Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game
{
    public static class PlayerConstant
    {
        public const string KEY_STATS_EXPERIENCE        = "Stats.Experience";
        public const string KEY_STATS_ATTRIBUTEPOINTS   = "Stats.AttributePoints";
        public const string KEY_TIMESTAMP_ENERGY        = "Timestamp.Energy";

        public const string KEY_P_GAME_BLACKJACK_STATE = "_Game.BlackJack.State";
    }
}
