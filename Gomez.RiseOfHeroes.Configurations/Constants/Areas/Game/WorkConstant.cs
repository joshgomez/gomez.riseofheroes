﻿namespace Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game
{
    public static class WorkConstant
    {
        public const int ANY = 0;
        public const int STR = 1;
        public const int CON = 2;
        public const int DEX = 3;
        public const int INT = 4;
    }
}
