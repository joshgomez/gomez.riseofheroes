﻿namespace Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game.Seed
{
    public static class ItemSeedConstant
    {
        public const string ID_MINOR_HEALTH_POITION     = "7083AE6C-C82D-40F7-BA3E-DB2B40AE7D27";
        public const string ID_LEATHER_HELMET           = "67F935E9-0BD7-43A2-8515-69ADD6ED362B";
        public const string ID_LEATHER_ARMOR            = "F38D2ACA-5645-4BE2-8407-AA51CA88C093";

        public const string ID_WOLF_PELT                = "03369820-AE44-4C5B-BBF8-FAD48DF88D54";
        public const string ID_SNAKE_SKIN               = "721D6553-E6F3-40A5-997E-35A44ED5D4BA";
        public const string ID_YETI_HORN                = "8F7B2AC0-C31B-4DCA-853E-D2061952A225";
        public const string ID_SLIME_BLOB               = "0F065147-41B2-478B-B55B-01C84E26496C";

        public const string ID_1HSWORD_WOODEN           = "774A3A7A-DAD0-4EF7-B78C-4FECBF6D7303";
        public const string ID_1HSWORD_BRONZE           = "1A2F565C-5ED6-4D76-A097-7BC942C0876F";
        public const string ID_1HSWORD_IRON             = "CE914758-FDA0-453E-9EB9-704FCE6A686C";

    }
}
