﻿namespace Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game.Seed
{
    public static class LocationSeedConstant
    {
        public const string ID_TOWN_OF_HAIWOOD          = "4B5A5893-0DE9-42E1-B8A3-F0DE3B75AFB5";
        public const string ID_HAIWOOD_FOREST           = "8AD83B8B-48BA-4EE2-A804-0E46B5588CCB";
        public const string ID_CITY_OF_SEFU             = "2D23C4F7-1865-420C-A846-E2AD9A4F0047";
        public const string ID_DARKWOOD                 = "8762C429-3D64-4C08-81F0-8557A16BD8B2";
        public const string ID_WITCHWOOD                = "A3CC2875-5207-4AC9-A215-EC0ED8F2DBFE";
        public const string ID_FROSTYMOUNTAIN           = "E08B855A-7CEB-4034-B69F-F2889D35395C";
        public const string ID_JUNGLE_OF_IVY            = "49266268-7039-4A06-BF53-5B1EE595359C";
    }
}
