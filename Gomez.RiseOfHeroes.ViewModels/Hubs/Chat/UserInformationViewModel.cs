﻿using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters;
using NetTopologySuite.Geometries;

namespace Gomez.RiseOfHeroes.ViewModels.Hubs.Chat
{
    public class UserInformationViewModel : MinimumInformationViewModel
    {
        public UserInformationViewModel()
        {

        }

        public UserInformationViewModel(MinimumInformationViewModel baseVm)
        {
            this.CharacterId = baseVm.CharacterId;
            this.Name = baseVm.Name;
            this.Location = baseVm.Location ?? new Point(0,0);
        }
    }
}
