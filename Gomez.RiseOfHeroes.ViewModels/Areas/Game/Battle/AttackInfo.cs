﻿using Gomez.RiseOfHeroes.Models.Areas.Game.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Battle
{
    public class AttackInfo
    {
        public double Damage { get; set; }
        public AttackType Type { get; set; }
    }
}
