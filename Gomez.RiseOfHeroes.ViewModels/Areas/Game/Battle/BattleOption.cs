﻿using Gomez.RiseOfHeroes.Models.Areas.Game.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Battle
{
    public class BattleOption
    {
        public TargetType Target { get; set; }
        public TacticType Tactic { get; set; }

        public double RetreatAtHealthFactor { get; set; }

        public double RetreatAtHealthPercent
        {
            get { return RetreatAtHealthFactor * 100d; }
            set { RetreatAtHealthFactor = value / 100d; }
        }

    }
}
