﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Battle
{
    public class ResultViewModel
    {
        public class BattleRound
        {
            public BattleRound()
            {
                Messages = new List<string>();
            }

            public int Round { get; set; }
            public List<string> Messages { get; set; }
        }

        public ResultViewModel()
        {
            Rounds = new Dictionary<int, BattleRound>();
        }

        public Dictionary<int, BattleRound> Rounds { get; set; }
    }
}
