﻿using Gomez.RiseOfHeroes.Models.Areas.Game.Enums;
using System;
using System.Collections.Generic;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Battle
{
    public class ResultInfo
    {
        public Dictionary<Guid,string> Items { get; set; }
        public BattleResultType Result { get; set; }
        public decimal Money { get; set; }
        public IEnumerable<KeyValuePair<string, object[]>> EventLog { get; set; }
    }
}
