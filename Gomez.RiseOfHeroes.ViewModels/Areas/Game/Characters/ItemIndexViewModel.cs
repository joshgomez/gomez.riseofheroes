﻿namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters
{
    public class ItemIndexViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
