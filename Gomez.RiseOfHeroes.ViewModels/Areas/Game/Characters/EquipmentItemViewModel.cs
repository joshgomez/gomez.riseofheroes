﻿using System;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters
{
    public class EquipmentItemViewModel
    {
        public long Id { get; set; }
        public Guid DetailId { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
    }
}
