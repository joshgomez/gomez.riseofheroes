﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters
{
    public class CharacterIndexViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Name")]
        [Required]
        [MaxLength(128)]
        public string Name { get; set; }
    }
}
