﻿using Gomez.Core.Models.Entity;
using Gomez.Core.ViewModels.Shared;
using System;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters
{
    public class QueryIndexViewModel : PagedQueryBaseModel<QueryIndexViewModel, IndexViewModel>
    {
        public Guid? LocationId { get; set; }
    }

    public class IndexViewModel : QueryIndexViewModel
    {
        public IndexViewModel(QueryIndexViewModel query) : base()
        {
            this.Update(query);
        }

        public PagedResult<CharacterIndexViewModel> Characters { get; set; }
    }
}
