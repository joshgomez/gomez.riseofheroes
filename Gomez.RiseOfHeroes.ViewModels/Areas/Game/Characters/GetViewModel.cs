﻿using Gomez.Core.ViewModels;
using Gomez.Core.ViewModels.Shared;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters
{
    public class QueryGetViewModel : QueryBaseModel<QueryGetViewModel, GetViewModel>
    {
        [FromRoute]
        public int Id { get; set; }
        public string Property { get; set; }
    }

    public class GetViewModel : QueryGetViewModel, IDynamicProperties
    {
        public GetViewModel() : base()
        {
            DynamicProperties = new Dictionary<string, object>();
        }

        public GetViewModel(QueryGetViewModel query) : this()
        {
            this.Update(query);
        }

        public CharacterBaseViewModel Character { get; set; }
        public SimplePropertyDtoCollection PropertyCollection { get; set; }
        public Dictionary<string, object> DynamicProperties { get; set; }

        public T GetPropertyByType<T>()
        {
            var nameOfType = typeof(T).Name;
            if (DynamicProperties.ContainsKey(nameOfType))
            {
                return (T)DynamicProperties[nameOfType];
            }

            return default;
        }
    }
}
