﻿using Gomez.Core.ViewModels.Shared;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters
{
    public interface IQueryInventoryViewModel : IPagedQueryBaseModel
    {
        /// <summary>
        /// CharacterId
        /// </summary>
        int Id { get; set; }
    }
}
