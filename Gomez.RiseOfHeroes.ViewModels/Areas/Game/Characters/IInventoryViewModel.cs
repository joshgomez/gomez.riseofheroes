﻿using Gomez.Core.Models.Entity;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters
{
    public interface IInventoryViewModel<TItem> where TItem : ItemIndexViewModel
    {
        PagedResult<TItem> Items { get; set; }
    }
}
