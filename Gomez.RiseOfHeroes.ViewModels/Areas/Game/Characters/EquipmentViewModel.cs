﻿namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters
{

    public class EquipmentViewModel
    {
        public EquipmentItemViewModel Head { get; set; }
        public EquipmentItemViewModel Body { get; set; }
        public EquipmentItemViewModel Weapon { get; set; }
    }
}
