﻿namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters
{
    public class BasicInformationViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Health { get; set; }
        public double MaxHealth { get; set; }

        public double Energy { get; set; }
        public double MaxEnergy { get; set; }

        public double MaxDamage { get; set; }
        public double MinDamage { get; set; }

        public int Strength { get; set; }
        public int Constitution { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Experience { get; set; }
        public int Level { get; set; }

        public double Weight { get; set; }
        public int Armor { get; set; }
        public decimal Money { get; set; }

        public bool HasImage { get; set; }
    }
}
