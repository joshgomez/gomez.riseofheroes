﻿using Gomez.Core.Models.Shared;
using Gomez.Core.ViewModels.Shared;
using Gomez.RiseOfHeroes.Calculations;
using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Battle;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters
{
    public class CharacterViewModel : CharacterViewModel<CharacterCalculation>, ICoordinateModel
    {
        public CharacterViewModel(GetViewModel baseVm) : base(baseVm)
        {
        }
    }

    public class CharacterViewModel<TCalc> where TCalc : CharacterCalculation
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public BattleOption Option { get; set; }
        public TCalc Calculation { get; set; }
        public SimplePropertyDtoCollection Properties { get; set; }
        public ICoordinate Coordinate { get; set; }

        public bool Fallen => Calculation.Died || Calculation.Health < Calculation.MaxHealth * Option.RetreatAtHealthFactor;

        public CharacterViewModel(GetViewModel baseVm)
        {
            Calculation = baseVm.GetPropertyByType<CharacterCalculation>() as TCalc;
            Properties = baseVm.PropertyCollection;
            Id = baseVm.Id;
            Name = baseVm.Character.Name;
            Type = baseVm.Character.Type;
            Coordinate = new Locations.LocationCoordinate(baseVm.Character.Coordinate);
        }
    }
}
