﻿
using Gomez.Core.Models.Shared;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters
{
    public class CharacterBaseViewModel : ICoordinateModel
    {
        public CharacterBaseViewModel()
        {
            Coordinate = new Locations.LocationCoordinate() { X = 0, Y = 0 };
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public ICoordinate Coordinate { get; set; }
    }
}
