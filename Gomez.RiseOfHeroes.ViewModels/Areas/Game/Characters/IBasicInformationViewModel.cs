﻿namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters
{
    public interface IBasicInformationViewModel
    {
        int Id { get; set; }
        string Name { get; set; }
        int Level { get; set; }
    }
}
