﻿

using NetTopologySuite.Geometries;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters
{
    public class MinimumInformationViewModel
    {
        public int CharacterId { get; set; }
        public string Name { get; set; }
        public Point Location { get; set; }
    }
}
