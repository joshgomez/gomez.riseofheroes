﻿using Gomez.Core.Models.Shared;
using HybridModelBinding;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters
{
    public class QueryInLocationViewModel
    {
        public class CoordinateViewModel : ICoordinate
        {
            [HybridBindProperty(new[] { Source.Body, Source.Form })]
            public double X { get; set; }
            [HybridBindProperty(new[] { Source.Body, Source.Form })]
            public double Y { get; set; }
        }

        [HybridBindProperty(new[] { Source.Body, Source.Form, Source.QueryString, Source.Route })]
        public int Id { get; set; }
        public CoordinateViewModel Coordinate { get; set; }
    }
}
