﻿using System;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Players
{
    public class ReplenishEnergyViewModel
    {
        private readonly DateTime _currentTime;
        public ReplenishEnergyViewModel()
        {

        }

        public ReplenishEnergyViewModel(DateTime currentTime)
        {
            _currentTime = currentTime;
        }

        public DateTime NextUpdateAt { get; set; }
        public double NextUpdateAtMs => (NextUpdateAt - _currentTime).TotalMilliseconds;
        public double? Energy { get; set; }
    }
}
