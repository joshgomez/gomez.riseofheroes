﻿using Newtonsoft.Json;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Players
{
    public class QuerySetAttributeViewModel
    {
        public int Strength { get; set; }
        public int Constitution { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        [JsonIgnore]
        public int Total => Strength + Constitution + Dexterity + Intelligence;
    }
}
