﻿using Gomez.RiseOfHeroes.Models.Areas.Game.Enums;
using System;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Players
{
    public class ItemIndexViewModel : Characters.ItemIndexViewModel
    {
        public int Quantity { get; set; }
        public double Weight { get; set; }
        public double TotalWeight => Weight * Quantity;
        public ItemState State { get; set; }
        public bool HasImage { get; set; }
        public Guid DetailId { get; set; }
    }
}
