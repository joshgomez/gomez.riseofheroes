﻿using System.ComponentModel.DataAnnotations;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Players
{
    public class QuerySetBasicViewModel
    {
        [Required]
        [StringLength(Gomez.Core.Configurations.Constants.Validation.NAME_MAX_LENGHT)]
        public string Name { get; set; }
    }
}
