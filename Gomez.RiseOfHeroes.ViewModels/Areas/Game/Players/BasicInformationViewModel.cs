﻿using Gomez.Core.Models.Shared;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Players
{
    public class BasicInformationViewModel : Characters.BasicInformationViewModel, ICoordinateModel
    {
        public int NextLevel { get; set; }
        public double NextLevelFactor { get; set; }
        public int Attributes { get; set; }
        public ICoordinate Coordinate { get; set; }
    }
}
