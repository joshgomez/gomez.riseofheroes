﻿using Gomez.Core.ViewModels.Shared;
using HybridModelBinding;
using System.ComponentModel.DataAnnotations;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Players
{
    public class QueryPutViewModel : QueryBaseModel<QueryPutViewModel, PutViewModel>
    {
        public QueryPutViewModel() : base()
        {

        }

        [HybridBindProperty(new[] { Source.Body, Source.Form, Source.QueryString, Source.Route })]
        public int Id { get; set; }

        [Display(Name = "Name", Description = "Your name.")]
        [MaxLength(128)]
        public string Name { get; set; }
    }

    public class PutViewModel : QueryPutViewModel
    {
        public PutViewModel(QueryPutViewModel query) : base()
        {
            this.Update(query);
        }
    }
}
