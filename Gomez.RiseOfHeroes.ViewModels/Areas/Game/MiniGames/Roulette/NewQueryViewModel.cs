﻿using Gomez.Core.Utilities.Game.Models.Roulette;
using HybridModelBinding;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.MiniGames.Roulette
{
    public class NewQueryViewModel
    {
        [HybridBindProperty(new[] { Source.Route, Source.QueryString })]
        public int Id { get; set; }
        [HybridBindProperty(new[] { Source.Body, Source.Form })]
        public decimal Stake { get; set; }

        [HybridBindProperty(new[] { Source.Body, Source.Form })]
        public EvenOrOdd? EvenOrOdd { get; set; }
        [HybridBindProperty(new[] { Source.Body, Source.Form })]
        public SlotColor? Color { get; set; }

        [HybridBindProperty(new[] { Source.Body, Source.Form })]
        public int Number { get; set; }
        [HybridBindProperty(new[] { Source.Body, Source.Form })]
        public bool OneToEighteen { get; set; }
        [HybridBindProperty(new[] { Source.Body, Source.Form })]
        public bool NineteenToThirtySix { get; set; }
    }
}
