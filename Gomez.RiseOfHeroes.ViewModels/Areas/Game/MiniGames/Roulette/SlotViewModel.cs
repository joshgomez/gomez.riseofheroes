﻿using Gomez.Core.Utilities.Game.Models.Roulette;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.MiniGames.Roulette
{
    public class SlotViewModel
    {
        public SlotViewModel()
        {

        }

        public SlotViewModel(Slot slot)
        {
            Number = slot.Number;
            Color = slot.Color.ToString();
        }

        public int Number { get; set; }
        public string Color { get; set; }
    }
}
