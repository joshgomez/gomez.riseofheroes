﻿using Gomez.Core.Utilities.Game.Models.Roulette;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.MiniGames.Roulette
{
    public class GetViewModel
    {
        public decimal Stake { get; set; }
        public decimal Profit { get; set; }
        public int Number { get; set; }
        public GetViewModel()
        {

        }

        public GetViewModel(IRouletteTable game)
        {
            Stake = game.Stake;
            Profit = game.Profit;
            Number = game.Number;
        }
    }
}
