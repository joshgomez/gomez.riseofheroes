﻿using HybridModelBinding;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.MiniGames.Slots
{
    public class NewQueryViewModel
    {
        [HybridBindProperty(new[] { Source.Route, Source.QueryString })]
        public int Id { get; set; }
        [HybridBindProperty(new[] { Source.Body, Source.Form })]
        public decimal Credits { get; set; }
    }
}
