﻿using Gomez.Core.Utilities.Game.Models.Slots;
using System.Collections.Generic;
using System.Linq;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.MiniGames.Slots
{
    public class GetViewModel
    {
        public IEnumerable<string> Slots { get; set; }
        public decimal Credits { get; set; }
        public decimal Profit { get; set; }
        public GetViewModel()
        {

        }

        public GetViewModel(ISlotMachine game)
        {
            Credits = game.Credits;
            Profit = game.Profit;
            Slots = game.Slots.Select(x => x.ToString());
        }
    }
}
