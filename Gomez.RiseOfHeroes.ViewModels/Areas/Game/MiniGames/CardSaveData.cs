﻿using Gomez.Core.Utilities.Game.Models.Cards;
using AutoMapper;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.MiniGames
{
    public class CardSaveData : SaveData
    {
        public CardSaveData()
        {

        }

        public CardSaveData(SaveData data)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<SaveData, CardSaveData>());
            var mapper = config.CreateMapper();
            mapper.Map(data, this);
        }

        public int PlayerId { get; set; }
        public int NpcId { get; set; }
    }
}
