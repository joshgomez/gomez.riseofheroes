﻿using Gomez.Core.Utilities.Game.Models.Cards;
using System.Collections.Generic;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.MiniGames.BlackJack
{
    public class GetViewModel
    {
        public IEnumerable<string> DealersCards { get; set; }
        public IEnumerable<string> PlayersCards { get; set; }
        public string State { get; set; }
        public decimal Stake { get; set; }
        public decimal Profit { get; set; }

        public int DealerScore { get; set; }
        public int PlayerScore { get; set; }

        public bool CanStay { get; set; }

        public GetViewModel()
        {

        }

        public GetViewModel(IBlackJack game)
        {
            DealersCards = game.DealersCards();
            PlayersCards = game.PlayersCards();
            State = game.State.ToString();
            Stake = game.Stake;
            Profit = game.Profit;

            DealerScore = game.DealerScore;
            PlayerScore = game.PlayerScore;

            CanStay = game.CanStay;
        }
    }
}
