﻿using HybridModelBinding;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.MiniGames.BlackJack
{
    public class QueryNewViewModel
    {
        [HybridBindProperty(new[] { Source.Route, Source.QueryString })]
        public int Id { get; set; }
        [HybridBindProperty(new[] { Source.Body, Source.Form })]
        public decimal Stake { get; set; }
    }
}
