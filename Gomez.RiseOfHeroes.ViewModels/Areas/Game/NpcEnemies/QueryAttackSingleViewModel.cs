﻿using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Battle;
using HybridModelBinding;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcEnemies
{
    public class QueryAttackSingleViewModel
    {
        [HybridBindProperty(new[] { Source.Body, Source.Form, Source.QueryString, Source.Route })]
        public int Id { get; set; }

        [HybridBindProperty(new[] { Source.Body, Source.Form })]
        public BattleOption Option { get; set; }
    }
}
