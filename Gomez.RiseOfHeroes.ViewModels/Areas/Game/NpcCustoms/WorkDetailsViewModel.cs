﻿using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcCustoms
{
    public class WorkDetailsViewModel
    {
        public const int BASE_REQUIRED_AP = 5;

        private readonly static Dictionary<int,int> _map = new Dictionary<int, int>()
            {
                { 1, WorkConstant.ANY },
                { 2, WorkConstant.STR },
                { 3, WorkConstant.CON },
                { 4, WorkConstant.DEX },
                { 5, WorkConstant.INT },
                { 6, WorkConstant.ANY },

                { 7,    WorkConstant.CON   },
                { 8,    WorkConstant.ANY   },
                { 9,    WorkConstant.INT   },
                { 10,   WorkConstant.DEX   },
                { 11,   WorkConstant.ANY   },
                { 12,   WorkConstant.STR   },

                { 13, WorkConstant.DEX },
                { 14, WorkConstant.ANY },
                { 15, WorkConstant.STR },
                { 16, WorkConstant.INT },
                { 17, WorkConstant.ANY },
                { 18, WorkConstant.CON  },
                   
                { 19, WorkConstant.ANY },
                { 20, WorkConstant.STR },
                { 21, WorkConstant.CON },
                { 22, WorkConstant.DEX },
                { 23, WorkConstant.INT },
                { 0,  WorkConstant.ANY },
            };

        private readonly DateTime _currentTime;
        private readonly int _level;
        private readonly int _workAttribute;

        public WorkDetailsViewModel()
        {
            Strength = BASE_REQUIRED_AP;
            Constitution = BASE_REQUIRED_AP;
            Dexterity = BASE_REQUIRED_AP;
            Intelligence = BASE_REQUIRED_AP;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentTime"></param>
        /// <param name="level"></param>
        /// <param name="workAttribute">It is the attribute which the work is specialized on.</param>
        public WorkDetailsViewModel(DateTime currentTime, int level, int workAttribute) : this()
        {
            _currentTime = currentTime;
            _level = level;
            _workAttribute = workAttribute;
            SetAttributes();
        }

        private void SetAttributes()
        {
            int attributePoints = _level * BASE_REQUIRED_AP;
            int quotient = attributePoints / 4; //str,con,dex,int
            int remainder = attributePoints % 4; //str,con,dex,int
            quotient /= 2;

            int ap1 = quotient * 2;
            int ap2 = quotient / 2;
            if (_currentTime.Minute >= 20 && _currentTime.Minute < 40)
            {
                ap1 = quotient / 2;
                ap2 = quotient * 2;
            }
            else if (_currentTime.Minute >= 40)
            {
                ap1 = quotient;
                ap2 = quotient;
            }

            Strength += ap1;
            Constitution += ap2;
            Dexterity += ap2;
            Intelligence += ap1;

            switch (_map[_currentTime.Hour])
            {
                case WorkConstant.STR:
                    Strength += remainder;
                    break;
                case WorkConstant.CON:
                    Constitution += remainder;
                    break;
                case WorkConstant.DEX:
                    Constitution += remainder;
                    break;
                case WorkConstant.INT:
                    Constitution += remainder;
                    break;
            }

            switch (_workAttribute)
            {
                case WorkConstant.STR:
                    Strength += quotient;
                    break;
                case WorkConstant.CON:
                    Constitution += quotient;
                    break;
                case WorkConstant.DEX:
                    Constitution += quotient;
                    break;
                case WorkConstant.INT:
                    Constitution += quotient;
                    break;
            }
        }

        public string Type { get; set; }
        public int Strength { get; set; }
        public int Constitution { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
    }
}
