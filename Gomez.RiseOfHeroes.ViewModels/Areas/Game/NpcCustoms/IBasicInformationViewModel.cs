﻿using Gomez.Core.Models.Shared;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcCustoms
{
    public interface IBasicInformationViewModel : ICoordinateModel, Characters.IBasicInformationViewModel
    {
        decimal Money { get; set; }
        bool HasBlackJack { get; set; }
        bool HasSlotMachine { get; set; }
        bool HasRoulette { get; set; }
    }
}
