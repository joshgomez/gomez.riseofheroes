﻿using Gomez.Core.ViewModels.Shared;
using HybridModelBinding;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcCustoms
{
    public class QueryWorkViewModel : QueryBaseModel<QueryWorkViewModel, WorkViewModel>
    {
        [HybridBindProperty(new[] { Source.Body, Source.Form, Source.QueryString, Source.Route })]
        public int Id { get; set; }

        [HybridBindProperty(new[] { Source.Body, Source.Form })]
        public double Energy { get; set; }

        [HybridBindProperty(new[] { Source.Body, Source.Form })]
        public string Type { get; set; }
    }

    public class WorkViewModel : QueryWorkViewModel
    {
        public WorkDetailsViewModel Details { get; set; }
        public decimal Wage { get; set; }
        public double EnergyLeft { get; set; }
        public decimal TotalMoney { get; set; }

        public WorkViewModel() : base()
        {

        }

        public WorkViewModel(QueryWorkViewModel query) : this()
        {
            this.Update(query);
        }
    }
}
