﻿using Gomez.Core.Models.Shared;
using System.Collections.Generic;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcCustoms
{
    public class BasicInformationViewModel : Characters.BasicInformationViewModel, IBasicInformationViewModel
    {
        public ICoordinate Coordinate { get; set; }
        public bool HasBlackJack { get; set; }
        public bool HasSlotMachine { get; set; }
        public bool HasRoulette { get; set; }
        public Dictionary<string,int> WorkTypes { get; set; }
    }
}
