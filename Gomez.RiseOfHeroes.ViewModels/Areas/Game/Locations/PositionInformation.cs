﻿using Gomez.Core.Models.Helpers;
using Gomez.Core.Models.Shared;
using Gomez.RiseOfHeroes.Calculations;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Locations
{
    public class PositionInformation
    {
        private readonly ICoordinate _coordinate;

        public PositionInformation(ICoordinate coordinate)
        {
            _coordinate = coordinate;
        }

        public void Init(ICoordinate current)
        {
            if (current == null)
            {
                current = new LocationCoordinate();
            }

            Distance = LocationCalculation.CalculateDistance(NtsHelper.GetDistance(current, _coordinate));
        }

        public double Distance { get; private set; }

        public decimal Cost => Distance > 0 ? LocationCalculation.CalculateCost(Distance) : 0;
    }
}
