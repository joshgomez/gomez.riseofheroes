﻿using Gomez.Core.Models.Helpers;
using Gomez.Core.Models.Shared;
using Gomez.RiseOfHeroes.Calculations;
using NetTopologySuite.Geometries;
using System;
using System.ComponentModel.DataAnnotations;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Locations
{
    public class LocationIndexViewModel : ICoordinateModel
    {
        public Guid Id { get; set; }

        [Display(Name = "Name")]
        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        public ICoordinate Coordinate { get; set; }

        public void Init(ICoordinate current)
        {
            if(current == null)
            {
                current = new LocationCoordinate();
            }

            Distance = LocationCalculation.CalculateDistance(NtsHelper.GetDistance(current,Coordinate));
        }

        public double Distance { get; set; }

        public decimal Cost => Distance > 0 ? LocationCalculation.CalculateCost(Distance) : 0;
    }
}
