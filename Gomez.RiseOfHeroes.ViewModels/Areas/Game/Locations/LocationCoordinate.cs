﻿using Gomez.Core.Models.Shared;
using Gomez.Core.ViewModels.Shared;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Locations
{
    public class LocationCoordinate : ICoordinate
    {
        public LocationCoordinate()
        {

        }

        public LocationCoordinate(ICoordinate coord)
        {
            X = coord.X;
            Y = coord.Y;
        }

        public double X { get; set; }
        public double Y { get; set; }
    }
}
