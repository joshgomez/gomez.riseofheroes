﻿using Gomez.Core.Models.Shared;
using System;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Locations
{
    public class LocationBaseViewModel : ICoordinateModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ICoordinate  Coordinate { get; set; }
        public string Description { get; set; }
        public int HasBackgrounds { get; set; }
    }
}
