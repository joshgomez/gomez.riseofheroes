﻿using Gomez.Core.ViewModels.Shared;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Locations
{
    public class QueryGetViewModel : QueryBaseModel<QueryGetViewModel, GetViewModel>
    {
        [FromRoute]
        public Guid Id { get; set; }
    }

    public class GetViewModel : QueryGetViewModel
    {
        public GetViewModel() : base()
        {

        }

        public GetViewModel(QueryGetViewModel query) : this()
        {
            this.Update(query);
        }

        public LocationBaseViewModel Location { get; set; }
    }
}
