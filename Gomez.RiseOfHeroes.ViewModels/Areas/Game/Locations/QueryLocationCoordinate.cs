﻿using Gomez.Core.Models.Shared;
using HybridModelBinding;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Locations
{
    public class QueryLocationCoordinate : ICoordinate
    {
        [HybridBindProperty(new[] { Source.QueryString })]
        public double X { get; set; }

        [HybridBindProperty(new[] { Source.QueryString })]
        public double Y { get; set; }
    }
}
