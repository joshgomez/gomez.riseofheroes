﻿using Gomez.Core.Models.Entity;
using Gomez.Core.Models.Helpers;
using Gomez.Core.ViewModels.Shared;
using HybridModelBinding;
using NetTopologySuite.Geometries;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Locations
{
    public class QueryIndexViewModel : PagedQueryBaseModel<QueryIndexViewModel, IndexViewModel>
    {
        public int? CharacterId { get; set; }

        [HybridBindProperty(new[] {  Source.QueryString })]
        public QueryLocationCoordinate CurrentPosition { get; set; }
    }

    public class IndexViewModel : QueryIndexViewModel
    {
        public IndexViewModel(QueryIndexViewModel query) : base()
        {
            this.Update(query);
        }

        public Point GetCurrentPoint()
        {
            return NtsHelper.GetPoint(CurrentPosition);
        }

        public PagedResult<LocationIndexViewModel> Locations { get; set; }
    }
}
