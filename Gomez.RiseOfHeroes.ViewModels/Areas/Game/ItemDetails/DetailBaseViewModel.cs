﻿using System;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.ItemDetails
{
    public class DetailBaseViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
    }
}
