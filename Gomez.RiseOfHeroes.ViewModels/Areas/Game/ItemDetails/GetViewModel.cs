﻿using Gomez.Core.ViewModels;
using Gomez.Core.ViewModels.Shared;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.ItemDetails
{
    public class QueryGetViewModel : QueryBaseModel<QueryGetViewModel, GetViewModel>
    {
        [FromRoute]
        public Guid Id { get; set; }
        public string Property { get; set; }
    }

    public class GetViewModel : QueryGetViewModel, IDynamicProperties
    {
        public GetViewModel() : base()
        {
            DynamicProperties = new Dictionary<string, object>();
        }

        public GetViewModel(QueryGetViewModel query) : this()
        {
            this.Update(query);
        }

        public Items.ItemBaseViewModel Item { get; set; }
        public DetailBaseViewModel Detail { get; set; }
        public SimplePropertyDtoCollection PropertyCollection { get; set; }
        public Dictionary<string, object> DynamicProperties { get; set; }

        public T GetPropertyByType<T>()
        {
            var nameOfType = typeof(T).Name;
            if (DynamicProperties.ContainsKey(nameOfType))
            {
                return (T)DynamicProperties[nameOfType];
            }

            return default;
        }
    }
}
