﻿
using Gomez.RiseOfHeroes.Models.Areas.Game.Enums;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.Items
{
    public class ItemBaseViewModel
    {
        public ItemBaseViewModel()
        {

        }

        public long Id { get; set; }
        public int Quantity { get; set; }
        public ItemState State { get; set; }
    }
}
