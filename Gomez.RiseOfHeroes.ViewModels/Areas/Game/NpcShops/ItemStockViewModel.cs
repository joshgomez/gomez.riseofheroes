﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcShops
{
    public class ItemStockViewModel
    {
        public long Id { get; set; }
        public int Quantity { get; set; }
        public int CharacterId { get; set; }
        public decimal Cost { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
