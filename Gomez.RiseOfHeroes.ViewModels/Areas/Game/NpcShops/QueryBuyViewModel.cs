﻿using HybridModelBinding;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcShops
{
    public class QueryBuyViewModel
    {
        [HybridBindProperty(new[] { Source.Body, Source.Form, Source.QueryString, Source.Route })]
        public long Id { get; set; }

        [HybridBindProperty(new[] { Source.Body, Source.Form })]
        public int Qty { get; set; }
    }
}
