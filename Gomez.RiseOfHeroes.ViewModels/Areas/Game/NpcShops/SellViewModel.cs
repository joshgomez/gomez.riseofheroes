﻿using AutoMapper;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcShops
{
    public class SellViewModel
    {
        public SellViewModel()
        {

        }

        public SellViewModel(SellInternalViewModel model)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<SellInternalViewModel, SellViewModel>());
            var mapper = config.CreateMapper();
            mapper.Map(model,this);
        }

        public int BuyersId { get; set; }
        public decimal BuyersMoney { get; set; }

        public string ItemName { get; set; }
        public decimal Cost { get; set; }
        public int Quantity { get; set; }

        public int SellersId { get; set; }
        public decimal SellersMoney { get; set; }
        public int SellersQuantity { get; set; }
    }

    public class SellInternalViewModel : SellViewModel
    {
        public byte[] BuyersMoneyVersion { get; set; }
        public byte[] SellersMoneyVersion { get; set; }
    }
}
