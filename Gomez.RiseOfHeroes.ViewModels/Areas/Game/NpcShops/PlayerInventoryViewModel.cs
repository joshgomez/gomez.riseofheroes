﻿using Gomez.Core.Models.Entity;
using Gomez.Core.ViewModels.Shared;
using HybridModelBinding;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcShops
{
    public class QueryPlayerInventoryViewModel : 
        PagedQueryBaseModel<QueryPlayerInventoryViewModel, PlayerInventoryViewModel>
    {
        /// <summary>
        /// Npc id
        /// </summary>
        [HybridBindProperty(new[] { Source.QueryString, Source.Route })]
        public int Id { get; set; }
    }

    public class PlayerInventoryViewModel : QueryPlayerInventoryViewModel, Characters.IInventoryViewModel<ItemIndexViewModel>
    {
        public PlayerInventoryViewModel(QueryPlayerInventoryViewModel query) : base()
        {
            this.Update(query);
        }

        public int PlayerId { get; set; }

        public PagedResult<ItemIndexViewModel> Items { get; set; }
    }
}
