﻿using Gomez.RiseOfHeroes.Calculations;
using Gomez.RiseOfHeroes.Models.Areas.Game.Enums;
using System;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcShops
{
    public class ItemIndexViewModel : Characters.ItemIndexViewModel
    {
        public ItemState State { get; set; }
        public int Quantity { get; set; }
        public decimal Cost { get; set; }
        public bool HasImage { get; set; }
        public Guid DetailId { get; set; }
        public decimal ActualCost => ItemCalculation.CostCalculation(Cost, State);
    }
}
