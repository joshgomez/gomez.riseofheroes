﻿using Gomez.Core.Models.Entity;
using Gomez.Core.ViewModels.Shared;
using HybridModelBinding;

namespace Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcShops
{
    public class QueryInventoryViewModel : 
        PagedQueryBaseModel<QueryInventoryViewModel, InventoryViewModel>, Characters.IQueryInventoryViewModel
    {
        [HybridBindProperty(new[] { Source.QueryString, Source.Route })]
        public int Id { get; set; }
    }

    public class InventoryViewModel : QueryInventoryViewModel, Characters.IInventoryViewModel<ItemIndexViewModel>
    {
        public InventoryViewModel(QueryInventoryViewModel query) : base()
        {
            this.Update(query);
        }

        public PagedResult<ItemIndexViewModel> Items { get; set; }
    }
}
