﻿using Gomez.RiseOfHeroes.Models.Areas.Game;
using System.Collections.Generic;

namespace Gomez.RiseOfHeroes.Models.Areas.Identity
{
    /// <summary>
    /// Add properties releated to the current project here
    /// </summary>
    public class ApplicationUser : Gomez.Core.Ef.Models.Areas.Identity.ApplicationUser
    {
        public ICollection<Character> Characters { get; set; }
    }
}
