﻿using Gomez.Core.Models.Entity;
using Gomez.RiseOfHeroes.Models.Areas.Game.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gomez.RiseOfHeroes.Models.Areas.Game
{
    public class Item : EntityBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public Guid DetailId { get; set; }
        public ItemDetail Detail { get; set; }

        public ItemState State { get; set; }
        public int Quantity { get; set; }

        public int CharacterId { get; set; }
        public Character Character { get; set; }
    }
}
