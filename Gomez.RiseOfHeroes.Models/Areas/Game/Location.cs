﻿using Gomez.Core.Ef.Models.Converters;
using Gomez.Core.Models.Entity;
using Gomez.RiseOfHeroes.Models.Areas.Game.Subscriptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NetTopologySuite.Geometries;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Gomez.RiseOfHeroes.Models.Areas.Game
{
    public class LocationConfiguration : IEntityTypeConfiguration<Location>
    {
        public void Configure(EntityTypeBuilder<Location> builder)
        {
            builder.Property(x => x.Coordinate)
                .HasConversion(PointToBinaryConverter.Instance);
        }
    }

    public class Location : EntityBase
    {
        public Guid Id { get; set; }

        [MaxLength(256)]
        public string Name { get; set; }

        public string Description { get; set; }

        /// <summary>
        /// Amount of images in sorting order
        /// </summary>
        public int HasImages { get; set; }

        /// <summary>
        /// Amount of backgrounds in sorting order
        /// </summary>
        public int HasBackgrounds { get; set; }

        public Point Coordinate { get; set; }

        public ICollection<SubscriptionCharacterLocation> CharacterSubscriptions { get; set; }
    }
}
