﻿using Gomez.Core.Models.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gomez.RiseOfHeroes.Models.Areas.Game
{
    public class CharacterPropertyConfiguration : IEntityTypeConfiguration<CharacterProperty>
    {
        public void Configure(EntityTypeBuilder<CharacterProperty> builder)
        {
            builder.HasIndex(x => new { x.CharacterId, x.Key })
                .IsUnique();
        }
    }

    public class CharacterProperty : PropertyBaseModel
    {
        public CharacterProperty()
        {

        }

        public CharacterProperty(string key, object value) : base(key, value)
        {
        }

        public CharacterProperty(Type type, string key, object value) : base(type, key, value)
        {

        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public int CharacterId { get; set; }
        public Character Character { get; set; }
    }
}
