﻿using Gomez.Core.Ef.Models.Converters;
using Gomez.Core.Models.Entity;
using Gomez.RiseOfHeroes.Models.Areas.Game.Subscriptions;
using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NetTopologySuite.Geometries;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gomez.RiseOfHeroes.Models.Areas.Game
{
    public class CharacterConfiguration : IEntityTypeConfiguration<Character>
    {
        public void Configure(EntityTypeBuilder<Character> builder)
        {
            builder.Property(x => x.Location)
                .HasConversion(PointToBinaryConverter.Instance);

            builder.HasIndex(x => x.Location);
        }
    }

    public class Character : PropCollectionEntityBase<CharacterProperty>, IPropertyCollection
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(256)]
        public string Name { get; set; }

        public string Description { get; set; }

        public int Type { get; set; }

        public Guid? UserId { get; set; }
        public ApplicationUser User { get; set; }

        public Point Location { get; set; }

        public ICollection<Item> Items { get; set; }
        public ICollection<SubscriptionCharacterLocation> LocationSubscriptions { get; set; }

        public override bool SetProperty(Type type, string key, object value)
        {
            if (!base.SetProperty(type, key, value))
            {
                var prop = new CharacterProperty(type, key, value)
                {
                    CharacterId = this.Id
                };
                Properties.Add(prop);
            }

            return true;
        }
    }
}
