﻿using Gomez.Core.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Gomez.RiseOfHeroes.Models.Areas.Game
{
    public class ItemDetail : PropCollectionEntityBase<ItemDetailProperty>, IPropertyCollection
    {
        [Key]
        public Guid Id { get; set; }

        [MaxLength(256)]
        public string Name { get; set; }

        public string Description { get; set; }

        public int Type { get; set; }

        public ICollection<Item> Items { get; set; }

        public override bool SetProperty(Type type, string key, object value)
        {
            if (!base.SetProperty(type, key, value))
            {
                var prop = new ItemDetailProperty(type, key, value)
                {
                    DetailId = this.Id
                };
                Properties.Add(prop);
            }

            return true;
        }
    }
}
