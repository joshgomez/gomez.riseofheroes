﻿using Gomez.Core.Models.Entity;
using Gomez.RiseOfHeroes.Models.Areas.Game.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gomez.RiseOfHeroes.Models.Areas.Game.Subscriptions
{
    public class SubscriptionCharacterLocationConfiguration : IEntityTypeConfiguration<SubscriptionCharacterLocation>
    {
        public void Configure(EntityTypeBuilder<SubscriptionCharacterLocation> builder)
        {
            builder.HasKey(c => new { c.CharacterId, c.LocationId });

            builder.HasOne(c => c.Character)
               .WithMany(c => c.LocationSubscriptions)
               .HasForeignKey(pc => pc.CharacterId);

            builder.HasOne(c => c.Location)
               .WithMany(p => p.CharacterSubscriptions)
               .HasForeignKey(pc => pc.LocationId);
        }
    }

    [Table("Subscriptions_CharacterLocation")]
    public class SubscriptionCharacterLocation : EntityBase
    {
        public int CharacterId { get; set; }
        public Character Character { get; set; }

        public Guid LocationId { get; set; }
        public Location Location { get; set; }
    }
}