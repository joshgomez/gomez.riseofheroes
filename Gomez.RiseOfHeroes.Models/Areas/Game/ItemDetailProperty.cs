﻿using Gomez.Core.Models.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gomez.RiseOfHeroes.Models.Areas.Game
{
    public class ItemDetailPropertyConfiguration : IEntityTypeConfiguration<ItemDetailProperty>
    {
        public void Configure(EntityTypeBuilder<ItemDetailProperty> builder)
        {
            builder.HasIndex(x => new { x.DetailId, x.Key })
            .IsUnique();
        }
    }

    public class ItemDetailProperty : PropertyBaseModel
    {
        public ItemDetailProperty()
        {

        }

        public ItemDetailProperty(string key, object value) : base(key, value)
        {
        }

        public ItemDetailProperty(Type type, string key, object value) : base(type, key, value)
        {

        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public Guid DetailId { get; set; }
        public ItemDetail Detail { get; set; }
    }
}
