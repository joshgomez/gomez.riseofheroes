﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gomez.RiseOfHeroes.Models.Areas.Game.Enums
{
    public enum AttackType
    {
        Miss,
        Normal,
        Critical,
    }
}
