﻿namespace Gomez.RiseOfHeroes.Models.Areas.Game.Enums
{
    public enum BattleResultType
    {
        Draw,
        PlayersWon,
        OpponentsWon
    }
}
