﻿namespace Gomez.RiseOfHeroes.Models.Areas.Game.Enums
{
    public enum TacticType
    {
        Normal,
        Offensive,
        Defensive
    }
}
