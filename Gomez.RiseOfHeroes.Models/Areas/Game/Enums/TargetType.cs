﻿namespace Gomez.RiseOfHeroes.Models.Areas.Game.Enums
{
    public enum TargetType
    {
        Any,
        LowestHealth,
        HighestHealth,
        Weakest,
        Strongest
    }
}
