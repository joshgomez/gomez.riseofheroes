﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gomez.RiseOfHeroes.Models.Areas.Game.Enums
{
    public enum ItemState
    {
        Undefined,
        Unequiped,
        Equiped,
        /// <summary>
        /// Can be dropped by NPC
        /// </summary>
        Loot,
        /// <summary>
        /// The item will not get deleted
        /// </summary>
        ShopSale,
        /// <summary>
        /// The item will moved to the buyer
        /// </summary>
        ShopSecondHand
    }
}
