﻿using Gomez.Core.Configurations.Constants.Areas.Identity;
using Gomez.Core.Ef.Database;
using Gomez.Core.Utilities;
using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using Gomez.RiseOfHeroes.Models.Areas.Game;
using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.Models.Contexts
{
    public class ApplicationDbInitializer : Core.Ef.Models.Contexts.ApplicationDbInitializer<ApplicationUser>
    {
        private readonly ApplicationDbContext _db;

        public ApplicationDbInitializer(ApplicationDbContext db, UserManager<ApplicationUser> userManager) : base(userManager)
        {
            _db = db;
            AddUser("admin@riseofheroes.online", "Password!1", RoleConstant.SUPER_ADMINISTRATOR);
        }

 
        public void AddMissingCharacterProperties()
        {
            AsyncPump.Run(async () =>
            {
                await AddCharacterPropertyAsync(
                    CharacterConstant.TYPE_PLAYER, 
                    CharacterConstant.KEY_STATS_ENERGY, 1d);
            });
        }

        protected async Task AddCharacterPropertyAsync(int type, string propKey, object defaultValue)
        {
            var query = _db.Characters
            .Where(x => x.Type == type)
            .Select(x => new
            {
                x.Id,
                HasEnergyProp = x.Properties
                .Any(z => z.Key == propKey)
            })
            .Where(x => !x.HasEnergyProp);

            var propsToAdd = new List<CharacterProperty>();
            var batchHelper = await BatchHelper.CreateAsync(query, 1, 100);
            for (int i = 1; i <= batchHelper.TotalPages; i++)
            {
                var subQuery = batchHelper.GetBatch(i);
                foreach (var item in await subQuery.ToArrayAsync())
                {
                    propsToAdd.Add(new CharacterProperty(propKey, defaultValue)
                    {
                        CharacterId = item.Id
                    });
                }
            }

            _db.CharacterProperties.AddRange(propsToAdd);
            await _db.SaveChangesAsync();
        }
    }
}
