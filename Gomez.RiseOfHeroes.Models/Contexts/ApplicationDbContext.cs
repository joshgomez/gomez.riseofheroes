﻿using Gomez.RiseOfHeroes.Models.Areas.Game;
using Gomez.RiseOfHeroes.Models.Areas.Game.Subscriptions;
using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Microsoft.EntityFrameworkCore;
using System;

namespace Gomez.RiseOfHeroes.Models.Contexts
{
    public class ApplicationDbContext : Gomez.Core.Ef.Database.ApplicationDbContext<ApplicationUser,ApplicationRole,Guid>
    {
        

        public DbSet<Item> Items { get; set; }
        public DbSet<ItemDetail> ItemDetails { get; set; }
        public DbSet<ItemDetailProperty> ItemDetailProperties { get; set; }

        public DbSet<Character> Characters { get; set; }
        public DbSet<CharacterProperty> CharacterProperties { get; set; }


        public DbSet<Location> Locations { get; set; }
        public DbSet<SubscriptionCharacterLocation> CharacterLocationSubscriptions { get; set; }


        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new CharacterConfiguration());
            builder.ApplyConfiguration(new LocationConfiguration());
            builder.ApplyConfiguration(new CharacterPropertyConfiguration());
            builder.ApplyConfiguration(new ItemDetailPropertyConfiguration());
            builder.ApplyConfiguration(new SubscriptionCharacterLocationConfiguration());


            base.OnModelCreating(builder);

            var sContext = new SeedContext(builder);
            sContext.Seed();
        }
    }
}
