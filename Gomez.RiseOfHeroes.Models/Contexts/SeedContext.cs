﻿using Gomez.Core.Configurations.Constants.Areas.Identity;
using Gomez.Core.Ef.Database;
using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using Gomez.RiseOfHeroes.Models.Areas.Game;
using Gomez.RiseOfHeroes.Models.Areas.Game.Enums;
using Gomez.RiseOfHeroes.Models.Areas.Game.Subscriptions;
using Gomez.RiseOfHeroes.Models.Areas.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Gomez.RiseOfHeroes.Models.Contexts
{
    using ITM = Configurations.Constants.Areas.Game.Seed.ItemSeedConstant;
    using LOC = Configurations.Constants.Areas.Game.Seed.LocationSeedConstant;

    public class SeedContext : ISeedContext
    {
        protected readonly ModelBuilder _builder;
        public SeedContext(ModelBuilder builder)
        {
            _builder = builder;
        }

        readonly List<Location> _locations = new List<Location>();
        readonly List<SubscriptionCharacterLocation> _characterLocationSubscriptions = new List<SubscriptionCharacterLocation>();

        long _currentItemId = 1;
        int _currentItemDetailId = 1;
        readonly List<Item> _items = new List<Item>();
        readonly List<ItemDetail> _itemDetails = new List<ItemDetail>();
        readonly List<ItemDetailProperty> _itemDetailProps = new List<ItemDetailProperty>();

        protected ItemDetail AddItem(string id, string name, int type, string description, params ItemDetailProperty[] properties)
        {
            var itemDetail = new ItemDetail()
            {
                Id = new Guid(id),
                Name = name,
                Type = type,
                Description = description,
                CreatedAt = DateTime.MinValue,
                RowVersion = null
            };

            _itemDetails.Add(itemDetail);

            foreach (var property in properties)
            {
                property.Id = _currentItemDetailId++;
                property.CreatedAt = DateTime.MinValue;
                property.DetailId = itemDetail.Id;
                property.RowVersion = null;
                _itemDetailProps.Add(property);
            }

            return itemDetail;
        }

        /// <summary>
        /// Use if new property is required to add after the item have been created.
        /// </summary>
        /// <param name="detail"></param>
        /// <param name="property"></param>
        protected void AddNewPropertyToItem(ItemDetail detail, ItemDetailProperty property)
        {
            property.Id = _currentItemDetailId++;
            property.CreatedAt = DateTime.MinValue;
            property.DetailId = detail.Id;
            property.RowVersion = null;
            _itemDetailProps.Add(property);
        }

        int _currentPlayerId = 1;
        int _currentPlayerPropId = 1;
        readonly List<Character> _characters = new List<Character>();
        readonly List<CharacterProperty> _characterProperties = new List<CharacterProperty>();

        private Character AddCharacter(string name, int type, params CharacterProperty[] properties)
        {
            var c = new Character()
            {
                Id = _currentPlayerId++,
                Name = name,
                Type = type,
                CreatedAt = DateTime.MinValue,
                RowVersion = null
            };

            _characters.Add(c);

            foreach (var item in properties)
            {
                item.Id = _currentPlayerPropId++;
                item.CreatedAt = DateTime.MinValue;
                item.CharacterId = _characters[_characters.Count - 1].Id;
                item.RowVersion = null;
                _characterProperties.Add(item);
            }

            return c;
        }

        private Character AddNpcEnemyCharacter(string name,int level, int str,int cst, int dex, int @int, decimal money, params CharacterProperty[] properties)
        {
            properties = properties.Union(new CharacterProperty[]
            {
                new CharacterProperty(CharacterConstant.KEY_STATS_LEVEL, level),
                new CharacterProperty(CharacterConstant.KEY_ATTR_STRENGTH, str),
                new CharacterProperty(CharacterConstant.KEY_ATTR_CONSTITUTION, cst),
                new CharacterProperty(CharacterConstant.KEY_ATTR_DEXTERITY, dex),
                new CharacterProperty(CharacterConstant.KEY_ATTR_INTELLIGENCE, @int),
                new CharacterProperty(CharacterConstant.KEY_MONEY, money)
            }).ToArray();

            return AddCharacter(name, CharacterConstant.TYPE_NPC_ENEMY, properties );
        }

        private Character AddNpcShopCharacter(string name, int level, decimal money, int[] itemTypes, params CharacterProperty[] properties)
        {
            properties = properties.Union(new CharacterProperty[]
            {
                new CharacterProperty(CharacterConstant.KEY_STATS_LEVEL, level),
                new CharacterProperty(CharacterConstant.KEY_MONEY, money),
                new CharacterProperty(NpcShopConstant.KEY_CAN_BUY_TYPES,String.Join(',',itemTypes))
            }).ToArray();

            return AddCharacter(name, CharacterConstant.TYPE_NPC_SHOP, properties);
        }

        private Character AddNpcCustomCharacter(string name, int level, decimal money, params CharacterProperty[] properties)
        {
            properties = properties.Union(new CharacterProperty[]
            {
                new CharacterProperty(CharacterConstant.KEY_STATS_LEVEL, level),
                new CharacterProperty(CharacterConstant.KEY_MONEY, money),
            }).ToArray();

            return AddCharacter(name, CharacterConstant.TYPE_NPC_CUSTOM, properties);
        }

        private void AddItemToCharacter(Character c, Guid detailId, ItemState state)
        {
            var item = new Item()
            {
                DetailId = detailId, Id = _currentItemId++, CreatedAt = DateTime.MinValue, 
                CharacterId = c.Id ,
                Quantity = 1,
                State = state,
                RowVersion = null
            };

            _items.Add(item);
        }

        private void AddLocationToCharacter(Character c, Guid locationId)
        {
            var subscription = new SubscriptionCharacterLocation()
            {
                CharacterId = c.Id,
                LocationId = locationId,
                CreatedAt = DateTime.MinValue,
                RowVersion = null
            };

            _characterLocationSubscriptions.Add(subscription);
        }

        private void SeedLocations()
        {
            //Must be placed in created sorting order.

            _locations.Add(new Location 
            { 
                Id = new Guid(LOC.ID_TOWN_OF_HAIWOOD),
                Name = "Town of Haiwood",
                Description = @"A small town that is rumored to be the starting point for heroes teleported from other worlds. 
Haiwood is surrounded by forest and is far away from major cities",
                Coordinate = new NetTopologySuite.Geometries.Point(0,0),
                CreatedAt = DateTime.MinValue,
                RowVersion = null
            });

            _locations.Add(new Location
            {
                Id = new Guid(LOC.ID_HAIWOOD_FOREST),
                Name = "Haiwood Forest",
                Description = "The forest which is surrounding the Town of Haiwood, common wolfs is told to exist in this forest.",
                Coordinate = new NetTopologySuite.Geometries.Point(4, 5),
                CreatedAt = DateTime.MinValue,
                RowVersion = null
            });

            _locations.Add(new Location
            {
                Id = new Guid(LOC.ID_CITY_OF_SEFU),
                Name = "City of Sefu",
                Description = @"The city is a hub in the desert where people from all over the world meets for tradings, gambling, entertainments and other shady stuffs.
Sefu is built upon a acient city where lot of unexplored places exists with rich history.",
                Coordinate = new NetTopologySuite.Geometries.Point(73, 89),
                CreatedAt = DateTime.MinValue, 
                RowVersion = null,
                HasBackgrounds = 1
            });

            _locations.Add(new Location
            {
                Id = new Guid(LOC.ID_JUNGLE_OF_IVY),
                Name = "Jungle of Ivy",
                Description = @"The humid and lush rain forest is famous for its dangerous snakes.
Only the brave heroes enter this jungle. There is a tale that the Amazon tribe put a curse 
to keep people from entering and stealing the riches of the forest.",
                Coordinate = new NetTopologySuite.Geometries.Point(23, 143),
                CreatedAt = DateTime.MinValue,
                RowVersion = null,
            });

            _builder.Entity<Location>().HasData(_locations);
        }

        private void SeedItems()
        {
            //Must be placed in created sorting order.

            AddItem(ITM.ID_MINOR_HEALTH_POITION, "Minor health potion", ItemConstant.TYPE_POTION, "A potion of lowest quality that gives a percentage of your max health.",
                new ItemDetailProperty(ItemConstant.KEY_STATS_LEVEL, 1),
                new ItemDetailProperty(ItemConstant.KEY_STATS_HEALTH, 30d),
                new ItemDetailProperty(ItemConstant.KEY_STATS_COST, 8m),
                new ItemDetailProperty(ItemConstant.KEY_STATS_WEIGHT, 0.01d),
                new ItemDetailProperty(ItemConstant.KEY_HAS_IMAGE, true));

            AddItem(ITM.ID_LEATHER_HELMET, "Leather Helmet", ItemConstant.TYPE_HEAD_GEAR, null,
                new ItemDetailProperty(ItemConstant.KEY_STATS_LEVEL, 1),
                new ItemDetailProperty(ItemConstant.KEY_STATS_ARMOR, 1),
                new ItemDetailProperty(ItemConstant.KEY_STATS_COST, 30m),
                new ItemDetailProperty(ItemConstant.KEY_STATS_WEIGHT, 0.15d),
                new ItemDetailProperty(ItemConstant.KEY_HAS_IMAGE, false));

            AddItem(ITM.ID_WOLF_PELT, "Wolf Pelt", ItemConstant.TYPE_MISC,"Dropped by wolf, can be used for crafting.",
                new ItemDetailProperty(ItemConstant.KEY_STATS_LEVEL, 1),
                new ItemDetailProperty(ItemConstant.KEY_STATS_COST, 4m),
                new ItemDetailProperty(ItemConstant.KEY_STATS_WEIGHT, 0.04d),
                new ItemDetailProperty(ItemConstant.KEY_HAS_IMAGE, false));

            AddItem(ITM.ID_LEATHER_ARMOR, "Leather Body Armor", ItemConstant.TYPE_BODY_GEAR, null,
                new ItemDetailProperty(ItemConstant.KEY_STATS_LEVEL, 1),
                new ItemDetailProperty(ItemConstant.KEY_STATS_ARMOR, 2),
                new ItemDetailProperty(ItemConstant.KEY_STATS_COST, 100m),
                new ItemDetailProperty(ItemConstant.KEY_STATS_WEIGHT, 1.65d),
                new ItemDetailProperty(ItemConstant.KEY_HAS_IMAGE, false));

            AddItem(ITM.ID_1HSWORD_WOODEN, "Short Wooden Sword", ItemConstant.TYPE_WEAPON, "Mostly used for training practices.",
                new ItemDetailProperty(ItemConstant.KEY_STATS_LEVEL, 1),
                new ItemDetailProperty(ItemConstant.KEY_STATS_DAMAGE, 0.5d),
                new ItemDetailProperty(ItemConstant.KEY_STATS_COST, 40m),
                new ItemDetailProperty(ItemConstant.KEY_STATS_WEIGHT, 0.2d),
                new ItemDetailProperty(ItemConstant.KEY_HAS_IMAGE, false));

            AddItem(ITM.ID_1HSWORD_BRONZE, "Short Bronze Sword", ItemConstant.TYPE_WEAPON, null,
                new ItemDetailProperty(ItemConstant.KEY_STATS_LEVEL, 5),
                new ItemDetailProperty(ItemConstant.KEY_STATS_DAMAGE, 1d),
                new ItemDetailProperty(ItemConstant.KEY_STATS_COST, 300m),
                new ItemDetailProperty(ItemConstant.KEY_STATS_WEIGHT, 0.5d),
                new ItemDetailProperty(ItemConstant.KEY_HAS_IMAGE, false));

            AddItem(ITM.ID_1HSWORD_IRON, "Short Iron Sword", ItemConstant.TYPE_WEAPON, null,
                new ItemDetailProperty(ItemConstant.KEY_STATS_LEVEL, 10),
                new ItemDetailProperty(ItemConstant.KEY_STATS_DAMAGE, 2d),
                new ItemDetailProperty(ItemConstant.KEY_STATS_COST, 700m),
                new ItemDetailProperty(ItemConstant.KEY_STATS_WEIGHT, 0.9d),
                new ItemDetailProperty(ItemConstant.KEY_HAS_IMAGE, false));

            AddItem(ITM.ID_SNAKE_SKIN, "Snake Skin", ItemConstant.TYPE_MISC, "Dropped by snakes, can be used for crafting.",
                new ItemDetailProperty(ItemConstant.KEY_STATS_LEVEL, 13),
                new ItemDetailProperty(ItemConstant.KEY_STATS_COST, 8m),
                new ItemDetailProperty(ItemConstant.KEY_STATS_WEIGHT, 0.02d),
                new ItemDetailProperty(ItemConstant.KEY_HAS_IMAGE, false));

            _builder.Entity<ItemDetail>().HasData(_itemDetails);
            _builder.Entity<ItemDetailProperty>().HasData(_itemDetailProps);
        }

        //1
        private void AddNpcPickpocketer()
        {
            var npc = AddNpcEnemyCharacter("Pickpocketer", 1, 5, 5, 10, 5, 2m,
                new CharacterProperty(CharacterConstant.KEY_HAS_IMAGE,false));
            npc.Description = "A common thief that mostly exist in town.";
            AddItemToCharacter(npc, new Guid(ITM.ID_LEATHER_HELMET), ItemState.Equiped);
            AddLocationToCharacter(npc, new Guid(LOC.ID_TOWN_OF_HAIWOOD));
        }

        //2
        private void AddNpcJaenviasPotions()
        {
            var npc = AddNpcShopCharacter("Jaenvia's Potions", 1, 2000m,
                new int[] { ItemConstant.TYPE_POTION }, 
                new CharacterProperty(CharacterConstant.KEY_HAS_IMAGE, false));
            npc.Description = "Jaenvia is a old witch that has retierd and is now having a potion shop.";

            AddItemToCharacter(npc, new Guid(ITM.ID_MINOR_HEALTH_POITION), ItemState.ShopSale);
            AddLocationToCharacter(npc, new Guid(LOC.ID_TOWN_OF_HAIWOOD));
        }

        //3
        private void AddNpcWolf()
        {
            var npc = AddNpcEnemyCharacter("Wolf", 2, 5, 10, 9, 6, 0m,
                new CharacterProperty(CharacterConstant.KEY_HAS_IMAGE, false));
            npc.Description = "The most common wolf which exists in many forests of this world.";
            AddItemToCharacter(npc, new Guid(ITM.ID_WOLF_PELT), ItemState.Loot);
            AddLocationToCharacter(npc, new Guid(LOC.ID_HAIWOOD_FOREST));
        }

        //4
        private void AddNpcHammerAndHand()
        {
            var npc = AddNpcShopCharacter("Hammer and Hand", 1, 3000m,
                new int[] { ItemConstant.TYPE_BODY_GEAR, ItemConstant.TYPE_HEAD_GEAR, ItemConstant.TYPE_WEAPON },
                new CharacterProperty(CharacterConstant.KEY_HAS_IMAGE, false));
            npc.Description = @"Is the only blacksmith in Haiwood which sells equipments for heroes. 
Hammer and Hand provides with basic armors and weapons as it is hard to find rare materials around here.";

            AddItemToCharacter(npc, new Guid(ITM.ID_LEATHER_HELMET), ItemState.ShopSale);
            AddItemToCharacter(npc, new Guid(ITM.ID_LEATHER_ARMOR), ItemState.ShopSale);
            AddItemToCharacter(npc, new Guid(ITM.ID_1HSWORD_WOODEN), ItemState.ShopSale);
            AddItemToCharacter(npc, new Guid(ITM.ID_1HSWORD_BRONZE), ItemState.ShopSale);
            AddLocationToCharacter(npc, new Guid(LOC.ID_TOWN_OF_HAIWOOD));
        }

        //5
        private void AddNpcCasino()
        {
            var npc = AddNpcCustomCharacter("The Amethyst", 87,70000000, 
                new CharacterProperty(NpcCustomConstant.KEY_GAME_BLACKJACK,true),
                new CharacterProperty(NpcCustomConstant.KEY_GAME_ROULETTE, true),
                new CharacterProperty(NpcCustomConstant.KEY_GAME_SLOTS, true),
                new CharacterProperty(CharacterConstant.KEY_HAS_IMAGE, false));
            npc.Description = @"A luxury casino in middle of Sefu. The owner is unknown but is rumored to be one of the first hero from another world.";
            AddLocationToCharacter(npc, new Guid(LOC.ID_CITY_OF_SEFU));
        }

        //6
        private void AddNpcHaiwoodTownSquare()
        {
            var npc = AddNpcCustomCharacter("Town Square", 4, 100000,
                new CharacterProperty(NpcCustomConstant.KEY_WORK_BUSKER, WorkConstant.CON),
                new CharacterProperty(NpcCustomConstant.KEY_WORK_MUDLARK, WorkConstant.DEX),
                new CharacterProperty(NpcCustomConstant.KEY_WORK_QUARRYMAN, WorkConstant.STR),
                new CharacterProperty(NpcCustomConstant.KEY_CHAT, true),
                new CharacterProperty(CharacterConstant.KEY_HAS_IMAGE, false));
            npc.Description = @"The town square of Haiwood. Heroes come to 
the town square to find odd jobs to earn some money. The market is held here.";
            AddLocationToCharacter(npc, new Guid(LOC.ID_TOWN_OF_HAIWOOD));
        }

        //7
        private void AddNpcAnacondaSnake()
        {
            var npc = AddNpcEnemyCharacter("Anaconda", 13, 20, 15, 30, 20, 0m,
                new CharacterProperty(CharacterConstant.KEY_HAS_IMAGE, false));
            npc.Description = "A dangerous snake that lives in the jungle.";
            AddItemToCharacter(npc, new Guid(ITM.ID_SNAKE_SKIN), ItemState.Loot);
            AddLocationToCharacter(npc, new Guid(LOC.ID_JUNGLE_OF_IVY));
        }

        private void AddNpcCassandraMonsterSupplies()
        {
            var npc = AddNpcShopCharacter("Cassandra's Monster Supplies", 1, 2000m,
                new int[] { ItemConstant.TYPE_MISC },
                new CharacterProperty(CharacterConstant.KEY_HAS_IMAGE, false));
            npc.Description = @"Cassandra is a retired monster hunter now running her own shop of monster supplies. 
She is buying all kind of loot dropped from monsters.";
            AddLocationToCharacter(npc, new Guid(LOC.ID_TOWN_OF_HAIWOOD));
        }

        private void SeedCharacters()
        {
            //Must be placed in sorting order.
            AddNpcPickpocketer();
            AddNpcJaenviasPotions();
            AddNpcWolf();
            AddNpcHammerAndHand();
            AddNpcCasino();
            AddNpcHaiwoodTownSquare();
            AddNpcAnacondaSnake();
            AddNpcCassandraMonsterSupplies();

            _builder.Entity<Character>().HasData(_characters);
            _builder.Entity<CharacterProperty>().HasData(_characterProperties);
            _builder.Entity<Item>().HasData(_items);
            _builder.Entity<SubscriptionCharacterLocation>().HasData(_characterLocationSubscriptions);
        }

        public virtual void Seed()
        {
            _builder.Entity<ApplicationRole>().HasData(new ApplicationRole
            {
                Id = new Guid("434FAD8B-35F9-4B41-8FA9-4248A0B06766"),
                Name = RoleConstant.SUPER_ADMINISTRATOR,
                NormalizedName = RoleConstant.SUPER_ADMINISTRATOR.ToUpper(),
                CreatedAt = new DateTime(2019, 11, 17, 14, 21, 45, 225, DateTimeKind.Utc).AddTicks(742),
                ConcurrencyStamp = "e333961c-3ec0-4570-8571-4af6e6ca09f8",
            });

            SeedLocations();
            SeedItems();
            SeedCharacters();
        }
    }
}
