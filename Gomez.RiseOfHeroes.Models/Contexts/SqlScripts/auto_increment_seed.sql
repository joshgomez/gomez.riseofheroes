﻿
--ALTER TABLE `characters` auto_increment = 100000;
--ALTER TABLE `characterproperties` auto_increment = 10000000;
--ALTER TABLE `itemdetailproperties` auto_increment = 10000000;
--ALTER TABLE `items` auto_increment = 10000000;

DBCC checkident ('characters', reseed, 10000);
DBCC checkident ('characterproperties', reseed, 10000000);
DBCC checkident ('itemdetailproperties', reseed, 10000000);
DBCC checkident ('items', reseed, 10000000);