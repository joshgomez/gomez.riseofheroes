﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Gomez.RiseOfHeroes.Models.Migrations
{
    public partial class RecreatedInitial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    SortingOrder = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    LastLoginAt = table.Column<DateTime>(nullable: true),
                    SortingOrder = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ItemDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    SortingOrder = table.Column<int>(nullable: false),
                    RowVersion = table.Column<DateTime>(rowVersion: true, nullable: true)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemDetails", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Locations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    SortingOrder = table.Column<int>(nullable: false),
                    RowVersion = table.Column<DateTime>(rowVersion: true, nullable: true)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    Description = table.Column<string>(nullable: true),
                    HasImages = table.Column<int>(nullable: false),
                    HasBackgrounds = table.Column<int>(nullable: false),
                    Coordinate = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Locations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RefreshTokens",
                columns: table => new
                {
                    Token = table.Column<string>(maxLength: 255, nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    SortingOrder = table.Column<int>(nullable: false),
                    RowVersion = table.Column<DateTime>(rowVersion: true, nullable: true)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    Expires = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    RemoteIpAddress = table.Column<byte[]>(maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RefreshTokens", x => x.Token);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<Guid>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<Guid>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderKey = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    Name = table.Column<string>(maxLength: 128, nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    SortingOrder = table.Column<int>(nullable: false),
                    RowVersion = table.Column<DateTime>(rowVersion: true, nullable: true)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    UserId = table.Column<Guid>(nullable: true),
                    Location = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Characters_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ItemDetailProperties",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    SortingOrder = table.Column<int>(nullable: false),
                    RowVersion = table.Column<DateTime>(rowVersion: true, nullable: true)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    Key = table.Column<string>(nullable: false),
                    TypeNum = table.Column<int>(nullable: false),
                    ValueInt32 = table.Column<int>(nullable: true),
                    ValueInt64 = table.Column<long>(nullable: true),
                    ValueFloat = table.Column<float>(nullable: true),
                    ValueDouble = table.Column<double>(nullable: true),
                    ValueDecimal = table.Column<decimal>(nullable: true),
                    ValueString = table.Column<string>(nullable: true),
                    ValueBool = table.Column<bool>(nullable: true),
                    ValueDateTime = table.Column<DateTime>(nullable: true),
                    DetailId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemDetailProperties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ItemDetailProperties_ItemDetails_DetailId",
                        column: x => x.DetailId,
                        principalTable: "ItemDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CharacterProperties",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    SortingOrder = table.Column<int>(nullable: false),
                    RowVersion = table.Column<DateTime>(rowVersion: true, nullable: true)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    Key = table.Column<string>(nullable: false),
                    TypeNum = table.Column<int>(nullable: false),
                    ValueInt32 = table.Column<int>(nullable: true),
                    ValueInt64 = table.Column<long>(nullable: true),
                    ValueFloat = table.Column<float>(nullable: true),
                    ValueDouble = table.Column<double>(nullable: true),
                    ValueDecimal = table.Column<decimal>(nullable: true),
                    ValueString = table.Column<string>(nullable: true),
                    ValueBool = table.Column<bool>(nullable: true),
                    ValueDateTime = table.Column<DateTime>(nullable: true),
                    CharacterId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterProperties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CharacterProperties_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Items",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    SortingOrder = table.Column<int>(nullable: false),
                    RowVersion = table.Column<DateTime>(rowVersion: true, nullable: true)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    DetailId = table.Column<Guid>(nullable: false),
                    State = table.Column<int>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    CharacterId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Items_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Items_ItemDetails_DetailId",
                        column: x => x.DetailId,
                        principalTable: "ItemDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Subscriptions_CharacterLocation",
                columns: table => new
                {
                    CharacterId = table.Column<int>(nullable: false),
                    LocationId = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    SortingOrder = table.Column<int>(nullable: false),
                    RowVersion = table.Column<DateTime>(rowVersion: true, nullable: true)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscriptions_CharacterLocation", x => new { x.CharacterId, x.LocationId });
                    table.ForeignKey(
                        name: "FK_Subscriptions_CharacterLocation_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Subscriptions_CharacterLocation_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "CreatedAt", "DeletedAt", "Name", "NormalizedName", "SortingOrder", "UpdatedAt" },
                values: new object[] { new Guid("434fad8b-35f9-4b41-8fa9-4248a0b06766"), "e333961c-3ec0-4570-8571-4af6e6ca09f8", new DateTime(2019, 11, 17, 14, 21, 45, 225, DateTimeKind.Utc).AddTicks(742), null, "SuperAdministrator", "SUPERADMINISTRATOR", 0, null });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "Description", "Location", "Name", "SortingOrder", "Type", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 7, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "A dangerous snake that lives in the jungle.", null, "Anaconda", 0, 1, null, null },
                    { 6, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, @"The town square of Haiwood. Heroes come to 
                the town square to find odd jobs to earn some money. The market is held here.", null, "Town Square", 0, 3, null, null },
                    { 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "A luxury casino in middle of Sefu. The owner is unknown but is rumored to be one of the first hero from another world.", null, "The Amethyst", 0, 3, null, null },
                    { 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "A common thief that mostly exist in town.", null, "Pickpocketer", 0, 1, null, null },
                    { 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "The most common wolf which exists in many forests of this world.", null, "Wolf", 0, 1, null, null },
                    { 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Jaenvia is a old witch that has retierd and is now having a potion shop.", null, "Jaenvia's Potions", 0, 2, null, null },
                    { 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, @"Is the only blacksmith in Haiwood which sells equipments for heroes. 
                Hammer and Hand provides with basic armors and weapons as it is hard to find rare materials around here.", null, "Hammer and Hand", 0, 2, null, null }
                });

            migrationBuilder.InsertData(
                table: "ItemDetails",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "Description", "Name", "SortingOrder", "Type", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("7083ae6c-c82d-40f7-ba3e-db2b40ae7d27"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "A potion of lowest quality that gives a percentage of your max health.", "Minor health potion", 0, 4, null },
                    { new Guid("67f935e9-0bd7-43a2-8515-69add6ed362b"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "Leather Helmet", 0, 2, null },
                    { new Guid("f38d2aca-5645-4be2-8407-aa51ca88c093"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "Leather Body Armor", 0, 3, null },
                    { new Guid("774a3a7a-dad0-4ef7-b78c-4fecbf6d7303"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Mostly used for training practices.", "Short Wooden Sword", 0, 1, null },
                    { new Guid("1a2f565c-5ed6-4d76-a097-7bc942c0876f"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "Short Bronze Sword", 0, 1, null },
                    { new Guid("ce914758-fda0-453e-9eb9-704fce6a686c"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, "Short Iron Sword", 0, 1, null },
                    { new Guid("721d6553-e6f3-40a5-997e-35a44ed5d4ba"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Dropped by snakes, can be used for crafting.", "Snake Skin", 0, 0, null },
                    { new Guid("03369820-ae44-4c5b-bbf8-fad48df88d54"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Dropped by wolf, can be used for crafting.", "Wolf Pelt", 0, 0, null }
                });

            migrationBuilder.InsertData(
                table: "Locations",
                columns: new[] { "Id", "Coordinate", "CreatedAt", "DeletedAt", "Description", "HasBackgrounds", "HasImages", "Name", "SortingOrder", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("49266268-7039-4a06-bf53-5b1ee595359c"), new byte[] { 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55, 64, 0, 0, 0, 0, 0, 224, 97, 64 }, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, @"The humid and lush rain forest is famous for its dangerous snakes.
                Only the brave heroes enter this jungle. There is a tale that the Amazon tribe put a curse 
                to keep people from entering and stealing the riches of the forest.", 0, 0, "Jungle of Ivy", 0, null },
                    { new Guid("4b5a5893-0de9-42e1-b8a3-f0de3b75afb5"), new byte[] { 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, @"A small town that is rumored to be the starting point for heroes teleported from other worlds. 
                Haiwood is surrounded by forest and is far away from major cities", 0, 0, "Town of Haiwood", 0, null },
                    { new Guid("8ad83b8b-48ba-4ee2-a804-0e46b5588ccb"), new byte[] { 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16, 64, 0, 0, 0, 0, 0, 0, 20, 64 }, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "The forest which is surrounding the Town of Haiwood, common wolfs is told to exist in this forest.", 0, 0, "Haiwood Forest", 0, null },
                    { new Guid("2d23c4f7-1865-420c-a846-e2ad9a4f0047"), new byte[] { 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 64, 82, 64, 0, 0, 0, 0, 0, 64, 86, 64 }, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, @"The city is a hub in the desert where people from all over the world meets for tradings, gambling, entertainments and other shady stuffs.
                Sefu is built upon a acient city where lot of unexplored places exists with rich history.", 1, 0, "City of Sefu", 0, null }
                });

            migrationBuilder.InsertData(
                table: "CharacterProperties",
                columns: new[] { "Id", "CharacterId", "CreatedAt", "DeletedAt", "Key", "SortingOrder", "TypeNum", "UpdatedAt", "ValueBool", "ValueDateTime", "ValueDecimal", "ValueDouble", "ValueFloat", "ValueInt32", "ValueInt64", "ValueString" },
                values: new object[,]
                {
                    { 1L, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Has.Image", 0, 7, null, false, null, null, null, null, null, null, null },
                    { 24L, 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Game.Roulette", 0, 7, null, true, null, null, null, null, null, null, null },
                    { 25L, 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Game.Slots", 0, 7, null, true, null, null, null, null, null, null, null },
                    { 26L, 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Has.Image", 0, 7, null, false, null, null, null, null, null, null, null },
                    { 27L, 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Stats.Level", 0, 1, null, null, null, null, null, null, 87, null, null },
                    { 28L, 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Money", 0, 5, null, null, null, 70000000m, null, null, null, null, null },
                    { 29L, 6, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Work.Busker", 0, 1, null, null, null, null, null, null, 2, null, null },
                    { 30L, 6, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Work.Mudlark", 0, 1, null, null, null, null, null, null, 3, null, null },
                    { 31L, 6, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Work.Quarryman", 0, 1, null, null, null, null, null, null, 1, null, null },
                    { 23L, 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Game.BlackJack", 0, 7, null, true, null, null, null, null, null, null, null },
                    { 33L, 6, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Has.Image", 0, 7, null, false, null, null, null, null, null, null, null },
                    { 35L, 6, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Money", 0, 5, null, null, null, 100000m, null, null, null, null, null },
                    { 36L, 7, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Has.Image", 0, 7, null, false, null, null, null, null, null, null, null },
                    { 37L, 7, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Stats.Level", 0, 1, null, null, null, null, null, null, 13, null, null },
                    { 38L, 7, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Attr.Strength", 0, 1, null, null, null, null, null, null, 20, null, null },
                    { 39L, 7, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Attr.Constitution", 0, 1, null, null, null, null, null, null, 15, null, null },
                    { 40L, 7, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Attr.Dexterity", 0, 1, null, null, null, null, null, null, 30, null, null },
                    { 41L, 7, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Attr.Intelligence", 0, 1, null, null, null, null, null, null, 20, null, null },
                    { 42L, 7, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Money", 0, 5, null, null, null, 0m, null, null, null, null, null },
                    { 34L, 6, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Stats.Level", 0, 1, null, null, null, null, null, null, 4, null, null },
                    { 22L, 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "CanBuyTypes", 0, 6, null, null, null, null, null, null, null, null, "3,2,1" },
                    { 32L, 6, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Chat", 0, 7, null, true, null, null, null, null, null, null, null },
                    { 20L, 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Stats.Level", 0, 1, null, null, null, null, null, null, 1, null, null },
                    { 21L, 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Money", 0, 5, null, null, null, 3000m, null, null, null, null, null },
                    { 2L, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Stats.Level", 0, 1, null, null, null, null, null, null, 1, null, null },
                    { 3L, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Attr.Strength", 0, 1, null, null, null, null, null, null, 5, null, null },
                    { 4L, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Attr.Constitution", 0, 1, null, null, null, null, null, null, 5, null, null },
                    { 5L, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Attr.Dexterity", 0, 1, null, null, null, null, null, null, 10, null, null },
                    { 6L, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Attr.Intelligence", 0, 1, null, null, null, null, null, null, 5, null, null },
                    { 8L, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Has.Image", 0, 7, null, false, null, null, null, null, null, null, null },
                    { 9L, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Stats.Level", 0, 1, null, null, null, null, null, null, 1, null, null },
                    { 10L, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Money", 0, 5, null, null, null, 2000m, null, null, null, null, null },
                    { 7L, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Money", 0, 5, null, null, null, 2m, null, null, null, null, null },
                    { 18L, 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Money", 0, 5, null, null, null, 0m, null, null, null, null, null },
                    { 12L, 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Has.Image", 0, 7, null, false, null, null, null, null, null, null, null },
                    { 13L, 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Stats.Level", 0, 1, null, null, null, null, null, null, 2, null, null },
                    { 14L, 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Attr.Strength", 0, 1, null, null, null, null, null, null, 5, null, null },
                    { 15L, 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Attr.Constitution", 0, 1, null, null, null, null, null, null, 10, null, null },
                    { 16L, 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Attr.Dexterity", 0, 1, null, null, null, null, null, null, 9, null, null },
                    { 17L, 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Attr.Intelligence", 0, 1, null, null, null, null, null, null, 6, null, null },
                    { 11L, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "CanBuyTypes", 0, 6, null, null, null, null, null, null, null, null, "4" },
                    { 19L, 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Has.Image", 0, 7, null, false, null, null, null, null, null, null, null }
                });

            migrationBuilder.InsertData(
                table: "ItemDetailProperties",
                columns: new[] { "Id", "CreatedAt", "DeletedAt", "DetailId", "Key", "SortingOrder", "TypeNum", "UpdatedAt", "ValueBool", "ValueDateTime", "ValueDecimal", "ValueDouble", "ValueFloat", "ValueInt32", "ValueInt64", "ValueString" },
                values: new object[,]
                {
                    { 26L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("1a2f565c-5ed6-4d76-a097-7bc942c0876f"), "Stats.Damage", 0, 4, null, null, null, null, 1.0, null, null, null, null },
                    { 25L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("1a2f565c-5ed6-4d76-a097-7bc942c0876f"), "Stats.Level", 0, 1, null, null, null, null, null, null, 5, null, null },
                    { 24L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("774a3a7a-dad0-4ef7-b78c-4fecbf6d7303"), "Has.Image", 0, 7, null, false, null, null, null, null, null, null, null },
                    { 20L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("774a3a7a-dad0-4ef7-b78c-4fecbf6d7303"), "Stats.Level", 0, 1, null, null, null, null, null, null, 1, null, null },
                    { 22L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("774a3a7a-dad0-4ef7-b78c-4fecbf6d7303"), "Stats.Cost", 0, 5, null, null, null, 40m, null, null, null, null, null },
                    { 21L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("774a3a7a-dad0-4ef7-b78c-4fecbf6d7303"), "Stats.Damage", 0, 4, null, null, null, null, 0.5, null, null, null, null },
                    { 27L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("1a2f565c-5ed6-4d76-a097-7bc942c0876f"), "Stats.Cost", 0, 5, null, null, null, 300m, null, null, null, null, null },
                    { 23L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("774a3a7a-dad0-4ef7-b78c-4fecbf6d7303"), "Stats.Weight", 0, 4, null, null, null, null, 0.20000000000000001, null, null, null, null },
                    { 28L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("1a2f565c-5ed6-4d76-a097-7bc942c0876f"), "Stats.Weight", 0, 4, null, null, null, null, 0.5, null, null, null, null },
                    { 38L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("721d6553-e6f3-40a5-997e-35a44ed5d4ba"), "Has.Image", 0, 7, null, false, null, null, null, null, null, null, null },
                    { 30L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("ce914758-fda0-453e-9eb9-704fce6a686c"), "Stats.Level", 0, 1, null, null, null, null, null, null, 10, null, null },
                    { 31L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("ce914758-fda0-453e-9eb9-704fce6a686c"), "Stats.Damage", 0, 4, null, null, null, null, 2.0, null, null, null, null },
                    { 32L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("ce914758-fda0-453e-9eb9-704fce6a686c"), "Stats.Cost", 0, 5, null, null, null, 700m, null, null, null, null, null },
                    { 33L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("ce914758-fda0-453e-9eb9-704fce6a686c"), "Stats.Weight", 0, 4, null, null, null, null, 0.90000000000000002, null, null, null, null },
                    { 34L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("ce914758-fda0-453e-9eb9-704fce6a686c"), "Has.Image", 0, 7, null, false, null, null, null, null, null, null, null },
                    { 35L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("721d6553-e6f3-40a5-997e-35a44ed5d4ba"), "Stats.Level", 0, 1, null, null, null, null, null, null, 13, null, null },
                    { 36L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("721d6553-e6f3-40a5-997e-35a44ed5d4ba"), "Stats.Cost", 0, 5, null, null, null, 8m, null, null, null, null, null },
                    { 37L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("721d6553-e6f3-40a5-997e-35a44ed5d4ba"), "Stats.Weight", 0, 4, null, null, null, null, 0.02, null, null, null, null },
                    { 19L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("f38d2aca-5645-4be2-8407-aa51ca88c093"), "Has.Image", 0, 7, null, false, null, null, null, null, null, null, null },
                    { 29L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("1a2f565c-5ed6-4d76-a097-7bc942c0876f"), "Has.Image", 0, 7, null, false, null, null, null, null, null, null, null },
                    { 18L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("f38d2aca-5645-4be2-8407-aa51ca88c093"), "Stats.Weight", 0, 4, null, null, null, null, 1.6499999999999999, null, null, null, null },
                    { 5L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("7083ae6c-c82d-40f7-ba3e-db2b40ae7d27"), "Has.Image", 0, 7, null, true, null, null, null, null, null, null, null },
                    { 16L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("f38d2aca-5645-4be2-8407-aa51ca88c093"), "Stats.Armor", 0, 1, null, null, null, null, null, null, 2, null, null },
                    { 17L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("f38d2aca-5645-4be2-8407-aa51ca88c093"), "Stats.Cost", 0, 5, null, null, null, 100m, null, null, null, null, null },
                    { 6L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("67f935e9-0bd7-43a2-8515-69add6ed362b"), "Stats.Level", 0, 1, null, null, null, null, null, null, 1, null, null },
                    { 7L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("67f935e9-0bd7-43a2-8515-69add6ed362b"), "Stats.Armor", 0, 1, null, null, null, null, null, null, 1, null, null },
                    { 8L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("67f935e9-0bd7-43a2-8515-69add6ed362b"), "Stats.Cost", 0, 5, null, null, null, 30m, null, null, null, null, null },
                    { 9L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("67f935e9-0bd7-43a2-8515-69add6ed362b"), "Stats.Weight", 0, 4, null, null, null, null, 0.14999999999999999, null, null, null, null },
                    { 10L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("67f935e9-0bd7-43a2-8515-69add6ed362b"), "Has.Image", 0, 7, null, false, null, null, null, null, null, null, null },
                    { 3L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("7083ae6c-c82d-40f7-ba3e-db2b40ae7d27"), "Stats.Cost", 0, 5, null, null, null, 8m, null, null, null, null, null },
                    { 2L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("7083ae6c-c82d-40f7-ba3e-db2b40ae7d27"), "Stats.Health", 0, 4, null, null, null, null, 30.0, null, null, null, null },
                    { 11L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("03369820-ae44-4c5b-bbf8-fad48df88d54"), "Stats.Level", 0, 1, null, null, null, null, null, null, 1, null, null },
                    { 12L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("03369820-ae44-4c5b-bbf8-fad48df88d54"), "Stats.Cost", 0, 5, null, null, null, 4m, null, null, null, null, null },
                    { 13L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("03369820-ae44-4c5b-bbf8-fad48df88d54"), "Stats.Weight", 0, 4, null, null, null, null, 0.040000000000000001, null, null, null, null },
                    { 14L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("03369820-ae44-4c5b-bbf8-fad48df88d54"), "Has.Image", 0, 7, null, false, null, null, null, null, null, null, null },
                    { 1L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("7083ae6c-c82d-40f7-ba3e-db2b40ae7d27"), "Stats.Level", 0, 1, null, null, null, null, null, null, 1, null, null },
                    { 15L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("f38d2aca-5645-4be2-8407-aa51ca88c093"), "Stats.Level", 0, 1, null, null, null, null, null, null, 1, null, null },
                    { 4L, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("7083ae6c-c82d-40f7-ba3e-db2b40ae7d27"), "Stats.Weight", 0, 4, null, null, null, null, 0.01, null, null, null, null }
                });

            migrationBuilder.InsertData(
                table: "Items",
                columns: new[] { "Id", "CharacterId", "CreatedAt", "DeletedAt", "DetailId", "Quantity", "SortingOrder", "State", "UpdatedAt" },
                values: new object[,]
                {
                    { 2L, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("7083ae6c-c82d-40f7-ba3e-db2b40ae7d27"), 1, 0, 4, null },
                    { 8L, 7, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("721d6553-e6f3-40a5-997e-35a44ed5d4ba"), 1, 0, 3, null },
                    { 4L, 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("67f935e9-0bd7-43a2-8515-69add6ed362b"), 1, 0, 4, null },
                    { 1L, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("67f935e9-0bd7-43a2-8515-69add6ed362b"), 1, 0, 2, null },
                    { 3L, 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("03369820-ae44-4c5b-bbf8-fad48df88d54"), 1, 0, 3, null },
                    { 7L, 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("1a2f565c-5ed6-4d76-a097-7bc942c0876f"), 1, 0, 4, null },
                    { 5L, 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("f38d2aca-5645-4be2-8407-aa51ca88c093"), 1, 0, 4, null },
                    { 6L, 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("774a3a7a-dad0-4ef7-b78c-4fecbf6d7303"), 1, 0, 4, null }
                });

            migrationBuilder.InsertData(
                table: "Subscriptions_CharacterLocation",
                columns: new[] { "CharacterId", "LocationId", "CreatedAt", "DeletedAt", "SortingOrder", "UpdatedAt" },
                values: new object[,]
                {
                    { 1, new Guid("4b5a5893-0de9-42e1-b8a3-f0de3b75afb5"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 0, null },
                    { 2, new Guid("4b5a5893-0de9-42e1-b8a3-f0de3b75afb5"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 0, null },
                    { 4, new Guid("4b5a5893-0de9-42e1-b8a3-f0de3b75afb5"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 0, null },
                    { 6, new Guid("4b5a5893-0de9-42e1-b8a3-f0de3b75afb5"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 0, null },
                    { 3, new Guid("8ad83b8b-48ba-4ee2-a804-0e46b5588ccb"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 0, null },
                    { 5, new Guid("2d23c4f7-1865-420c-a846-e2ad9a4f0047"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 0, null },
                    { 7, new Guid("49266268-7039-4a06-bf53-5b1ee595359c"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 0, null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CharacterProperties_CharacterId_Key",
                table: "CharacterProperties",
                columns: new[] { "CharacterId", "Key" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Characters_Location",
                table: "Characters",
                column: "Location");

            migrationBuilder.CreateIndex(
                name: "IX_Characters_UserId",
                table: "Characters",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemDetailProperties_DetailId_Key",
                table: "ItemDetailProperties",
                columns: new[] { "DetailId", "Key" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Items_CharacterId",
                table: "Items",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_Items_DetailId",
                table: "Items",
                column: "DetailId");

            migrationBuilder.CreateIndex(
                name: "IX_Subscriptions_CharacterLocation_LocationId",
                table: "Subscriptions_CharacterLocation",
                column: "LocationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "CharacterProperties");

            migrationBuilder.DropTable(
                name: "ItemDetailProperties");

            migrationBuilder.DropTable(
                name: "Items");

            migrationBuilder.DropTable(
                name: "RefreshTokens");

            migrationBuilder.DropTable(
                name: "Subscriptions_CharacterLocation");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "ItemDetails");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Locations");

            migrationBuilder.DropTable(
                name: "AspNetUsers");
        }
    }
}
