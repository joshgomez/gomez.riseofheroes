﻿using Gomez.Core.ViewModels.Shared;
using Gomez.RiseOfHeroes.BusinessLogic.Areas.Game.Shared;
using Gomez.RiseOfHeroes.Calculations;
using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using Gomez.RiseOfHeroes.Models.Areas.Game;
using Gomez.RiseOfHeroes.Models.Areas.Game.Enums;
using Gomez.RiseOfHeroes.Repositories.Areas.Game;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcShops;
using System.Linq;
using System.Threading.Tasks;


namespace Gomez.RiseOfHeroes.BusinessLogic.Areas.Game
{
    public class NpcShopHandler : CharacterHandler<NpcShopRepository>
    {
        private readonly NpcShopRepository _npcRepository;

        public NpcShopHandler(NpcShopRepository npcRepository, ItemRepository itemRepository) : base(npcRepository,itemRepository)
        {
            _npcRepository = npcRepository;
        }

        public async Task<InventoryViewModel> GetInventoryAsync(QueryInventoryViewModel query)
        {
            var viewModel = new InventoryViewModel(query);
            viewModel = await _npcRepository.GetInventoryAsync(viewModel);
            return viewModel;
        }

        public async Task<IndexViewModel> GetIndexAsync(QueryIndexViewModel query)
        {
            var viewModel = new IndexViewModel(query);
            viewModel = await _npcRepository.IndexAsync(viewModel);
            return viewModel;
        }

        public async Task<ViewModels.Areas.Game.NpcShops.BasicInformationViewModel> GetBasicInformationAsync(int id)
        {
            var baseVm = new GetViewModel
            {
                Id = id,
                Property = "*"
            };

            baseVm = await _npcRepository.GetWithDetailsAsync(baseVm);
            var npcCacl = baseVm.GetPropertyByType<CharacterCalculation>() as NpcShopCalculation;
            npcCacl = await base.LoadEquipmentStatsAsync(baseVm.Id, npcCacl);
            return new ViewModels.Areas.Game.NpcShops.BasicInformationViewModel()
            {
                Id = baseVm.Character.Id,
                Level = npcCacl.Level,
                Name = baseVm.Character.Name,
                Money = npcCacl.Money,
            };
        }

        public async Task<CharacterViewModel> GetAsync(int id)
        {
            var baseVm = new GetViewModel
            {
                Id = id,
                Property = "*"
            };

            baseVm = await _npcRepository.GetWithDetailsAsync(baseVm);
            var vm = new CharacterViewModel(baseVm);
            vm.Calculation = await base.LoadEquipmentStatsAsync(vm.Id, vm.Calculation);


            return vm;
        }

        public async Task<int> RefillStockForAllAsync()
        {
            var updated = await _npcRepository.RefillStockForAllAsync();
            return updated;
        }


        /// <summary>
        /// Information when buying something from a npc
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="qty"></param>
        /// <returns></returns>
        public async Task<(ValidationViewModel<SellInternalViewModel> vm, Item item)> GetSellInformationAsync(long itemId, int qty)
        {
            var innerVm = new SellInternalViewModel();
            var vm = new ValidationViewModel<SellInternalViewModel>(innerVm);

            var item = await _itemRepository.FindAsync(itemId);
            if(item == null)
            {
                vm.AddError("Shop.Sell.Item.NotFound.Text");
                return (vm,null);
            }

            innerVm.Quantity = qty;
            if (item.State != ItemState.ShopSale && item.State != ItemState.ShopSecondHand)
            {
                vm.AddError("Shop.Sell.Item.Invalid.Text");
                return (vm, item);
            }

            if (item.Quantity < qty)
            {
                vm.AddError("Shop.Sell.Item.RequiredQuantityToBig.Text");
                return (vm, item);
            }

            var npc = await _npcRepository.GetWithDetailsAsync(item.CharacterId, $"{CharacterConstant.KEY_MONEY}");
            innerVm.SellersId = npc.Id;
            innerVm.SellersMoney = npc.PropertyCollection.GetProperty<decimal>(CharacterConstant.KEY_MONEY, out byte[] rowVersion);
            innerVm.SellersQuantity = item.Quantity - qty;
            innerVm.SellersMoneyVersion = rowVersion;

            if(npc.Character.Type != CharacterConstant.TYPE_NPC_SHOP)
            {
                vm.AddError("Shop.Sell.NotFound");
                return (vm, item);
            }

            var detail = await _itemRepository.DetailRepo.GetWithDetailsAsync(item.DetailId, $"{ItemConstant.KEY_STATS_COST}");
            innerVm.ItemName = detail.Detail.Name;
            var cost = detail.PropertyCollection.GetProperty<decimal>(ItemConstant.KEY_STATS_COST);
            cost = ItemCalculation.CostCalculation(cost, item.State);
            innerVm.Cost = cost * qty;
            return (vm,item);
        }

        /// <summary>
        /// Information when selling something to a npc.
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="qty"></param>
        /// <param name="buyerId">The id of the npc</param>
        /// <returns></returns>
        public async Task<(ValidationViewModel<SellInternalViewModel> vm, Item item)> GetBuyInformationAsync(int buyerId, long itemId, int qty)
        {
            var innerVm = new SellInternalViewModel();
            var vm = new ValidationViewModel<SellInternalViewModel>(innerVm);

            var item = await _itemRepository.FindAsync(itemId);
            if (item == null)
            {
                vm.AddError("Shop.Buy.Item.NotFound.Text");
                return (vm, null);
            }

            innerVm.Quantity = qty;
            if (item.State == ItemState.Equiped)
            {
                vm.AddError("Shop.Buy.Item.Invalid.Text");
                return (vm, item);
            }

            if (item.Quantity < qty)
            {
                vm.AddError("Shop.Buy.Item.RequiredQuantityToBig.Text");
                return (vm, item);
            }

            var npc = await _npcRepository.GetWithDetailsAsync(buyerId,
                $"{CharacterConstant.KEY_MONEY},{NpcShopConstant.KEY_CAN_BUY_TYPES}");

            //Can only buy this types
            var itemTypes = npc.PropertyCollection
                .GetProperty<string>(NpcShopConstant.KEY_CAN_BUY_TYPES).Split(',')
                .Select(x => int.Parse(x)).ToArray();

            innerVm.BuyersId = npc.Id;
            innerVm.BuyersMoney = npc.PropertyCollection
                .GetProperty<decimal>(CharacterConstant.KEY_MONEY, out byte[] rowVersion);
            innerVm.BuyersMoneyVersion = rowVersion;

            if (npc.Character.Type != CharacterConstant.TYPE_NPC_SHOP)
            {
                vm.AddError("Shop.Buy.NotFound.Text");
                return (vm, item);
            }

            var detail = await _itemRepository.DetailRepo.GetWithDetailsAsync(item.DetailId, $"{ItemConstant.KEY_STATS_COST}");
            innerVm.ItemName = detail.Detail.Name;
            if (!itemTypes.Contains(detail.Detail.Type))
            {
                vm.AddError("Shop.Buy.CannotBuy.Text");
                return (vm, item);
            }

            var cost = detail.PropertyCollection.GetProperty<decimal>(ItemConstant.KEY_STATS_COST);
            cost = ItemCalculation.CostCalculation(cost, item.State);
            innerVm.Cost = cost * qty;
            if (innerVm.Cost > vm.Model.BuyersMoney)
            {
                vm.AddError("Shop.Buy.NotEnoughMoney.Text");
            }

            return (vm, item);
        }
    }
}
