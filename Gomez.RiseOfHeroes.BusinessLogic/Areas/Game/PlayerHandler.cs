﻿using Gomez.Core.Models.Exceptions;
using Gomez.RiseOfHeroes.BusinessLogic.Areas.Game.Shared;
using Gomez.RiseOfHeroes.Calculations;
using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using Gomez.RiseOfHeroes.Models.Areas.Game;
using Gomez.RiseOfHeroes.Repositories.Areas.Game;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Players;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using ItemDetailVm = Gomez.RiseOfHeroes.ViewModels.Areas.Game.ItemDetails;
using NpcShopsVm = Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcShops;

namespace Gomez.RiseOfHeroes.BusinessLogic.Areas.Game
{
    public class PlayerHandler : CharacterHandler<PlayerRepository>
    {
        private readonly PlayerRepository _playerRepository;

        public PlayerHandler(PlayerRepository playerRepository, ItemRepository itemRepository) : base(playerRepository,itemRepository)
        {
            _playerRepository = playerRepository;
        }

        public async Task NewPlayerAsync(Guid userId)
        {
            await _playerRepository.CreateCharacterIfNullAsync(userId);
        }

        public async Task SetBasicAsync(Guid userId, QuerySetBasicViewModel vm)
        {
            await _playerRepository.SetBasicAsync(userId,vm);
        }

        public async Task<ReplenishEnergyViewModel> ReplenishEnergyAsync(Guid userId)
        {
            try
            {
                return await _playerRepository.ReplenishEnergyAsync(userId);
            }
            catch(Exception ex)
            {
                if(ex is ConcurrencyException || ex is DbUpdateConcurrencyException)
                {
                    return await _playerRepository.ReplenishEnergyAsync(userId);
                }

                throw;
            }
        }

        public async Task<bool> DeleteAsync(Guid userId)
        {
            return await _playerRepository.DeleteAsync(userId);
        }

        public async Task<bool> SetAttributesAsync(Guid userId, QuerySetAttributeViewModel vm)
        {
            return await _playerRepository.SetAttributesAsync(userId, vm);
        }

        public async Task<ViewModels.Areas.Game.Players.BasicInformationViewModel> GetBasicInformationAsync(Guid userId)
        {
            var baseVm = new GetViewModel
            {
                Property = "*"
            };

            baseVm = await _playerRepository.GetWithDetailsAsync(userId, baseVm);
            var playerCacl = baseVm.GetPropertyByType<CharacterCalculation>() as PlayerCalculation;
            playerCacl = await base.LoadEquipmentStatsAsync(baseVm.Id, playerCacl);
            return new ViewModels.Areas.Game.Players.BasicInformationViewModel()
            {
                Id = baseVm.Character.Id,
                Name = baseVm.Character.Name,
                Health = playerCacl.Health,
                MaxHealth = playerCacl.MaxHealth,
                Energy = playerCacl.Energy,
                MaxEnergy = playerCacl.MaxEnergy,
                MaxDamage = playerCacl.MaxDamage,
                MinDamage = playerCacl.MinDamage,
                Strength = baseVm.PropertyCollection.GetProperty<int>(CharacterConstant.KEY_ATTR_STRENGTH),
                Constitution = baseVm.PropertyCollection.GetProperty<int>(CharacterConstant.KEY_ATTR_CONSTITUTION),
                Dexterity = baseVm.PropertyCollection.GetProperty<int>(CharacterConstant.KEY_ATTR_DEXTERITY),
                Intelligence = baseVm.PropertyCollection.GetProperty<int>(CharacterConstant.KEY_ATTR_INTELLIGENCE),
                Weight = playerCacl.Weight,
                Attributes = baseVm.PropertyCollection.GetProperty<int>(PlayerConstant.KEY_STATS_ATTRIBUTEPOINTS),
                Level = baseVm.PropertyCollection.GetProperty<int>(CharacterConstant.KEY_STATS_LEVEL),
                Armor = playerCacl.Armor,
                Experience = baseVm.PropertyCollection.GetProperty<int>(PlayerConstant.KEY_STATS_EXPERIENCE),
                NextLevel = playerCacl.NextLevel,
                NextLevelFactor = playerCacl.NextLevelFactor,
                Money = playerCacl.Money,
                Coordinate = baseVm.Character.Coordinate
            };
        }

        public async Task<ViewModels.Hubs.Chat.UserInformationViewModel> 
            GetMinimumChatInfoAsync(Guid userId)
        {
            var vm = await _playerRepository.GetMinimumInfoAsync(userId);
            if (vm != null)
            {
                return new ViewModels.Hubs.Chat.UserInformationViewModel(vm);
            }

            return null;
        }

        public async Task<CharacterViewModel> GetAsync(Guid userId)
        {
            var baseVm = new GetViewModel
            {
                Property = "*"
            };

            baseVm = await _playerRepository.GetWithDetailsAsync(userId, baseVm);
            var vm = new CharacterViewModel(baseVm);
            return vm;
        }

        public async Task<CharacterViewModel> GetAsync(int id)
        {
            var baseVm = new GetViewModel
            {
                Id = id,
                Property = "*"
            };

            baseVm = await _playerRepository.GetWithDetailsAsync(baseVm);
            var vm = new CharacterViewModel(baseVm);
            vm.Calculation = await base.LoadEquipmentStatsAsync(vm.Id, vm.Calculation);
            return vm;
        }

        public async Task<InventoryViewModel> GetInventoryAsync(Guid userId, QueryInventoryViewModel query)
        {
            var viewModel = new InventoryViewModel(query);
            viewModel = await _playerRepository.GetInventoryAsync(userId, viewModel);
            return viewModel;
        }

        public async Task<NpcShopsVm.PlayerInventoryViewModel> GetSaleInventoryAsync(Guid userId, NpcShopsVm.QueryPlayerInventoryViewModel query)
        {
            var viewModel = new NpcShopsVm.PlayerInventoryViewModel(query);
            viewModel = await _playerRepository.GetSaleInventoryAsync(userId, viewModel);
            return viewModel;
        }

        public async Task<InventoryViewModel> GetInventoryAsync(QueryInventoryViewModel query)
        {
            var viewModel = new InventoryViewModel(query);
            viewModel = await _playerRepository.GetInventoryAsync(viewModel);
            return viewModel;
        }

        public async Task<bool> UnequipItemAsync(Guid userId, long itemId)
        {
            int id = await _playerRepository.GetIdByUserIdAsyc(userId);
            return await UnequipItemAsync(id, itemId);
        }

        public async Task<bool> UnequipItemAsync(int id, long itemId)
        {
            try
            {
                await _itemRepository.UnequipItemAsync(id, itemId);
                return true;
            }
            catch (NotFoundException)
            {
                return false;
            }
        }

        public async Task<ItemDetailVm.GetViewModel> UseItemAsync(Guid userId, long itemId)
        {
            int id = await _playerRepository.GetIdByUserIdAsyc(userId);
            return await UseItemAsync(id, itemId);
        }

        public async Task<ItemDetailVm.GetViewModel> UseItemAsync(int id, long itemId)
        {
            var item = await _itemRepository.FindAsync(itemId);
            if(item == null || item.CharacterId != id)
            {
                return null;
            }

            var detailVm = await _itemRepository.DetailRepo.GetWithDetailsAsync(item.DetailId);
            switch(detailVm.Detail.Type)
            {
                case ItemConstant.TYPE_POTION:
                    await UsePotionAsync(item, detailVm);
                    break;
                case ItemConstant.TYPE_HEAD_GEAR:
                case ItemConstant.TYPE_BODY_GEAR:
                case ItemConstant.TYPE_WEAPON:
                    await EquipItemAsync(item, detailVm);
                    break;
            }

            detailVm.Item = _itemRepository.MapItemToViewModel(item);
            return detailVm;
        }

        private async Task UsePotionAsync(Item item, ItemDetailVm.GetViewModel detailVm)
        {
            var character = await GetAsync(item.CharacterId);
            var itemCalc = detailVm.GetPropertyByType<ItemCalculation>();
            var health = itemCalc.HealingEffect * character.Calculation.MaxHealth;
            character.Calculation.AddHealth(health);

            item.Quantity--;
            if (item.Quantity < 1)
            {
                await _itemRepository.DeleteAsync(item, false);
            }

            //Will also save item changes from above
            await _playerRepository.SetSinglePropertyAsync(character.Id, CharacterConstant.KEY_STATS_HEALTH, character.Calculation.Health, 
                rowVersion: character.Calculation.RowVersions[CharacterConstant.KEY_STATS_HEALTH]);
        }

        private async Task EquipItemAsync(Item item, ItemDetailVm.GetViewModel detailVm)
        {
            await _itemRepository.AddEquippedItemAsync(item, detailVm);
        }

        public async Task<bool> SaveFromViewModelAsync(CharacterViewModel vm)
        {
            return await _playerRepository.SaveFromViewModelAsync(vm);
        }

        public async Task<bool> SaveFromViewModelAndLootAsync(CharacterViewModel vm, Guid[] itemIds)
        {
            using var transaction = await _playerRepository.BeginTransactionAsync();
            try
            {
                foreach (var itemId in itemIds)
                {
                    var toItem = await _itemRepository
                        .GetItemByDetailIdAsync(vm.Id, itemId,
                        Models.Areas.Game.Enums.ItemState.Unequiped);
                    await _itemRepository.AddItemByDetailAsync(itemId, 1, vm.Id, toItem, false);
                }

                var ret =  await _playerRepository.SaveFromViewModelAsync(vm);
                await transaction.CommitAsync();
                return ret;
            }
            catch
            {
                await transaction.RollbackAsync();
                throw;
            }
        }

        public async Task<bool> TravelAsync(Guid userId, Guid locationId, LocationHandler handler)
        {
            var characterVm = await _playerRepository.GetWithDetailsAsync(userId, new GetViewModel() { Property = CharacterConstant.KEY_MONEY });
            var locationVm = await handler.GetAsync(new ViewModels.Areas.Game.Locations.QueryGetViewModel() { Id = locationId, Search = "*" });
            var info = new ViewModels.Areas.Game.Locations.PositionInformation(locationVm.Location.Coordinate);
            info.Init(characterVm.Character.Coordinate);
            if (info.Distance > 0)
            {
                return await _playerRepository.TravelAsync(characterVm, info.Cost, locationVm.Location.Coordinate);
            }

            return false;
        }

        public async Task<EquipmentViewModel> GetEquipmentAsync(Guid userId)
        {
            int id = await _playerRepository.GetIdByUserIdAsyc(userId);
            return await GetEquipmentAsync(id);
        }
    }
}
