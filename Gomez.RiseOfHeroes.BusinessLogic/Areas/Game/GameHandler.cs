﻿using Gomez.RiseOfHeroes.Models.Contexts;
using Gomez.RiseOfHeroes.Repositories.Areas.Game;
using Microsoft.Extensions.Localization;

namespace Gomez.RiseOfHeroes.BusinessLogic.Areas.Game
{
    public class GameHandler
    {
        public CharacterHandler CharacterHandler { get; private set; }
        public PlayerHandler PlayerHandler { get; private set; }
        public NpcEnemyHandler NpcEnemyHandler { get; private set; }
        public NpcShopHandler NpcShopHandler { get; private set; }
        public NpcCustomHandler NpcCustomHandler { get; private set; }
        public WorkHandler WorkHandler { get; set; }
        public ShopHandler ShopHandler { get; private set; }
        public LocationHandler LocationHandler { get; private set; }
        public BattleHelper BattleHelper { get; private set; }
        public AttackHandler AttackHandler { get; private set; }
        public ItemHandler ItemHandler { get; private set; }
        public LootHandler LootHandler { get; private set; }

        public MiniGameHandler MiniGameHandler { get; set; }


        public GameHandler(ApplicationDbContext db, IStringLocalizerFactory localizerFactory)
        {
            var playerRepo = new PlayerRepository(db, localizerFactory);
            var itemDetailRepo = new ItemDetailRepository(db, localizerFactory);
            var itemRepo = new ItemRepository(db, localizerFactory, itemDetailRepo);

            var characterRepo = new CharacterSharedRepository(db, localizerFactory);
            var npcEnemyRepo = new NpcEnemyRepository(db, localizerFactory);
            var npcShopRepo = new NpcShopRepository(db, localizerFactory);
            var npcCustomRepo = new NpcCustomRepository(db, localizerFactory);
            var shopRepo = new ShopRepository(db, localizerFactory,itemRepo,playerRepo,npcShopRepo);
            var miniGameRepo = new MiniGameRepository(db, localizerFactory, playerRepo, npcCustomRepo);

            var locationRepo = new LocationRepository(db, localizerFactory);

            ItemHandler = new ItemHandler(itemRepo);
            PlayerHandler = new PlayerHandler(playerRepo,itemRepo);

            CharacterHandler = new CharacterHandler(characterRepo, itemRepo);
            NpcEnemyHandler = new NpcEnemyHandler(npcEnemyRepo, itemRepo);
            NpcShopHandler = new NpcShopHandler(npcShopRepo, itemRepo);
            NpcCustomHandler = new NpcCustomHandler(npcCustomRepo, itemRepo);

            WorkHandler = new WorkHandler(npcCustomRepo, playerRepo);
            MiniGameHandler = new MiniGameHandler(playerRepo, npcCustomRepo, miniGameRepo);

            LocationHandler = new LocationHandler(locationRepo);
            BattleHelper = new BattleHelper(localizerFactory);
            LootHandler = new LootHandler(itemRepo);
            AttackHandler = new AttackHandler(PlayerHandler, NpcEnemyHandler, LootHandler, BattleHelper);
            ShopHandler = new ShopHandler(NpcShopHandler, shopRepo, playerRepo);
        }

    }
}
