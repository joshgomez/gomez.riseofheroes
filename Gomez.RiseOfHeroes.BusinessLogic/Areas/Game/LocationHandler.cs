﻿using Gomez.RiseOfHeroes.Repositories.Areas.Game;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Locations;
using Nts = NetTopologySuite.Geometries;
using System.Threading.Tasks;
using System;

namespace Gomez.RiseOfHeroes.BusinessLogic.Areas.Game
{
    public class LocationHandler
    {
        private readonly LocationRepository _locationRepository;

        public LocationHandler(LocationRepository locationRepository)
        {
            _locationRepository = locationRepository;
        }

        public async Task<IndexViewModel> GetIndexAsync(QueryIndexViewModel query)
        {
            var viewModel = new IndexViewModel(query);
            viewModel = await _locationRepository.IndexAsync(viewModel);
            foreach (var item in viewModel.Locations.Results)
            {
                item.Init(viewModel.CurrentPosition);
            }
            return viewModel;
        }

        public async Task<GetViewModel> GetAsync(QueryGetViewModel query)
        {
            var viewModel = new GetViewModel(query);
            viewModel = await _locationRepository.GetWithDetailsAsync(viewModel);
            return viewModel;
        }

        public async Task<GetViewModel> GetAsync(Nts.Point point, QueryGetViewModel query)
        {
            var viewModel = await _locationRepository.GetWithDetailsAsync(point, query);
            return viewModel;
        }
        public async Task<string> GetGescriptionAsync(Guid id)
        {
            return await _locationRepository.GetDescriptionAsync(id);
        }
    }
}
