﻿using Gomez.RiseOfHeroes.Repositories.Areas.Game;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters;
using System;
using System.Threading.Tasks;
using NpcVm = Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcCustoms;
using playerVm = Gomez.RiseOfHeroes.ViewModels.Areas.Game.Players;

namespace Gomez.RiseOfHeroes.BusinessLogic.Areas.Game
{
    public class WorkHandler
    {
        private readonly NpcCustomRepository _npcRepository;
        private readonly PlayerRepository _playerRepository;

        public WorkHandler(NpcCustomRepository npcRepository, PlayerRepository playerRepository)
        {
            _npcRepository = npcRepository;
            _playerRepository = playerRepository;
        }

        public async Task<NpcVm.WorkViewModel> DoWorkAsync(NpcVm.QueryWorkViewModel query, Guid userId)
        {
            var vm = new NpcVm.WorkViewModel(query);
            var playerVm = await _playerRepository.GetWithDetailsAsync(userId, new GetViewModel { Property = "*" });
            var viewModel = await _npcRepository.WorkAsync(vm, playerVm);
            return viewModel;
        }
    }
}
