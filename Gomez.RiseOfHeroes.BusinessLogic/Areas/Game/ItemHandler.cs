﻿using Gomez.RiseOfHeroes.Repositories.Areas.Game;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.ItemDetails;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.BusinessLogic.Areas.Game
{
    public class ItemHandler
    {
        private readonly ItemRepository _itemRepository;

        public ItemHandler(ItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public async Task<GetViewModel> GetAsync(long itemId, QueryGetViewModel query)
        {
            var item = await _itemRepository.FindAsync(itemId);
            query.Id = item.DetailId;
            var viewModel = new GetViewModel(query)
            {
                Item = _itemRepository.MapItemToViewModel(item)
            };

            viewModel = await _itemRepository.DetailRepo.GetWithDetailsAsync(viewModel);
            return viewModel;
        }

        public async Task<GetViewModel> GetAsync(QueryGetViewModel query)
        {
            var viewModel = new GetViewModel(query);
            viewModel = await _itemRepository.DetailRepo.GetWithDetailsAsync(viewModel);
            return viewModel;
        }

        public async Task<string> GetDescriptionAsync(long id)
        {
            var detailId = await _itemRepository.GetDetailIdByIdAsync(id);
            return await _itemRepository.DetailRepo.GetDescriptionAsync(detailId);
        }
    }
}
