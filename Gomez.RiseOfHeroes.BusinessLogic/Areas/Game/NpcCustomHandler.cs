﻿using Gomez.RiseOfHeroes.BusinessLogic.Areas.Game.Shared;
using Gomez.RiseOfHeroes.Calculations;
using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using Gomez.RiseOfHeroes.Repositories.Areas.Game;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters;
using System.Linq;
using System.Threading.Tasks;
using VM = Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcCustoms;


namespace Gomez.RiseOfHeroes.BusinessLogic.Areas.Game
{
    public class NpcCustomHandler : CharacterHandler<NpcCustomRepository>
    {
        private readonly NpcCustomRepository _npcRepository;

        public NpcCustomHandler(NpcCustomRepository npcRepository, ItemRepository itemRepository) : base(npcRepository,itemRepository)
        {
            _npcRepository = npcRepository;
        }

        public async Task<IndexViewModel> GetIndexAsync(QueryIndexViewModel query)
        {
            var viewModel = new IndexViewModel(query);
            viewModel = await _npcRepository.IndexAsync(viewModel);
            return viewModel;
        }

        public async Task<ViewModels.Areas.Game.NpcCustoms.IBasicInformationViewModel> GetBasicInformationAsync(int id)
        {
            var baseVm = new GetViewModel
            {
                Id = id,
                Property = "*"
            };

            baseVm = await _npcRepository.GetWithDetailsAsync(baseVm);
            var npcCacl = baseVm.GetPropertyByType<CharacterCalculation>() as NpcCustomCalculation;
            return new VM.BasicInformationViewModel()
            {
                Id = baseVm.Character.Id,
                Level = npcCacl.Level,
                Name = baseVm.Character.Name,
                Money = npcCacl.Money,
                HasBlackJack = baseVm.PropertyCollection.GetProperty<bool>(NpcCustomConstant.KEY_GAME_BLACKJACK),
                HasSlotMachine = baseVm.PropertyCollection.GetProperty<bool>(NpcCustomConstant.KEY_GAME_SLOTS),
                HasRoulette = baseVm.PropertyCollection.GetProperty<bool>(NpcCustomConstant.KEY_GAME_ROULETTE),
                Coordinate = baseVm.Character.Coordinate,
                WorkTypes = baseVm.PropertyCollection
                    .Properties.Where(x => x.Key.StartsWith("Work.")).ToDictionary(x => x.Key, x => (int)x.Value)
            };
        }

        public async Task<VM.WorkDetailsViewModel> GetWorkDetailsAsync(int id, string type)
        {
            var viewModel = await _npcRepository.GetWorkDetailsAsync(id, type);
            return viewModel;
        }
    }
}
