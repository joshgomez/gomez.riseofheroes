﻿using Gomez.RiseOfHeroes.Models.Areas.Game.Enums;
using Gomez.RiseOfHeroes.Resources;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Battle;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Gomez.RiseOfHeroes.Resources.Areas.Game;
using Gomez.Core.Utilities;

namespace Gomez.RiseOfHeroes.BusinessLogic.Areas.Game
{
    public class BattleHelper
    {
        private readonly IStringLocalizer _sharedLocalizer;
        private readonly IStringLocalizer _battleLocalizer;

        public BattleHelper(IStringLocalizerFactory localizerFactory)
        {
            _sharedLocalizer = localizerFactory.Create(typeof(GameResource));
            _battleLocalizer = localizerFactory.Create(typeof(BattleResources));
        }

        public IEnumerable<KeyValuePair<int,string>> TargetTypes()
        {
            foreach (TargetType type in Enum.GetValues(typeof(TargetType)))
            {

                yield return new KeyValuePair<int, string>((int)type, _sharedLocalizer[type.ToString()]);
            }
        }

        public IEnumerable<KeyValuePair<int, string>> TacticTypes()
        {
            foreach (TacticType type in Enum.GetValues(typeof(TacticType)))
            {

                yield return new KeyValuePair<int, string>((int)type, _sharedLocalizer[type.ToString()]);
            }
        }

        public ResultViewModel ToResult(ResultInfo info)
        {
            var vm = new ResultViewModel();
            ResultViewModel.BattleRound current = null;
            foreach (var log in info.EventLog)
            {
                if (log.Key == "Battle.Round")
                {
                    int round = (int)log.Value[0];
                    var battleRound = new ResultViewModel.BattleRound() { Round = round };
                    current = battleRound;
                    vm.Rounds.Add(round, battleRound);
                }

                if(current != null)
                {
                    if (log.Key.StartsWith("Battle.Target.Opponent"))
                    {
                        var msg = new string[] {
                            "{0} takes the initiative and takes the moment to attack {1}.",
                            "{0} is quick to react and begin the attack on {1}.",
                            "{0} is violently rushing at {1}.",
                            "{0} is carefully moving towards {1}."
                        };

                        var randomIndex = NumberUtility.RandomBetween(0, msg.Length - 1);
                        current.Messages.Add(_battleLocalizer[msg[randomIndex], log.Value]);
                        continue;
                    }

                    if (log.Key.StartsWith("Battle.Attack.Miss"))
                    {
                        var msg = new string[] {
                            "{0} fail to hit {1}.",
                            "{0} got dust in the eye and {1} successfully evaded {0}'s attack.",
                            "{0} landed on wrong foot and failed to hit {1}."
                        };

                        var randomIndex = NumberUtility.RandomBetween(0, msg.Length - 1);
                        current.Messages.Add(_battleLocalizer[msg[randomIndex], log.Value[0], log.Value[1]]);
                        continue;
                    }

                    if (log.Key.StartsWith("Battle.Attack.Hit"))
                    {
                        var msg = new string[] {
                            "{0} hits {1}.",
                            "{0} landed a blow on {1} because {1} reacted to slow.",
                            "{0} hits {1} as {1} could not dodge the attack."
                        };

                        var randomIndex = NumberUtility.RandomBetween(0, msg.Length - 1);
                        current.Messages.Add(_battleLocalizer[msg[randomIndex], log.Value[0], log.Value[1]]);
                        continue;
                    }

                    if (log.Key.StartsWith("Battle.Attack.Critical"))
                    {
                        var msg = new string[] {
                            "{0} hit {1} really hard.",
                            "{1} was hurt so bad {1} did not feel the damage done by {0}.",
                            "{0} landed a critical hit on {1}."
                        };

                        var randomIndex = NumberUtility.RandomBetween(0, msg.Length - 1);
                        current.Messages.Add(_battleLocalizer[msg[randomIndex], log.Value[0], log.Value[1]]);
                        continue;
                    }

                    if (log.Key.StartsWith("Battle.DoDamage"))
                    {
                        var msg = new string[] {
                            "{0} did {2:0.##} damage on {1}, {1} has {3:0.##} health left.",
                            "{1} got damaged by {0}, {2:0.##} in damage, {1} has {3:0.##} health left.",
                        };

                        var randomIndex = NumberUtility.RandomBetween(0, msg.Length - 1);
                        current.Messages.Add(_battleLocalizer[msg[randomIndex], log.Value]);
                        continue;
                    }

                    if (log.Key.StartsWith("Battle.Died"))
                    {
                        var msg = new string[] {
                            "{0} collapsed on the grund. Lost {1} experience.",
                            "{0} fainted. Lost {1} experience.",
                        };

                        var randomIndex = NumberUtility.RandomBetween(0, msg.Length - 1);
                        current.Messages.Add(_battleLocalizer[msg[randomIndex], log.Value]);
                        continue;
                    }

                    if (log.Key.StartsWith("Battle.Fallen"))
                    {
                        var msg = new string[] {
                            "{0} fled the battle.",
                            "{0} stepped down from the battle.",
                        };

                        var randomIndex = NumberUtility.RandomBetween(0, msg.Length - 1);
                        current.Messages.Add(_battleLocalizer[msg[randomIndex], log.Value]);
                        continue;
                    }

                    if (log.Key.EndsWith("Battle.AddExperience"))
                    {
                        var msg = new string[] {
                            "{0} gained {1} experience."
                        };

                        current.Messages.Add(_battleLocalizer[msg[0], log.Value]);
                        continue;
                    }

                    if (log.Key.StartsWith("Battle.AddExperience.Levelup"))
                    {
                        var msg = new string[] {
                            "{0} rached a new level! {0} is now {1}."
                        };

                        current.Messages.Add(_battleLocalizer[msg[0], log.Value]);
                        continue;
                    }


                    if (log.Key.EndsWith("Battle.Result.Draw"))
                    {
                        var msg = new string[] {
                            "Battle ended up in a draw fight."
                        };

                        current.Messages.Add(_battleLocalizer[msg[0]]);
                        continue;
                    }

                    if (log.Key.StartsWith("Battle.Result.Won"))
                    {
                        var namesArr = (string[])log.Value;
                        var names = String.Join(", ", namesArr);
                        var msg = new string[] {
                            "{0} won the fight!"
                        };

                        current.Messages.Add(_battleLocalizer[msg[0],names]);
                        if(info.Items?.Count > 0)
                        {
                            string items = String.Join(", ", info.Items.Values);
                            current.Messages.Add(_battleLocalizer["Found {0}.", items]);
                        }
                    }
                }
            }

            return vm;
        }
    }
}
