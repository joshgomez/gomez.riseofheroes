﻿using Gomez.Core.Models.Exceptions;
using Gomez.Core.Utilities.Game.Models.Cards;
using Gomez.RiseOfHeroes.BusinessLogic.Areas.Game.Shared;
using Gomez.RiseOfHeroes.Repositories.Areas.Game;
using Gomez.RiseOfHeroes.Repositories.Areas.Game.MiniGames;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.MiniGames;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.MiniGames.BlackJack;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System;
using System.IO;
using System.Threading.Tasks;
using NpcVm = Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcCustoms;
using playerVm = Gomez.RiseOfHeroes.ViewModels.Areas.Game.Players;

namespace Gomez.RiseOfHeroes.BusinessLogic.Areas.Game.MiniGames
{
    public class BlackJackHandler : MiniGameHandlerBase
    {
        private IBlackJack _game = null;
        private CardSaveData _data;

        private new readonly BlackJackRepository _gameRepository;

        public BlackJackHandler(PlayerRepository playerRepository, NpcCustomRepository npcRepository, BlackJackRepository gameRepository) : 
            base(playerRepository, npcRepository, gameRepository)
        {
            _gameRepository = (BlackJackRepository)base._gameRepository;
        }

        private async Task ValidationLocationAsync(NpcVm.IBasicInformationViewModel npc, playerVm.BasicInformationViewModel player)
        {
            if (!await _npcRepository.IsInSamePositionAsync(npc.Id, player.Coordinate))
            {
                throw new BadRequestException(nameof(player.Coordinate));
            }

            if (!npc.HasBlackJack)
            {
                throw new BadRequestException(nameof(npc.HasBlackJack));
            }
        }

        /// <summary>
        /// Load a existing game, return false if no game exists.
        /// </summary>
        /// <param name="npc"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        public async Task<bool> LoadAsync(NpcVm.IBasicInformationViewModel npc, playerVm.BasicInformationViewModel player)
        {
            await ValidationLocationAsync(npc, player);
            if(!await _gameRepository.AnyBlackJackStateAsync(player.Id))
            {
                return false;
            }

            var base64 = await _gameRepository.GetBlackJackStateAsync(player.Id);
            if (!String.IsNullOrEmpty(base64) && Load(base64))
            {
                _data.PlayerId = player.Id;
                _data.NpcId = npc.Id;
                await ValidateEndGameAsync();
                return true;
            }

            //Can start a new game..
            return false;
        }

        /// <summary>
        /// Start a new game, throw exception if a game is in progress.
        /// </summary>
        /// <param name="npc"></param>
        /// <param name="player"></param>
        /// <param name="stake"></param>
        /// <returns></returns>
        public async Task<bool> NewAsync(NpcVm.IBasicInformationViewModel npc, playerVm.BasicInformationViewModel player, decimal stake)
        {
            if(stake <= 0)
            {
                throw new NotEnoughException(nameof(stake));
            }

            if (stake > player.Money * 0.10m)
            {
                throw new OutOfBoundException(nameof(stake));
            }

            await ValidationLocationAsync(npc, player);

            if (await _gameRepository.AnyBlackJackStateAsync(player.Id))
            {
                throw new UncompletedException();
            }

            _game = new BlackJack(stake);
            _data = await _gameRepository.InitBlackJackStateAsync(npc,player,_game);
            await ValidateEndGameAsync();
            return true;
        }

        public async Task StayAsync()
        {
            _game.Stay();
            if(!await ValidateEndGameAsync())
            {
                await SaveAsync();
            }
        }

        public async Task HitAsync()
        {
            _game.Hit();
            if (!await ValidateEndGameAsync())
            {
                await SaveAsync();
            }
        }

        public GetViewModel GetResults()
        {
            return new GetViewModel(_game);
        }

        private bool Load(string base64)
        {
            byte[] data = Convert.FromBase64String(base64);

            var ms = new MemoryStream(data);
            _data = null;
            using (var reader = new BsonDataReader(ms))
            {
                JsonSerializer serializer = new JsonSerializer();
                _data = serializer.Deserialize<CardSaveData>(reader);

            }

            if (_data == null)
            {
                return false;
            }

            _game = BlackJack.Load(_data);
            return true;
        }

        /// <summary>
        /// Reward the player or end the game if the player lost. If the game is still in progress returns false. 
        /// </summary>
        /// <returns></returns>
        private async Task<bool> ValidateEndGameAsync()
        {
            if(_game == null || _game.State == BlackJack.StateType.Undetermined)
            {
                return false;
            }

            await _gameRepository.EndBlackJackGameAsync(_data.NpcId, _data.PlayerId, _game.Profit);
            return true;
        }

        private async Task<bool> SaveAsync()
        {
            if (_game == null)
            {
                return false;
            }

            await _gameRepository.SaveAsync(_data.PlayerId, _game);
            return true;
        }


    }
}
