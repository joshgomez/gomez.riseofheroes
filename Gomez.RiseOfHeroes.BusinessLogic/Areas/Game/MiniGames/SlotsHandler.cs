﻿using Gomez.Core.Models.Exceptions;
using Gomez.Core.Utilities.Game.Models.Slots;
using Gomez.RiseOfHeroes.BusinessLogic.Areas.Game.Shared;
using Gomez.RiseOfHeroes.Repositories.Areas.Game;
using Gomez.RiseOfHeroes.Repositories.Areas.Game.MiniGames;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.MiniGames.Slots;
using System.Threading.Tasks;
using NpcVm = Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcCustoms;
using playerVm = Gomez.RiseOfHeroes.ViewModels.Areas.Game.Players;

namespace Gomez.RiseOfHeroes.BusinessLogic.Areas.Game.MiniGames
{
    public class SlotsHandler : MiniGameHandlerBase
    {
        private ISlotMachine _game = null;
        private new readonly SlotsRepository _gameRepository;

        public SlotsHandler(PlayerRepository playerRepository, NpcCustomRepository npcRepository, SlotsRepository gameRepository) : 
            base(playerRepository, npcRepository, gameRepository)
        {
            _gameRepository = (SlotsRepository)base._gameRepository;
        }

        private async Task ValidationLocationAsync(NpcVm.IBasicInformationViewModel npc, playerVm.BasicInformationViewModel player)
        {
            if (!await _npcRepository.IsInSamePositionAsync(npc.Id, player.Coordinate))
            {
                throw new BadRequestException(nameof(player.Coordinate));
            }

            if (!npc.HasSlotMachine)
            {
                throw new BadRequestException(nameof(npc.HasSlotMachine));
            }
        }

        /// <summary>
        /// Start a new game
        /// </summary>
        /// <param name="npc"></param>
        /// <param name="player"></param>
        /// <param name="credits"></param>
        /// <returns></returns>
        public async Task<bool> NewAsync(NpcVm.IBasicInformationViewModel npc, playerVm.BasicInformationViewModel player, decimal credits)
        {
            if(credits <= 0)
            {
                throw new NotEnoughException(nameof(credits));
            }

            await ValidationLocationAsync(npc, player);
            _game = new SlotMachine(credits);
            await _gameRepository.NewGameAsync(npc, player, _game);
            return true;
        }

        public GetViewModel GetResults()
        {
            return new GetViewModel(_game);
        }
    }
}
