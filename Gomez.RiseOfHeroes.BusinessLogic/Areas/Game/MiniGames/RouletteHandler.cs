﻿using Gomez.Core.Models.Exceptions;
using Gomez.Core.Utilities.Game.Models.Roulette;
using Gomez.RiseOfHeroes.BusinessLogic.Areas.Game.Shared;
using Gomez.RiseOfHeroes.Repositories.Areas.Game;
using Gomez.RiseOfHeroes.Repositories.Areas.Game.MiniGames;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.MiniGames.Roulette;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NpcVm = Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcCustoms;
using playerVm = Gomez.RiseOfHeroes.ViewModels.Areas.Game.Players;

namespace Gomez.RiseOfHeroes.BusinessLogic.Areas.Game.MiniGames
{
    public class RouletteHandler : MiniGameHandlerBase
    {
        private IRouletteTable _game = null;
        private new readonly RouletteRepository _gameRepository;

        public RouletteHandler(PlayerRepository playerRepository, NpcCustomRepository npcRepository, RouletteRepository gameRepository) : 
            base(playerRepository, npcRepository, gameRepository)
        {
            _gameRepository = (RouletteRepository)base._gameRepository;
        }

        private async Task ValidationLocationAsync(NpcVm.IBasicInformationViewModel npc, playerVm.BasicInformationViewModel player)
        {
            if (!await _npcRepository.IsInSamePositionAsync(npc.Id, player.Coordinate))
            {
                throw new BadRequestException(nameof(player.Coordinate));
            }

            if (!npc.HasSlotMachine)
            {
                throw new BadRequestException(nameof(npc.HasSlotMachine));
            }
        }

        /// <summary>
        /// Start a new game
        /// </summary>
        /// <param name="npc"></param>
        /// <param name="player"></param>
        /// <param name="credits"></param>
        /// <returns></returns>
        public async Task<bool> NewAsync(NpcVm.IBasicInformationViewModel npc, playerVm.BasicInformationViewModel player, Bet bet, decimal stake)
        {
            if(stake <= 0)
            {
                throw new NotEnoughException(nameof(stake));
            }

            if (stake > player.Money * 0.10m)
            {
                throw new OutOfBoundException(nameof(stake));
            }

            await ValidationLocationAsync(npc, player);
            _game = new RouletteTable(bet,stake);
            await _gameRepository.NewGameAsync(npc, player, _game);
            return true;
        }

        public GetViewModel GetResults()
        {
            return new GetViewModel(_game);
        }

        public static IEnumerable<SlotViewModel> GetSlots()
        {
            return RouletteTable.GetSlots().Select(x => new SlotViewModel(x));
        }
    }
}
