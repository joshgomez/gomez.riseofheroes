﻿using Gomez.RiseOfHeroes.Repositories.Areas.Game;

namespace Gomez.RiseOfHeroes.BusinessLogic.Areas.Game
{
    public class CharacterHandler : Shared.CharacterHandler<CharacterSharedRepository>
    {
        public CharacterHandler(CharacterSharedRepository sharedRepository, ItemRepository itemRepository) : base(sharedRepository, itemRepository)
        {
            
        }
    }
}
