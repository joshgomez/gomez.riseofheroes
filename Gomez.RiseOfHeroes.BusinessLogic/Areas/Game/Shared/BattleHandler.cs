﻿using Gomez.Core.Utilities;
using Gomez.RiseOfHeroes.Models.Areas.Game.Enums;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Battle;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using G = Gomez.Core.Utilities.Game;

namespace Gomez.RiseOfHeroes.BusinessLogic.Areas.Game.Shared
{
    public abstract class BattleHandler
    {
        private const int MAX_ITERATION = 30; //For safety

        private readonly List<KeyValuePair<string, object[]>> _eventLog = new List<KeyValuePair<string, object[]>>();
        private readonly ResultInfo _info = new ResultInfo();

        protected void AddEventLog(string msg, params object[] values)
        {
            _eventLog.Add(new KeyValuePair<string, object[]>(msg, values));
        }

        public Dictionary<int, CharacterViewModel> Players { get; private set; }
        public Dictionary<int, CharacterViewModel> Opponents { get; private set; }

        protected BattleHandler(CharacterViewModel[] players, CharacterViewModel[] monsters)
        {
            Players = players.ToDictionary(x => x.Id, x => x);
            Opponents = monsters.ToDictionary(x => x.Id, x => x);
        }

        private bool DoesHit(CharacterViewModel victim, CharacterViewModel attacker, out double hardness)
        {
            hardness = 0;
            double dodgeChance = victim.Calculation.DodgeChance;
            double hitChance = attacker.Calculation.HitChance;

            switch (attacker.Option.Tactic)
            {
                case TacticType.Offensive:
                    hitChance *= 1.1;
                    break;
                case TacticType.Defensive:
                    hitChance *= 0.9;
                    break;
            }

            switch (victim.Option.Tactic)
            {
                case TacticType.Offensive:
                    dodgeChance *= 0.9;
                    break;
                case TacticType.Defensive:
                    dodgeChance *= 1.1;
                    break;
            }

            double pot = NumberUtility.RandomBetween(0, hitChance + dodgeChance);

            if (hitChance > pot)
            {
                hardness = (hitChance - pot) / hitChance;
                return true;
            }

            return false;
        }

        private bool DoesCriticalHit(CharacterViewModel victim, CharacterViewModel attacker)
        {
            double dodgeChance = victim.Calculation.DodgeChance;
            double hitChance = attacker.Calculation.CrticalChance;
            double pot = NumberUtility.RandomBetween(0, hitChance + dodgeChance);

            return hitChance > pot;
        }

        private AttackInfo Attack(CharacterViewModel victim, CharacterViewModel attacker)
        {
            var info = new AttackInfo();
            if (DoesHit(victim, attacker, out double hardness))
            {
                if (DoesCriticalHit(victim, attacker))
                {
                    info.Type = AttackType.Critical;
                    info.Damage = attacker.Calculation.MaxDamage * 1.2 * (hardness + 1d);

                    AddEventLog("Battle.Attack.Critical", attacker.Name, victim.Name );
                    return info;
                }

                info.Type = AttackType.Normal;
                info.Damage = NumberUtility.RandomBetween(attacker.Calculation.MinDamage,
                    attacker.Calculation.MaxDamage) * (hardness + 0.5d) - victim.Calculation.Armor;

                AddEventLog("Battle.Attack.Hit", attacker.Name, victim.Name );
                return info;
            }

            AddEventLog("Battle.Attack.Miss", attacker.Name, victim.Name );
            return info;
        }

        private void DoDamage(CharacterViewModel victim, CharacterViewModel attacker, AttackInfo info)
        {
            if(info.Type == AttackType.Miss || attacker.Fallen)
            {
                return;
            }

            victim.Calculation.AddHealth(-info.Damage);
            AddEventLog("Battle.DoDamage", attacker.Name, victim.Name, info.Damage, victim.Calculation.Health);
            if (victim.Calculation.Died)
            {
                int xp = (int)Math.Ceiling(victim.Calculation.Experience * 0.1);
                victim.Calculation.AddExperience(-xp);
                AddEventLog("Battle.Died", victim.Name, xp);
                return;
            }

            if (victim.Fallen)
            {
                AddEventLog("Battle.Fallen",  victim.Name );
            }
        }

        private void AddExperience(CharacterViewModel victim, CharacterViewModel attacker, AttackInfo info)
        {
            if (attacker.Fallen)
            {
                return;
            }

            CharacterViewModel gainer = attacker;
            CharacterViewModel loser = victim;
            double factor = info.Damage;
            if (info.Type == AttackType.Miss)
            {
                gainer = victim;
                loser = attacker;
                factor = attacker.Calculation.MinDamage;
            }

            var xp = (int)Math.Ceiling(loser.Calculation.Level * factor);
            AddEventLog("Battle.AddExperience", gainer.Name, xp );
            if (gainer.Calculation.AddExperience(xp))
            {
                AddEventLog("Battle.AddExperience.Levelup", gainer.Name, gainer.Calculation.Level);
                //Levelup
            }
        }

        private CharacterViewModel GetFirstPlayer(ref int pTurnId)
        {
            int playerId = pTurnId;
            var players = Players.Where(x => !x.Value.Fallen);
            var count = players.Count();
            if (count == 0)
            {
                return Players[pTurnId];
            }

            bool singlePlayer = count == 1;
            pTurnId = players
                .Where(x => singlePlayer || x.Key != playerId)
                .OrderByDescending(x => x.Value.Calculation.Quickness).Select(x => x.Key).First();

            return Players[pTurnId];
        }

        private CharacterViewModel GetFirstOpponent(ref int oTurnId)
        {
            int opponentId = oTurnId;
            var opponents = Opponents.Where(x => !x.Value.Fallen);
            var count = opponents.Count();
            if(count == 0)
            {
                return Opponents[oTurnId];
            }

            bool singleOpponent = count == 1;
            oTurnId = opponents
                .Where(x => singleOpponent || x.Key != opponentId)
                .OrderByDescending(x => x.Value.Calculation.Quickness).Select(x => x.Key).First();

            return Opponents[oTurnId];
        }

        private CharacterViewModel Target(Dictionary<int, CharacterViewModel> targets, CharacterViewModel current)
        {
            var opponents = targets.Where(x => !x.Value.Fallen);
            switch (current.Option.Target)
            {
                case TargetType.HighestHealth:
                    opponents = opponents.OrderByDescending(x => x.Value.Calculation.Health);
                    break;
                case TargetType.LowestHealth:
                    opponents = opponents.OrderBy(x => x.Value.Calculation.Health);
                    break;
                case TargetType.Strongest:
                    opponents = opponents.OrderByDescending(x => x.Value.Calculation.MaxDamage);
                    break;
                case TargetType.Weakest:
                    opponents = opponents.OrderBy(x => x.Value.Calculation.MaxDamage);
                    break;
            }

            int oId = opponents.Select(x => x.Key).FirstOrDefault();
            if (current.Option.Target == TargetType.Any)
            {
                int count = opponents.Count();
                if (count > 1)
                {
                    oId = opponents.ElementAt(NumberUtility.RandomBetween(0, count - 1)).Key;
                }
            }

            var target = targets[oId];
            AddEventLog("Battle.Target.Opponent", current.Name, target.Name );
            return target;
        }

        private CharacterViewModel GetOpponent(CharacterViewModel player)
        {
            return Target(Opponents, player);
        }

        private CharacterViewModel GetPlayer(CharacterViewModel opponent)
        {
            return Target(Players, opponent);
        }

        private bool IsPlayerFirst(CharacterViewModel player, CharacterViewModel opponent)
        {
            double pot = NumberUtility.RandomBetween(0, player.Calculation.Quickness + opponent.Calculation.Quickness);
            bool isFirst = player.Calculation.Quickness > pot;

            if (isFirst)
            {
                AddEventLog("Battle.IsFirst.Player",  player.Name );
                return true;
            }

            AddEventLog("Battle.IsFirst.Opponent",  opponent.Name );
            return false;
        }

        private void AddLootMoney(Dictionary<int, CharacterViewModel> winners, IEnumerable<CharacterViewModel> losers, decimal factor)
        {
            var money = (losers.Sum(x => x.Calculation.Money) / winners.Count) * factor;
            foreach (var winner in winners)
            {
                decimal value = money;
                if (winner.Value.Fallen)
                {
                    value *= 0.5m;
                }

                winner.Value.Calculation.AddMoney(value);
                _info.Money += value;
            }
        }

        /// <summary>
        /// Use the override the factor, default is all money from the losers.
        /// </summary>
        /// <param name="winners"></param>
        /// <param name="losers"></param>
        protected virtual void AddLootMoney(Dictionary<int, CharacterViewModel> winners, IEnumerable<CharacterViewModel> losers)
        {
            AddLootMoney(winners, losers, 1);
        }

        public ResultInfo Battle()
        {
            int pTurnId = -1;
            int oTurnId = -1;

            int i = 0;
            while(!Players.All(x => x.Value.Fallen) && !Opponents.All(x => x.Value.Fallen))
            {
                i++;
                if(i >= MAX_ITERATION)
                {
                    //Max round rached, battle is a draw.
                    AddEventLog("Battle.Result.Draw.ByLength", i);
                    break;
                }

                AddEventLog("Battle.Round", i);

                var player = GetFirstPlayer(ref pTurnId);
                var opponent = GetFirstOpponent(ref oTurnId);

                bool playerFirst = IsPlayerFirst(player, opponent);

                if (playerFirst)
                {
                    opponent = GetOpponent(player);
                    var info = Attack(opponent, player);
                    DoDamage(opponent, player, info);
                    AddExperience(opponent, player, info);

                    if(info.Type != AttackType.Critical)
                    {
                        info = Attack(player, opponent);
                        DoDamage(player, opponent, info);
                        AddExperience(player, opponent, info);
                    }
                }
                else
                {
                    player = GetPlayer(opponent);
                    var info = Attack(player, opponent);
                    DoDamage(player, opponent, info);
                    AddExperience(player, opponent, info);

                    if(info.Type != AttackType.Critical)
                    {
                        info = Attack(opponent, player);
                        DoDamage(opponent, player, info);
                        AddExperience(opponent, player, info);
                    }
                }
            }

            bool allPlayersFallen = Players.All(x => x.Value.Fallen);
            bool allOpponentFallen = Opponents.All(x => x.Value.Fallen);
            if ((allPlayersFallen && allOpponentFallen) || i >= MAX_ITERATION)
            {
                _info.Result = BattleResultType.Draw;
                AddEventLog("Battle.Result.Draw", Array.Empty<object>());
            }
            else if (allPlayersFallen)
            {
                string[] winnerNames = Opponents.Select(x => x.Value.Name).ToArray();
                _info.Result = BattleResultType.OpponentsWon;
                AddEventLog("Battle.Result.Won.Opponents", winnerNames);
                AddLootMoney(Opponents, Players.Select(x => x.Value));
            }
            else if (allOpponentFallen)
            {
                string[] winnerNames = Players.Select(x => x.Value.Name).ToArray();
                _info.Result = BattleResultType.PlayersWon;
                AddEventLog("Battle.Result.Won.Players", winnerNames);
                AddLootMoney(Players, Opponents.Select(x => x.Value));
            }

            _info.EventLog = GetEventLog();

            return _info;
        }

        private IEnumerable<KeyValuePair<string,object[]>> GetEventLog()
        {
            return _eventLog.AsEnumerable();
        }
    }
}
