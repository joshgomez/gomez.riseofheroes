﻿using Gomez.RiseOfHeroes.Repositories.Areas.Game;

namespace Gomez.RiseOfHeroes.BusinessLogic.Areas.Game.Shared
{
    public abstract class MiniGameHandlerBase
    {
        protected readonly PlayerRepository _playerRepository;
        protected readonly NpcCustomRepository _npcRepository;
        protected readonly MiniGameRepository _gameRepository;

        protected MiniGameHandlerBase(PlayerRepository playerRepository, NpcCustomRepository npcRepository, MiniGameRepository gameRepository)
        {
            _playerRepository = playerRepository;
            _npcRepository = npcRepository;
            _gameRepository = gameRepository;
        }
    }
}
