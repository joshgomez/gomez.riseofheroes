﻿using Gomez.Core.Models.Shared;
using Gomez.Core.ViewModels.Shared;
using Gomez.RiseOfHeroes.Calculations;
using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using Gomez.RiseOfHeroes.Repositories.Areas.Game;
using Gomez.RiseOfHeroes.Repositories.Areas.Game.Shared;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.BusinessLogic.Areas.Game.Shared
{
    public abstract class CharacterHandler<T> where T : CharacterRepository
    {
        private readonly T _characterRepository;
        protected readonly ItemRepository _itemRepository;

        protected CharacterHandler(T characterRepository, ItemRepository itemRepository)
        {
            _characterRepository = characterRepository;
            _itemRepository = itemRepository;
        }

        public async Task<EquipmentViewModel> GetEquipmentAsync(int id)
        {
            var vm = new EquipmentViewModel();
            var equipments = await _characterRepository.GetEquipmentItemsAsync(id);
            vm.Head = equipments.FirstOrDefault(x => x.Type == ItemConstant.TYPE_HEAD_GEAR);
            vm.Body = equipments.FirstOrDefault(x => x.Type == ItemConstant.TYPE_BODY_GEAR);
            vm.Weapon = equipments.FirstOrDefault(x => x.Type == ItemConstant.TYPE_WEAPON);
            return vm;
        }

        protected async Task<TCalc> LoadEquipmentStatsAsync<TCalc>(int id, TCalc calculation) where TCalc : CharacterCalculation
        {
            var equipmentsIds = await _characterRepository.GetEquipmentDetailIdsAsync(id);
            if(equipmentsIds.Count > 0)
            {
                var detailIds = equipmentsIds.Values.Distinct().ToArray();
                var loadItemData = await _itemRepository.DetailRepo.GetPropertiesAsync(detailIds);
                foreach (var detail in equipmentsIds)
                {
                    var collection = new SimplePropertyDtoCollection(loadItemData[detail.Value]);
                    calculation.InitEquipment(collection);
                }
            }


            return calculation;
        }

        /// <summary>
        /// Verify location of the none player character.
        /// </summary>
        /// <param name="id">The id should not be the player id.</param>
        /// <param name="coord"></param>
        /// <returns></returns>
        public async Task<bool> InInSamePositionAsync(int id, ICoordinate coord)
        {
            return await _characterRepository.IsInSamePositionAsync(id, coord);
        }

        public async Task<string> GetGescriptionAsync(int id)
        {
            return await _characterRepository.GetDescriptionAsync(id);
        }
    }
}
