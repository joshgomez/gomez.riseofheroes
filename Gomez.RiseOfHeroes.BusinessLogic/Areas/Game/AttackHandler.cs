﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Battle;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters;

namespace Gomez.RiseOfHeroes.BusinessLogic.Areas.Game
{
    public class AttackHandler
    {
        private readonly PlayerHandler _playerHandler;
        private readonly NpcEnemyHandler _npcEnemyHandler;
        private readonly BattleHelper _helper;
        private readonly LootHandler _lootHandler;

        public AttackHandler(PlayerHandler playerHandler, NpcEnemyHandler npcEnemyHandler, LootHandler lootHandler, BattleHelper helper)
        {
            _playerHandler = playerHandler;
            _npcEnemyHandler = npcEnemyHandler;
            _lootHandler = lootHandler;
            _helper = helper;
        }

        private async Task<ResultInfo> SetLootToResultAsync(ResultInfo result, CharacterViewModel[] enemies)
        {
            var allLoot = await _lootHandler.GetAllLootsAsync(enemies);
            var loot = await _lootHandler.GetDropsAsync(allLoot, enemies);
            result.Items = loot;
            return result;
        }


        public async Task<ResultViewModel> AttackSingleNpcAsync(Guid userId, int enemyId, BattleOption option)
        {
            double energyCost = 0.01;
            var player = await _playerHandler.GetAsync(userId);
            player.Option = option;

            if(player.Calculation.Energy < energyCost)
            {
                throw new Gomez.Core.Models.Exceptions
                    .NotEnoughException($"{nameof(player.Calculation.Energy)}:{energyCost}");
            }

            player.Calculation.AddEnergy(-energyCost);
            var enemy = await _npcEnemyHandler.GetAsync(enemyId);
            if(!await _npcEnemyHandler.InInSamePositionAsync(enemyId, player.Coordinate))
            {
                throw new Gomez.Core.Models.Exceptions.BadRequestException("", "INVALID_LOCATION");
            }

            enemy.Calculation.AddHealth(enemy.Calculation.MaxHealth);
            enemy.Option = new BattleOption
            {
                RetreatAtHealthPercent = 20,
                Tactic = Models.Areas.Game.Enums.TacticType.Normal,
                Target = Models.Areas.Game.Enums.TargetType.Any
            };

            var players = new CharacterViewModel[] { player };
            var enemies = new CharacterViewModel[] { enemy };

            var battleHandler = new NpcBattleHandler(players,enemies);
            var result = battleHandler.Battle();
            result = await SetLootToResultAsync(result, enemies);
            var drops = result.Items.Keys.ToArray();
            await _playerHandler.SaveFromViewModelAndLootAsync(player, drops);
            return _helper.ToResult(result);
        }
    }
}
