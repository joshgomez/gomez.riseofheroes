﻿using Gomez.RiseOfHeroes.BusinessLogic.Areas.Game.Shared;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.BusinessLogic.Areas.Game
{
    public class NpcBattleHandler : BattleHandler
    {
        public NpcBattleHandler(CharacterViewModel[] players, CharacterViewModel[] monsters) : base(players, monsters)
        {

        }
    }
}
