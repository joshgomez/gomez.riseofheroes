﻿using Gomez.RiseOfHeroes.BusinessLogic.Areas.Game.MiniGames;
using Gomez.RiseOfHeroes.Repositories.Areas.Game;

namespace Gomez.RiseOfHeroes.BusinessLogic.Areas.Game
{
    public class MiniGameHandler
    {
        public BlackJackHandler BlackJack { get; private set; }
        public SlotsHandler Slots { get; set; }
        public RouletteHandler Roulette { get; set; }

        public MiniGameHandler(PlayerRepository playerRepository, NpcCustomRepository npcRepository, MiniGameRepository gameRepository)
        {
            var blackJackRepo = gameRepository.CreateBlackJackRepo();
            var slotsRepo = gameRepository.CreateSlotsRepo();
            var rouletteRepo = gameRepository.CreateRouletteRepo();

            BlackJack = new BlackJackHandler(playerRepository, npcRepository, blackJackRepo);
            Slots = new SlotsHandler(playerRepository, npcRepository, slotsRepo);
            Roulette = new RouletteHandler(playerRepository, npcRepository, rouletteRepo);
        }
    }
}
