﻿using Gomez.RiseOfHeroes.Repositories.Areas.Game;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.BusinessLogic.Areas.Game
{
    public class LootHandler
    {
        private readonly ItemRepository _itemRepository;

        public LootHandler(ItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        /// <summary>
        /// Get all possible loot from all enemies.
        /// </summary>
        /// <param name="enemies"></param>
        /// <returns></returns>
        public async Task<Guid[]> GetAllLootsAsync(CharacterViewModel[] enemies)
        {
            var enemyIds = enemies.Select(x => x.Id).ToArray();
            return await _itemRepository.GetAllLootsAsync(enemyIds);
        }

        /// <summary>
        /// Get the drops
        /// </summary>
        /// <param name="detailIds">id of the item</param>
        /// <param name="enemies"></param>
        /// <returns></returns>
        private async Task<Guid[]> GetDropsInternalAsync(Guid[] detailIds, CharacterViewModel[] enemies)
        {
            if (detailIds?.Length > 0)
            {
                int monsterLvl = enemies.Max(x => x.Calculation.Level);
                int itemFound = 0;
                var retValue = new List<Guid>();
                await foreach (var (itemId, score, level) in _itemRepository.DetailRepo.GetAllRarenessAsync(detailIds))
                {
                    if (itemFound >= enemies.Length)
                    {
                        break;
                    }

                    var chance = monsterLvl * 5;
                    var pot = Core.Utilities.NumberUtility.RandomBetween(0,(10 * score) + chance);

                    if (chance > pot)
                    {
                        itemFound++;
                        retValue.Add(itemId);
                    }
                }

                return retValue.ToArray();
            }

            return Array.Empty<Guid>();
        }

        public async Task<Dictionary<Guid,string>> GetDropsAsync(Guid[] detailIds, CharacterViewModel[] enemies)
        {
            var drops = await GetDropsInternalAsync(detailIds, enemies);
            if(drops.Length == 0)
            {
                return default;
            }

            return await _itemRepository.GetNamesOfDropsAsync(drops);
        }
    }
}
