﻿using Gomez.Core.ViewModels.Shared;
using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using Gomez.RiseOfHeroes.Repositories.Areas.Game;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.NpcShops;
using System;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.BusinessLogic.Areas.Game
{
    public class ShopHandler
    {
        private readonly NpcShopHandler _npcShopHandler;
        private readonly ShopRepository _shopRepository;
        private readonly PlayerRepository _playerRepository;

        public ShopHandler(NpcShopHandler npcShopHandler, ShopRepository shopRepository, PlayerRepository playerRepository)
        {
            _npcShopHandler = npcShopHandler;
            _shopRepository = shopRepository;
            _playerRepository = playerRepository;
        }

        public async Task<ValidationViewModel<SellInternalViewModel>> SellAsync(Guid userId, long itemId, int qty)
        {
            var buyerId = await _playerRepository.GetIdByUserIdAsyc(userId);
            return await SellAsync(buyerId, itemId, qty);
        }


        /// <summary>
        /// Use to buy items from a npc shop.
        /// Concurrency safe, abort on conflict 
        /// </summary>
        /// <param name="buyerId">The id of the player</param>
        /// <param name="itemId"></param>
        /// <param name="qty"></param>
        /// <returns></returns>
        public async Task<ValidationViewModel<SellInternalViewModel>> SellAsync(int buyerId, long itemId, int qty)
        {
            var data = await _npcShopHandler.GetSellInformationAsync(itemId, qty);
            var vm = data.vm;
            var item = data.item;
            if (item == null)
            {
                return vm;
            }

            if (buyerId == item.CharacterId)
            {
                vm.AddError("Shop.Sell.SamePerson");
            }

            if (vm.IsValid)
            {
                var character = await _playerRepository.GetWithDetailsAsync(buyerId, $"{CharacterConstant.KEY_MONEY}");
                vm.Model.BuyersId = character.Id;
                vm.Model.BuyersMoney = character.PropertyCollection
                    .GetProperty<decimal>(CharacterConstant.KEY_MONEY, out byte[] rowVersion);
                vm.Model.BuyersMoneyVersion = rowVersion;
                if (vm.Model.Cost > vm.Model.BuyersMoney)
                {
                    vm.AddError("NOT_ENOUGH_MONEY");
                }

                if (!await _npcShopHandler.InInSamePositionAsync(vm.Model.SellersId, character.Character.Coordinate))
                {
                    vm.AddError("INVALID_LOCATION");
                }
            }

            return await _shopRepository.SellAsync(vm,item);
        }

        /// <summary>
        /// Use to sell items to a shop.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="buyerId">The id of the npc</param>
        /// <param name="itemId"></param>
        /// <param name="qty"></param>
        /// <returns></returns>
        public async Task<ValidationViewModel<SellInternalViewModel>> BuyAsync(Guid? userId, int buyerId, long itemId, int qty)
        {
            var data = await _npcShopHandler.GetBuyInformationAsync(buyerId,itemId, qty);
            var vm = data.vm;
            var item = data.item;
            if(item == null)
            {
                return vm;
            }


            if(userId != null)
            {
                var sellerId = await _playerRepository.GetIdByUserIdAsyc(userId.Value);
                if(item.CharacterId != sellerId)
                {
                    vm.AddError("Shop.Buy.SellerNotFound.Text");
                }
            }

            if (vm.IsValid)
            {
                var character = await _playerRepository.GetWithDetailsAsync(item.CharacterId, $"{CharacterConstant.KEY_MONEY}");
                vm.Model.SellersId = character.Id;
                vm.Model.SellersMoney = character.PropertyCollection
                    .GetProperty<decimal>(CharacterConstant.KEY_MONEY, out byte[] rowVersion);
                vm.Model.SellersQuantity = item.Quantity - 1;
                vm.Model.SellersMoneyVersion = rowVersion;
                if (vm.Model.Cost > vm.Model.BuyersMoney)
                {
                    vm.AddError("NOT_ENOUGH_MONEY");
                }

                if (!await _npcShopHandler.InInSamePositionAsync(vm.Model.BuyersId, character.Character.Coordinate))
                {
                    vm.AddError("INVALID_LOCATION");
                }

                if (vm.Model.BuyersId == vm.Model.SellersId)
                {
                    vm.AddError("Shop.Buy.SamePerson");
                }
            }

            return await _shopRepository.BuyAsync(vm, item);
        }
    }
}
