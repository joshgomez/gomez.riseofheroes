﻿using Gomez.RiseOfHeroes.BusinessLogic.Areas.Game.Shared;
using Gomez.RiseOfHeroes.Calculations;
using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using Gomez.RiseOfHeroes.Repositories.Areas.Game;
using Gomez.RiseOfHeroes.ViewModels.Areas.Game.Characters;
using System.Threading.Tasks;

namespace Gomez.RiseOfHeroes.BusinessLogic.Areas.Game
{
    public class NpcEnemyHandler : CharacterHandler<NpcEnemyRepository>
    {
        private readonly NpcEnemyRepository _npcRepository;

        public NpcEnemyHandler(NpcEnemyRepository npcRepository, ItemRepository itemRepository) : base(npcRepository,itemRepository)
        {
            _npcRepository = npcRepository;
        }

        public async Task<IndexViewModel> GetIndexAsync(QueryIndexViewModel query)
        {
            var viewModel = new IndexViewModel(query);
            viewModel = await _npcRepository.IndexAsync(viewModel);
            return viewModel;
        }

        public async Task<ViewModels.Areas.Game.NpcEnemies.BasicInformationViewModel> GetBasicInformationAsync(int id)
        {
            var baseVm = new GetViewModel
            {
                Id = id,
                Property = "*"
            };

            baseVm = await _npcRepository.GetWithDetailsAsync(baseVm);
            var npcCacl = baseVm.GetPropertyByType<CharacterCalculation>() as NpcEnemyCalculation;
            npcCacl = await base.LoadEquipmentStatsAsync(baseVm.Id, npcCacl);
            return new ViewModels.Areas.Game.NpcEnemies.BasicInformationViewModel()
            {
                Id = baseVm.Character.Id,
                Name = baseVm.Character.Name,
                Health = baseVm.PropertyCollection.GetProperty<double>(CharacterConstant.KEY_STATS_HEALTH),
                MaxHealth = npcCacl.MaxHealth,
                MaxDamage = npcCacl.MaxDamage,
                MinDamage = npcCacl.MinDamage,
                Strength = baseVm.PropertyCollection.GetProperty<int>(CharacterConstant.KEY_ATTR_STRENGTH),
                Constitution = baseVm.PropertyCollection.GetProperty<int>(CharacterConstant.KEY_ATTR_CONSTITUTION),
                Dexterity = baseVm.PropertyCollection.GetProperty<int>(CharacterConstant.KEY_ATTR_DEXTERITY),
                Intelligence = baseVm.PropertyCollection.GetProperty<int>(CharacterConstant.KEY_ATTR_INTELLIGENCE),
                Weight = npcCacl.Weight,
                Level = baseVm.PropertyCollection.GetProperty<int>(CharacterConstant.KEY_STATS_LEVEL),
                Armor = npcCacl.Armor,
                Money = npcCacl.Money,
                HasImage = baseVm.PropertyCollection.GetProperty<bool>(CharacterConstant.KEY_HAS_IMAGE)
            };
        }

        public async Task<CharacterViewModel> GetAsync(int id)
        {
            var baseVm = new GetViewModel
            {
                Id = id,
                Property = "*"
            };

            baseVm = await _npcRepository.GetWithDetailsAsync(baseVm);
            var vm = new CharacterViewModel(baseVm);
            vm.Calculation = await base.LoadEquipmentStatsAsync(vm.Id, vm.Calculation);


            return vm;
        }
    }
}
