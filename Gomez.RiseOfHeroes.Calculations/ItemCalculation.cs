﻿using Gomez.Core.Models.Entity;
using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using Gomez.RiseOfHeroes.Models.Areas.Game.Enums;

namespace Gomez.RiseOfHeroes.Calculations
{
    public class ItemCalculation : Gomez.Core.Calculations.CalculationBase
    {
        public const double BASE_HEALTH = 100;
        public const double BASE_MAX_DAMAGE = 10;
        public const double BASE_MIN_DAMAGE = 5;

        public const double BASE_CHANCE_CRITICAL_HIT    = 0.05;
        public const double BASE_CHANCE_DODGE           = 0.30;
        public const double BASE_CHANCE_HIT             = 0.70;

        public ItemCalculation()
        {

        }

        public override void Init(ISimplePropertyCollection collection)
        {
            Health = collection.GetProperty<double>(ItemConstant.KEY_STATS_HEALTH);
            Weight = collection.GetProperty<double>(ItemConstant.KEY_STATS_WEIGHT);
            Quantity = collection.GetProperty<int>(ItemConstant.KEY_STATS_QUANTITY);
        }

        public double Health { get; protected set; }
        public double Weight { get; protected set; }
        public int Quantity { get; protected set; }
        public double TotalWeight => Weight * (Quantity > 0 ? Quantity : 1);
        public double HealingEffect => Health / 100d ;

        public static decimal CostCalculation(decimal cost, ItemState state)
        {
            return state switch
            {
                ItemState.ShopSecondHand => cost * 0.9m,
                ItemState.ShopSale => cost,
                _ => cost * 0.8m,
            };
        }
    }
}
