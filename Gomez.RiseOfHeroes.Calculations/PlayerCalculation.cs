﻿using Gomez.Core.Models.Entity;
using Gomez.Core.Utilities;
using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using System;

namespace Gomez.RiseOfHeroes.Calculations
{
    public class PlayerCalculation : CharacterCalculation
    {
        public PlayerCalculation()
        {

        }

        public override void Init(ISimplePropertyCollection collection)
        {
            Experience = GetProperty<int>(collection, PlayerConstant.KEY_STATS_EXPERIENCE);
            Attributes = GetProperty<int>(collection, PlayerConstant.KEY_STATS_ATTRIBUTEPOINTS);
            base.Init(collection);
        }

        /// <summary>
        /// Load all parameters required for energy calculations.
        /// </summary>
        /// <param name="collection"></param>
        public void InitEnergy(double energy, byte[] energyRowVersion)
        {
            Energy = energy;
            RowVersions[CharacterConstant.KEY_STATS_ENERGY] = energyRowVersion;
        }

        public double LevelProgressFactor => (double)Experience / NextLevel;
        public double LevelProgressPercent => LevelProgressFactor * 100d;

        public int Attributes { get; private set; }

        public override bool AddExperience(int value)
        {
            if (base.AddExperience(value))
            {
                Attributes += 5;
                return true;
            }

            return false;
        }

        public decimal WorkWage(int difficult, int workIntelligence, double energy)
        {
            var pot = NumberUtility.RandomBetween(1,workIntelligence + Intelligence);
            if(Intelligence > pot)
            {
                pot = workIntelligence + Intelligence;
            }

            decimal wage = (pot * difficult) * (0.01m + Convert.ToDecimal(energy));
            return wage * 2;
        }
    }
}
