﻿using Gomez.Core.Models.Entity;
using Gomez.Core.Utilities.Game;
using Gomez.RiseOfHeroes.Configurations.Constants.Areas.Game;
using System;
using System.Collections.Generic;

namespace Gomez.RiseOfHeroes.Calculations
{
    public class CharacterCalculation : Gomez.Core.Calculations.CalculationBase
    {
        public const double BASE_ENERGY = 1.0;
        public const double BASE_HEALTH = 100;
        public const double BASE_MAX_DAMAGE = 0.4;
        public const double BASE_MIN_DAMAGE = 0.2;

        public const double BASE_CHANCE_CRITICAL_HIT = 0.05;
        public const double BASE_CHANCE_DODGE = 0.30;
        public const double BASE_CHANCE_HIT = 0.70;

        public CharacterCalculation() : base()
        {

        }

        public override void Init(ISimplePropertyCollection collection)
        {
            Level = GetProperty<int>(collection,CharacterConstant.KEY_STATS_LEVEL);

            Energy = GetProperty<double>(collection, CharacterConstant.KEY_STATS_ENERGY);
            Health = GetProperty<double>(collection, CharacterConstant.KEY_STATS_HEALTH);

            Strength = GetProperty<int>(collection,CharacterConstant.KEY_ATTR_STRENGTH);
            Constitution = GetProperty<int>(collection, CharacterConstant.KEY_ATTR_CONSTITUTION);
            Dexterity = GetProperty<int>(collection, CharacterConstant.KEY_ATTR_DEXTERITY);
            Intelligence = GetProperty<int>(collection, CharacterConstant.KEY_ATTR_INTELLIGENCE);
            Money = GetProperty<decimal>(collection, CharacterConstant.KEY_MONEY);
        }

        public int Experience { get; protected set; }

        public int NextLevel => Convert.ToInt32(NumberUtility.Nextlevel(this.Level, 150));
        public double NextLevelFactor => NextLevel > 0 ? ((double)Experience / (double)NextLevel) : 0;

        /// <summary>
        /// Add experience to reach next level.
        /// </summary>
        /// <param name="value">amount of experience</param>
        /// <returns>true if reached next level</returns>
        public virtual bool AddExperience(int value)
        {
            Experience += value;
            if (Experience > NextLevel)
            {
                int xp = Experience - NextLevel;
                Level += 1;
                Experience = xp;
                return true;
            }

            if (Experience < 0)
            {
                Experience = 0;
            }

            return false;
        }

        public double Energy { get; protected set; }
        public double Health { get; protected set; }
        
        public int Armor { get; protected set; }
        public double Damage { get; protected set; }

        public int Level { get; protected set; }
        public decimal Money { get; protected set; }
        public void AddMoney(decimal value)
        {
            Money += value;
        }

        public bool Buy(decimal cost)
        {
            if(Money >= cost)
            {
                AddMoney(-cost);
                return true;
            }

            return false;
        }

        private double MaxHealthInternal() => BASE_HEALTH + (Constitution * 5) - Level;
        public double MaxHealth => MaxHealthInternal() > BASE_HEALTH ? MaxHealthInternal() : BASE_HEALTH;
        public double MaxEnergy => BASE_ENERGY;

        public bool Died => Health <= 0;

        public void AddHealth(double value)
        {
            Health += value;
            if (Health > MaxHealth)
            {
                Health = MaxHealth;
                return;
            }

            if(Health < 0)
            {
                Health = 0; 
            }
        }

        public void AddEnergy(double value)
        {
            Energy += value;
            if (Energy > MaxEnergy)
            {
                Energy = MaxEnergy;
                return;
            }

            if (Energy < 0)
            {
                Energy = 0;
            }
        }

        public int Strength { get; protected set ; }
        public int Constitution { get; protected set; }
        public int Dexterity { get; protected set; }
        public int Intelligence { get; protected set; }
        public double Weight { get; protected set; }

        public double MaxDamage => BASE_MAX_DAMAGE + Strength * 3 + Damage;
        public double MinDamage => BASE_MIN_DAMAGE + Strength + Dexterity * 2;
        public double CriticalDamage => MaxDamage > (MinDamage * 2) ? MaxDamage * 1.5d : MinDamage * 2;

        private double WeightPenalty => Weight/200d;

        private double CriticalChanceInternal() => BASE_CHANCE_CRITICAL_HIT * (1 + Math.Log10((Dexterity + Intelligence * 0.5) - WeightPenalty));
        public double CrticalChance => CriticalChanceInternal() > BASE_CHANCE_CRITICAL_HIT ? CriticalChanceInternal() : BASE_CHANCE_CRITICAL_HIT;


        private double DodgeChanceInternal() => BASE_CHANCE_DODGE * (1 + Math.Log10(Dexterity - WeightPenalty));
        public double DodgeChance => DodgeChanceInternal() > BASE_CHANCE_DODGE ? DodgeChanceInternal() : BASE_CHANCE_DODGE;

        private double HitChanceInternal() => BASE_CHANCE_HIT * (1 + Math.Log10(Dexterity - WeightPenalty));
        public double  HitChance => HitChanceInternal() > BASE_CHANCE_HIT ? HitChanceInternal() : BASE_CHANCE_HIT;

        public double Quickness => Math.Log10((Health * 0.5d + Dexterity) - WeightPenalty);

        public void InitEquipment(ISimplePropertyCollection collection)
        {
            Strength        += collection.GetProperty<int>(ItemConstant.KEY_ATTR_STRENGTH);
            Constitution    += collection.GetProperty<int>(ItemConstant.KEY_ATTR_CONSTITUTION);
            Dexterity       += collection.GetProperty<int>(ItemConstant.KEY_ATTR_DEXTERITY);
            Intelligence    += collection.GetProperty<int>(ItemConstant.KEY_ATTR_INTELLIGENCE);
            Armor           += collection.GetProperty<int>(ItemConstant.KEY_STATS_ARMOR);
            Weight          += collection.GetProperty<double>(ItemConstant.KEY_STATS_WEIGHT);
            Damage          += collection.GetProperty<double>(ItemConstant.KEY_STATS_DAMAGE);
        }
    }
}
