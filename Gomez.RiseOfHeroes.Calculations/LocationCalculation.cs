﻿using Gomez.Core.Utilities;
using System;

namespace Gomez.RiseOfHeroes.Calculations
{
    public class LocationCalculation : Gomez.Core.Calculations.CalculationBase
    {
        public const double BASE_DISTANCE_MULTIPLIER = 100;
        public const double BASE_DISTANCE_COST_DIVIDER = 100;

        public LocationCalculation()
        {

        }

        public static double CalculateDistance(double value)
        {
            return NumberUtility.RoundToNearestTen(value * BASE_DISTANCE_MULTIPLIER);
        }

        public static decimal CalculateCost(double value)
        {
            return Convert.ToDecimal(value / BASE_DISTANCE_COST_DIVIDER);
        }
    }
}
